package;

import hw.log.ILogger;

class L {

  private static var loggers:Array<ILogger> = new Array<ILogger>();

  public static function push(logger:ILogger):Void {
    loggers.push(logger);
  }

  public static function d(tag:String, message:String, ?error:Dynamic, ?p:haxe.PosInfos):Void {
    for (logger in loggers) logger.d(tag, message, error, p);
  }

  public static function i(tag:String, message:String, ?error:Dynamic, ?p:haxe.PosInfos):Void {
    for (logger in loggers) logger.i(tag, message, error, p);
  }

  public static function w(tag:String, message:String, ?error:Dynamic, ?p:haxe.PosInfos):Void {
    for (logger in loggers) logger.w(tag, message, error, p);
  }

  public static function e(tag:String, message:String, ?error:Dynamic, ?p:haxe.PosInfos):Void {
    for (logger in loggers) logger.e(tag, message, error, p);
  }
}
