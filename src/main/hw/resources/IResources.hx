package hw.resources;

import flash.display.BitmapData;
import flash.display.MovieClip;
import hw.resources.Resources;

@:provide(Resources) interface IResources {
    public var image(default, null):ResMap<BitmapData>;
    public var color(default, null):ResMap<Int>;
    public var movie(default, null):ResMap<MovieClip>;
    public var text(default, null):ResMap<String>;
    public var float(default, null):ResMap<Float>;
    public var int(default, null):ResMap<Int>;
    public var any(default, null):ResMap<Dynamic>;
}
