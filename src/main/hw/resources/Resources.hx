package hw.resources;

import flash.display.BitmapData;
import flash.display.MovieClip;
import haxe.ds.StringMap;

private typedef Listener = {object:Dynamic, field:String};

class ResMap<T> extends StringMap<T> {

    private var listeners:StringMap<Array<Listener>>;

    public function new() {
        super();
        listeners = new StringMap();
    }

    public function put(key:String, value:T):Void {
        set(key, value);
        if (listeners.exists(key)) {
            for (f in listeners.get(key)) call(f, value);
        }
    }

    public function bind(key:String, object:Dynamic, field:String):Void {
        var listener:Listener = {object:object, field:field};
        if (listeners.exists(key)) {
            listeners.set(key, listeners.get(key).filter(function(l) return l.object != object || l.field != field));
            listeners.get(key).push(listener);
        } else {
            listeners.set(key, [listener]);
        }
        if (exists(key)) call(listener, get(key));
    }

    private function call(listener:Listener, value:T):Void {
        Reflect.setProperty(listener.object, listener.field, value);
    }

    public function merge(value:Dynamic<T>):Void {
        for (field in Reflect.fields(value)) {
            put(field, Reflect.field(value, field));
        }
    }
}

class Resources implements IResources {

    public var image(default, null):ResMap<BitmapData>;
    public var color(default, null):ResMap<Int>;
    public var movie(default, null):ResMap<MovieClip>;
    public var text(default, null):ResMap<String>;
    public var float(default, null):ResMap<Float>;
    public var int(default, null):ResMap<Int>;
    public var any(default, null):ResMap<Dynamic>;

    public function new() {
        image = new ResMap<BitmapData>();
        color = new ResMap<Int>();
        movie = new ResMap<MovieClip>();
        text = new ResMap<String>();
        float = new ResMap<Float>();
        int = new ResMap<Int>();
        any = new ResMap<Dynamic>();
    }
}
