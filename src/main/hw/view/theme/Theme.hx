package hw.view.theme;

import flash.text.Font;
import flash.text.FontType;
import hw.color.Color;
import hw.signal.Signal;
import hw.view.geometry.Box;
import hw.view.geometry.HAlign;
import hw.view.geometry.SizeValue;
import hw.view.geometry.VAlign;
import hw.view.skin.TabColorSkin;
import hw.view.theme.ITheme;

using hw.color.ColorUtil;

class Style {
    public var id(default, null):StyleId;
    public var parent(default, null):StyleId;
    public var data(default, null):Map<String, Dynamic>;

    public function new(id:StyleId, data:Map<String, Dynamic>, ?parent:StyleId) {
        this.id = id;
        this.parent = parent;
        this.data = data;
    }
}

class StyleBundle {

    private var data:Map<String, Map<String, Dynamic>>;
    private var parentMap:Map<String, String>;

    public function new() {
        data = new Map();
        parentMap = new Map();
    }

    public function register(style:Style):Void {
        for (key in style.data.keys()) {
            if (!data.exists(key)) {
                data.set(key, new Map());
            }
            data.get(key).set(style.id, style.data.get(key));
        }
        if (style.parent != null) {
            parentMap.set(style.id, style.parent);
        }
    }

    public function get(key:String, id:StyleId):Null<Dynamic> {
        var result = null;
        if (data.exists(key)) {
            var bundle:Map<String, Dynamic> = data.get(key);
            var length = 0;
            for (key in bundle.keys()) {
                if (id.match(key) && key.length > length) {
                    result = bundle.get(key);
                    length = key.length;
                }
            }
        }
        if (result == null) {
            if (parentMap.exists(id)) {
                var parentId:StyleId = parentMap.get(id);
                for (pId in parentId.toArray()) {
                    result = get(key, pId);
                    if (result != null) {
                        break;
                    }
                }
            }
        }
        return result;
    }
}

class Theme implements ITheme {
    public var font(default, set):ThemeFont;
    public var fontSize(default, set):ThemeFontSize;
    public var colors(default, set):ThemeColors;
    public var updateSignal(default, null):Signal0;

    private var cache:Map<String, Dynamic>;
    private var bundle:StyleBundle;

    public function new(?font:ThemeFont, ?colors:ThemeColors, ?fontSize:ThemeFontSize) {
        updateSignal = new Signal0();
        cache = new Map();
        bundle = new StyleBundle();
        this.font = font;
        this.colors = colors;
        this.fontSize = fontSize;
        L.d("Theme", 'font: ${this.font}');
        L.d("Theme", 'colors: ${this.colors}');
    }

    private function set_font(value:ThemeFont):ThemeFont {
        font = resolveFont(value);
        _reload();
        return font;
    }

    private function set_fontSize(value:ThemeFontSize):ThemeFontSize {
        fontSize = resolveFontSize(value);
        _reload();
        return fontSize;
    }

    private function set_colors(value:ThemeColors):ThemeColors {
        colors = resolveColors(value);
        _reload();
        return colors;
    }

    private function _reload():Void {
        if (colors == null || font == null || fontSize == null) {
            return;
        }
        reload();
        cache = new Map();
        // ToDo: hardcode update views
        if (Root.instance != null) {
            for (view in Root.instance.views()) {
                view.style = view.style;
            }
        }
        updateSignal.emit();
    }

    private function register(style:Style):Void {
        bundle.register(style);
    }

    private function reload():Void {
        register(new Style("background", [
            "skin.background.color" => colors.dark,
        ]));
        register(new Style("border", [
            "skin.border.color" => colors.border,
        ]));
        register(new Style("frame", [
            "geometry.padding" => Box.fromFloat(2),
        ], ["background", "border"]));
        register(new Style("text", [
            "font.color" => colors.text,
            "font.family" => font.name,
            "font.embed" => font.embed,
            "font.size" => fontSize.base,
        ]));
        register(new Style("text0", [
            "skin.background.color" => colors.light.diff(-16),
        ], ["text"]));
        register(new Style("text1", [
            "skin.background.color" => colors.light.diff(16),
        ], ["text"]));
        register(new Style("text0.selected", [
            "font.color" => colors.active,
        ], ["text0"]));
        register(new Style("text1.selected", [
            "font.color" => colors.active,
        ], ["text1"]));
        register(new Style("label", [
            "geometry.padding" => Box.fromArray([8, 2]),
        ], ["text"]));
        register(new Style("input", [
            "geometry.height" => SizeValue.fromInt(26),
        ], ["label", "border"]));
        register(new Style("button", [
            "skin.color" => colors.light,
            "geometry.padding" => Box.fromArray([25, 8]),
        ], ["text"]));
        register(new Style("button.active", [
            "skin.borderColor" => colors.active,
            "font.color" => colors.active,
        ], ["button"]));
        register(new Style("button.tab", [
            "skin" => function() return new TabColorSkin(),
        ], ["button"]));
        register(new Style("dropdown", [
            "_" => null,
        ], ["background", "border"]));
        register(new Style("scroll.vertical", [
            "skin.foreColor" => colors.light,
            "skin.backColor" => colors.dark,
            "geometry.width" => SizeValue.fromFloat(10),
            "geometry.height" => SizeValue.fromString("100%"),
            "geometry.hAlign" => HAlign.RIGHT,
            "geometry.vAlign" => VAlign.TOP,
        ]));
        register(new Style("scroll.horizontal", [
            "skin.foreColor" => colors.light,
            "skin.backColor" => colors.dark,
            "geometry.width" => SizeValue.fromString("100%"),
            "geometry.height" => SizeValue.fromFloat(10),
            "geometry.hAlign" => HAlign.LEFT,
            "geometry.vAlign" => VAlign.BOTTOM,
        ]));
    }

    private function _resolve<T>(key:String, style:StyleId):Null<T> {
        return bundle.get(key, style);
    }

    public function resolve<T>(key:String, style:StyleId):T {
        var result:Dynamic = null;
        if (style != null) {
            var k = key + "." + style;
            if (cache.exists(k)) {
                result = cache.get(k);
            } else {
                result = _resolve(key, style);
                cache.set(k, result);
            }
        }
        if (Reflect.isFunction(result)) {
            result = result();
        }
        return result;
    }

    private static function resolveFont(font:ThemeFont):ThemeFont {
        if (font == null) {
            font = {};
        }
        var fonts = Font.enumerateFonts(!font.embed);
        if (font.name == null) {
            var flashFont = Font.enumerateFonts(!font.embed)[0];
            font = {
                name: flashFont.fontName,
                embed: switch flashFont.fontType {
                    case DEVICE: false;
                    case _: true;
                }
            }
        }
        return font;
    }

    private static function resolveFontSize(fontSize:ThemeFontSize):ThemeFontSize {
        if (fontSize == null) {
            fontSize = {};
        }
        return {
            base: fontSize.base != null ? fontSize.base : 18,
            big: fontSize.big != null ? fontSize.big : 22,
            veryBig: fontSize.veryBig != null ? fontSize.big : 24,
        }
    }

    private static function resolveColors(colors:ThemeColors):ThemeColors {
        if (colors == null) {
            colors = {};
        }
        var light:Color = colors.light != null ? colors.light : 0x33aa33;
        var dark:Color = colors.dark != null ? colors.dark : light.diff(-64);
        var text:Color = colors.text != null ? colors.text : 0xffffff;
        var border:Color = colors.border != null ? colors.border : light.multiply(1.5);
        var active:Color = colors.active != null ? colors.active : 0x00ff00;
        return {
            light: light,
            dark: dark,
            text: text,
            border: border,
            active: active,
        }
    }
}
