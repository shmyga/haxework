package hw.view.theme;

import hw.signal.Signal;
import hw.color.Color;
import hw.view.geometry.SizeValue;

typedef ThemeFont = {
    @:optional var name:String;
    @:optional var embed:Bool;
}

typedef ThemeColors = {
    @:optional var light:Color;
    @:optional var dark:Color;
    @:optional var text:Color;
    @:optional var border:Color;
    @:optional var active:Color;
}

typedef ThemeFontSize = {
    @:optional var base:SizeValue;
    @:optional var big:SizeValue;
    @:optional var veryBig:SizeValue;
}

interface ITheme {
    public var font(default, set):ThemeFont;
    public var fontSize(default, set):ThemeFontSize;
    public var colors(default, set):ThemeColors;
    public var updateSignal(default, null):Signal0;
    public function resolve<T>(key:String, style:StyleId):T;
}
