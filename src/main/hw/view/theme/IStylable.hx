package hw.view.theme;

interface IStylable {
    public var styleKey(default, set):String;
    public var style(default, set):StyleId;
}
