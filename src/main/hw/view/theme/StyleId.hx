package hw.view.theme;

abstract StyleId(Array<String>) {

    public function new(value:Array<String>) {
        this = value != null ? value : [];
    }

    public function add(value:String):Bool {
        if (this.indexOf(value) == -1) {
            this.push(value);
            return true;
        }
        return false;
    }

    public inline function remove(value:String):Bool {
        return this.remove(value);
    }

    public function match(value:StyleId):Bool {
        for (v in value.toArray()) {
            if (this.indexOf(v) == -1) {
                return false;
            }
        }
        return true;
    }

    @:to public inline function toArray():Array<String> {
        return this;
    }

    @:to public inline function toString():String {
        return this.join(".");
    }

    @:from public static inline function fromArray(value:Array<String>):StyleId {
        return new StyleId(value);
    }

    @:from public static inline function fromString(value:String):StyleId {
        return new StyleId(value.split("."));
    }
}
