package hw.view;

import flash.events.Event;
import flash.display.Stage;

class ViewUpdater {
    public var stage(null, set):Stage;
    private var updateViews:Array<IView<Dynamic>>;
    private var redrawViews:Array<IView<Dynamic>>;

    public function new() {
        updateViews = [];
        redrawViews = [];
    }

    private function set_stage(value:Stage):Stage {
        value.addEventListener(Event.ENTER_FRAME, onEnterFrame);
        return value;
    }

    public function toUpdate(view:IView<Dynamic>):Void {
        if (updateViews.indexOf(view) == -1) updateViews.push(view);
    }

    public function toRedraw(view:IView<Dynamic>):Void {
        if (redrawViews.indexOf(view) == -1) redrawViews.push(view);
    }

    private function onEnterFrame(_):Void {
        update();
        redraw();
        if (updateViews.length > 0) {
            update();
            redraw();
        }
    }

    public function isUpdate(view:IView<Dynamic>):Bool {
        return updateViews.indexOf(view) > -1;
    }

    public function update():Void {
        while (updateViews.length > 0) {
            var v = updateViews.shift();
            v.update();
        }
    }

    public function redraw():Void {
        while (redrawViews.length > 0) {
            var v = redrawViews.shift();
            v.redraw();
        }
    }
}
