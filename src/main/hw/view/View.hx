package hw.view;

import flash.Lib;
import flash.display.Stage;
import flash.display.DisplayObject;
import flash.display.InteractiveObject;
import flash.geom.Rectangle;
import hw.view.geometry.Geometry;
import hw.view.geometry.SizeSet;
import hw.view.group.IGroupView;
import hw.view.skin.ISkin;
import hw.view.theme.ITheme;

@:style class View<C:DisplayObject> implements IView<C> {

    private static var counter:Int = 0;
    public static var updater(default, null):ViewUpdater = new ViewUpdater();

    @:provide var theme:ITheme;

    @:style public var geometry(default, default):Geometry;
    @:style public var skin(default, default):ISkin<Dynamic>;

    public var id(default, default):String;

    public var x(default, set):Float;
    public var y(default, set):Float;

    public var width(default, null):Float;
    public var height(default, null):Float;

    public var size(default, null):SizeSet;

    public var content(default, null):C;

    public var parent(default, null):Null<IGroupView>;

    public var visible(default, set):Bool;
    public var index(default, set):Int;
    public var mouseEnabled(default, set):Bool = true;

    public var rect(get, null):Rectangle;

    private var stage(get, null):Stage;

    private function get_stage():Stage {
        return Lib.current.stage;
    }

    public function new(content:C) {
        id = Type.getClassName(Type.getClass(this)) + counter++;
        size = new SizeSet();
        this.content = content;
        x = 0;
        y = 0;
        width = 1;
        height = 1;
        geometry = new Geometry();
        visible = true;
        index = -1;
    }

    private function onStyle():Void {
        toUpdate();
        toRedraw();
    }

    public function toRedraw():Void {
        updater.toRedraw(this);
    }

    public function toUpdate():Void {
        updater.toUpdate(this);
    }

    public function toUpdateParent():Void {
        if (parent != null) {
            updater.toUpdate(parent);
        }
    }

    public function update():Void {
        setSize(
            geometry.width.fixed - geometry.padding.horizontal,
            geometry.height.fixed - geometry.padding.vertical,
            "geometry"
        );
    }

    public function redraw():Void {
        if (skin != null) {
            skin.draw(this);
        }
    }

    public function setSize(width:Float, height:Float, type:String = "default"):Void {
        if (size.update([width, height], type)) {
            var s = size.toSize();
            this.width = s.width + geometry.padding.horizontal;
            this.height = s.height + geometry.padding.vertical;
            toUpdateParent();
            toRedraw();
        }
    }

    public function remove():Void {
        if (parent != null) parent.removeView(this);
    }

    private function set_x(value:Float):Float {
        if (x != value) {
            x = content.x = value;
        }
        return x;
    }

    private function set_y(value:Float):Float {
        if (y != value) {
            y = content.y = value;
        }
        return y;
    }

    private function set_visible(value:Bool):Bool {
        if (visible != value) {
            visible = value;
            if (content != null) content.visible = visible;
        }
        return visible;
    }

    private function set_index(value:Int):Int {
        if (index != value) {
            index = value;
            toUpdateParent();
        }
        return index;
    }

    private function set_mouseEnabled(value:Bool):Bool {
        if (mouseEnabled != value) {
            mouseEnabled = value;
            if (content != null && Std.is(content, InteractiveObject)) {
                cast(content, InteractiveObject).mouseEnabled = mouseEnabled;
            }
        }
        return mouseEnabled;
    }

    private function get_rect():Rectangle {
        var x = this.x;
        var y = this.y;
        if (parent != null) {
            var rect = parent.rect;
            x += rect.x;
            y += rect.y;
        }
        return new Rectangle(x, y, width, height);
    }
}
