package hw.view.skin;

import flash.display.BitmapData;
import flash.geom.Rectangle;
import hw.view.form.ButtonView.ButtonState;
import hw.view.form.ButtonView;
import hw.view.utils.BitmapUtil;
import hw.view.utils.DrawUtil;

@:style class ButtonBitmapSkin implements ISkin<ButtonView> {

    public var fillType(default, default):FillType;
    public var color(default, default):Int;
    public var image(null, set):BitmapData;
    public var upImage(null, set):BitmapData;
    public var overImage(null, set):BitmapData;
    public var downImage(null, set):BitmapData;
    public var disableImage(null, default):BitmapData;

    private var images:Map<ButtonState, BitmapData>;
    private var disable:BitmapData;

    public function new(image:BitmapData = null, fillType:FillType = null, color = -1) {
        images = new Map<ButtonState, BitmapData>();
        if (image != null) {
            this.image = image;
        }
        this.fillType = fillType;
        this.color = color;
    }

    private function set_image(value:BitmapData):BitmapData {
        images.set(ButtonState.UP, value);
        images.set(ButtonState.DOWN, BitmapUtil.multiply(value, 0.8));
        images.set(ButtonState.OVER, BitmapUtil.multiply(value, 1.2));
        disable = BitmapUtil.grayscale(value, 0.2);
        return value;
    }

    private function set_upImage(value:BitmapData):BitmapData {
        images.set(ButtonState.UP, value);
        return value;
    }

    private function set_overImage(value:BitmapData):BitmapData {
        images.set(ButtonState.OVER, value);
        return value;
    }

    private function set_downImage(value:BitmapData):BitmapData {
        images.set(ButtonState.DOWN, value);
        return value;
    }

    public function draw(view:ButtonView):Void {
        if (images == null) return;
        var image:BitmapData = view.disabled ? disableImage == null ? disable : disableImage : images.get(view.state);
        DrawUtil.draw(view.content.graphics, image, new Rectangle(0, 0, view.width, view.height), fillType, color);
    }
}
