package hw.view.skin;

import flash.display.BitmapData;
import hw.color.Color;
import hw.view.form.ButtonView;
import hw.view.utils.DrawUtil;

class Skin {

    public static function transparent():ISkin<SpriteView> {
        return new SpriteSkin(new Background(0, 0));
    }

    public static function bitmap(image:BitmapData, fillType:FillType = null):ISkin<SpriteView> {
        return new BitmapSkin(image, fillType);
    }

    public static function color(color:Int, alpha:Float = 1.0):ISkin<SpriteView> {
        return new SpriteSkin(new Background(color, alpha));
    }

    public static function buttonColor(color:Color, ?borderColor:Color):ISkin<ButtonView> {
        return new ButtonColorSkin(color, borderColor);
    }

    public static function buttonBitmap(image:BitmapData, fillType:FillType = null):ISkin<ButtonView> {
        return new ButtonBitmapSkin(image, fillType);
    }

    public static function tabColor(color:Int):ISkin<ButtonView> {
        return new TabColorSkin(color);
    }

    public static function scrollHorizontal(foreColor:Int, backColor:Int) {
        return new HScrollBarSkin(foreColor, backColor);
    }

    public static function scrollVertical(foreColor:Int, backColor:Int) {
        return new VScrollBarSkin(foreColor, backColor);
    }
}
