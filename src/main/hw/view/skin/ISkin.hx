package hw.view.skin;

import hw.view.theme.IStylable;

interface ISkin<V:IView<Dynamic>> extends IStylable {
    public function draw(view:V):Void;
}
