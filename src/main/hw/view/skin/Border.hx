package hw.view.skin;

import hw.color.Color;

@:style class Border {
    @:style(null) public var color(default, default):Null<Color>;
    @:style(2) public var thickness(default, default):Null<Float>;
    @:style(1) public var alpha(default, default):Null<Float>;

    public function new(?color:Color, ?thickness:Float, ?alpha:Float) {
        this.color = color;
        this.thickness = thickness;
        this.alpha = alpha;
    }
}
