package hw.view.skin;

import flash.display.BitmapData;
import flash.geom.Rectangle;
import hw.view.utils.DrawUtil;

@:style(true) class BitmapSkin extends SpriteSkin {
    @:style(null) public var image(default, default):BitmapData;
    @:style(FillType.DEFAULT) public var fillType(default, default):FillType;

    public var content:Bool;

    public function new(?image:BitmapData, ?fillType, ?background:Background, ?border:Border) {
        super(background, border);
        if (image != null) {
            this.image = image;
        }
        this.fillType = fillType;
    }

    override public function draw(view:SpriteView):Void {
        super.draw(view);
        if (image == null) return;
        var rect = new Rectangle(
        view.geometry.padding.left,
        view.geometry.padding.top,
        view.width - view.geometry.padding.horizontal,
        view.height - view.geometry.padding.vertical
        );
        DrawUtil.draw(view.content.graphics, image, rect, fillType, -1, false);
    }
}
