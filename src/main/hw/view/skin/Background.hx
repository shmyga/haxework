package hw.view.skin;

import hw.color.Color;

@:style class Background {
    @:style(null) public var color(default, default):Null<Color>;
    @:style(1) public var alpha(default, default):Null<Float>;

    public function new(?color:Color, ?alpha:Float) {
        this.color = color;
        this.alpha = alpha;
    }
}
