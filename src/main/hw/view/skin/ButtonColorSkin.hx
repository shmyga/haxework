package hw.view.skin;

import flash.display.CapsStyle;
import flash.display.Graphics;
import flash.display.JointStyle;
import flash.display.LineScaleMode;
import hw.color.Color;
import hw.view.form.ButtonView;
import hw.view.form.ToggleButtonView;

using hw.color.ColorUtil;

@:style class ButtonColorSkin implements ISkin<ButtonView> {

    @:style(0xffffff) public var color(default, default):Null<Color>;
    @:style(null) public var borderColor(default, default):Null<Color>;
    @:style(15) public var round(default, default):Null<Float>;
    private var colors:Map<ButtonState, Int>;

    public function new(?color:Color, ?borderColor:Color, ?round:Float) {
        this.color = color;
        this.borderColor = borderColor;
        this.round = round;
    }

    public function draw(view:ButtonView):Void {
        var color:Color = stateColor(color, view.state);
        var borderColor:Color = borderColor != null ? borderColor : color.multiply(1.5);
        if (Std.is(view, ToggleButtonView)) {
            if (!cast(view, ToggleButtonView).on) {
                color = color.multiply(0.5);
            }
        }
        var graphics:Graphics = view.content.graphics;
        graphics.beginFill(color, 1.0);
        graphics.lineStyle(2, borderColor, 1, true, LineScaleMode.NONE, CapsStyle.ROUND, JointStyle.ROUND);
        graphics.drawRoundRect(1, 1, view.width - 2, view.height - 2, round, round);
        graphics.lineStyle();
        graphics.endFill();
    }

    private static function stateColor(color:Color, state:ButtonState):Color {
        return switch state {
            case UP: color;
            case DOWN: color.diff(-24);
            case OVER: color.diff(24);
            case DISABLED: color.multiply(0.6);
        }
    }
}
