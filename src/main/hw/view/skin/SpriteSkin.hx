package hw.view.skin;

import flash.display.Graphics;

@:style class SpriteSkin implements ISkin<SpriteView> {

    @:style public var border(default, default):Border;
    @:style public var background(default, default):Background;
    @:style(null) public var round(default, default):Null<Float>;

    public function new(?background:Background, ?border:Border, ?round:Float) {
        this.background = background != null ? background : new Background();
        this.border = border != null ? border : new Border();
        this.round = round;
    }

    public function draw(view:SpriteView):Void {
        var graphics:Graphics = view.content.graphics;
        graphics.clear();
        if (border.color != null) {
            graphics.lineStyle(border.thickness, border.color, border.alpha, true);
        }
        if (background.color != null) {
            graphics.beginFill(background.color, background.alpha);
        } else {
            graphics.beginFill(0, 0);
        }
        var o = border.color != null ? border.thickness / 2 : 0;
        if (round != null) {
            graphics.drawRoundRect(o, o, view.width - o * 2, view.height - o * 2, round, round);
        } else {
            graphics.drawRect(o, o, view.width - o * 2, view.height - o * 2);
        }
        graphics.lineStyle();
        graphics.endFill();
    }
}
