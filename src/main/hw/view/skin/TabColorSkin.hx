package hw.view.skin;

import flash.display.Graphics;
import hw.color.ColorUtil;
import hw.view.form.ButtonView;
import hw.view.form.ToggleButtonView;

class TabColorSkin extends ButtonColorSkin {

    override public function draw(view:ButtonView):Void {
        var color:Int = ButtonColorSkin.stateColor(color, view.state);
        if (Std.is(view, ToggleButtonView)) {
            if (!cast(view, ToggleButtonView).on) {
                color = ColorUtil.multiply(color, 0.5);
            }
        }
        var graphics:Graphics = view.content.graphics;
        graphics.beginFill(color, 1.0);
        graphics.lineStyle(2, ColorUtil.multiply(color, 1.5));
        graphics.drawRoundRectComplex(1, 1, view.width - 2, view.height - 2, 5, 5, 0, 0);
        graphics.lineStyle();
        graphics.endFill();
    }
}
