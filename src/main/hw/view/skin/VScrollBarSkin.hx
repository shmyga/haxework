package hw.view.skin;

import hw.color.Color;
import hw.color.ColorUtil;
import hw.view.list.ScrollBarView;

@:style class VScrollBarSkin implements ISkin<ScrollBarView> {
    @:style(0) public var foreColor(default, default):Null<Color>;
    @:style(0) public var backColor(default, default):Null<Color>;

    public function new(?foreColor:Color, ?backColor:Color) {
        this.foreColor = foreColor;
        this.backColor = backColor;
    }

    public function draw(view:ScrollBarView):Void {
        if (view.ratio < 1) {
            var graphics = view.content.graphics;
            var size = Math.max(view.height * view.ratio, 15);
            var position = Math.min(view.height * view.position, view.height - size);
            graphics.beginFill(backColor);
            graphics.lineStyle(2, ColorUtil.multiply(foreColor, 1.5));
            graphics.drawRect(0, 0, view.width, view.height);
            graphics.beginFill(foreColor);
            graphics.lineStyle();
            if (!Math.isNaN(position) && !Math.isNaN(size)) {
                graphics.drawRect(0, position, view.width, size);
            }
            graphics.endFill();
        }
    }
}
