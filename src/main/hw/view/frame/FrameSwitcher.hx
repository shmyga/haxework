package hw.view.frame;

import hw.animate.IAnimate;
import hw.signal.Signal;
import hw.view.group.GroupView;
import hw.view.IView;

class FrameSwitcher extends GroupView {

    public var current(default, null):Null<FrameView<Dynamic>>;
    public var onSwitch:Signal<FrameView<Dynamic>> = new Signal();

    public var factory(default, default):Map<String, Class<FrameView<Dynamic>>>;

    public var frames(default, default):Map<String, FrameView<Dynamic>>;

    public var animateFactory(default, default):Class<IAnimate>;

    private var animate:IAnimate;
    private var history:Array<{id:String, data:Dynamic}>;

    public function new() {
        super();
        factory = new Map();
        frames = new Map();
        history = [];
        current = null;
    }

    private function buildAnimate(view:IView<Dynamic>):Null<IAnimate> {
        if (animateFactory != null) {
            return Type.createInstance(animateFactory, [view]);
        }
        return null;
    }

    private function resolveFrame<T>(id:String):FrameView<T> {
        if (!frames.exists(id)) {
            if (!factory.exists(id)) {
                throw 'frame "$id" not found';
            }
            frames.set(id, Type.createInstance(factory.get(id), []));
        }
        return cast frames.get(id);
    }

    public function change<T>(id:String, data:T = null):FrameView<T> {
        var prev = null;
        if (current != null) {
            if (current.frameId == id) {
                current.onShow(data);
                return cast current;
            }
            prev = current;
        }
        current = resolveFrame(id);
        if (current == null) {
            throw 'frame "$id" not found';
        }
        if (animate != null) {
            animate.cancel();
        }
        animate = buildAnimate(current);
        if (animate != null && prev != null) {
            animate.start().then(_ -> removePrev(prev));
        } else {
            removePrev(prev);
        }
        addView(current);
        toUpdate();
        update();
        focus(current);
        current.onShow(data);
        history.push({id:current.frameId, data:data});
        onSwitch.emit(current);
        return cast current;
    }

    public function back():Void {
        if (history.length > 1) {
            history.pop();
            var item = history.pop();
            change(item.id, item.data);
        }
    }

    private function removePrev(prev:Null<FrameView<Dynamic>>):Void {
        if (prev != null) {
            prev.onHide();
            removeView(prev);
        }
    }
}
