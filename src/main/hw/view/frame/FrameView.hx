package hw.view.frame;

import hw.view.group.GroupView;
import hw.view.layout.ILayout;
import hw.view.layout.VerticalLayout;

class FrameView<D> extends GroupView {
    public var frameId(default, null):String;

    public function new(frameId:String, ?layout:ILayout) {
        super(layout != null ? layout : new VerticalLayout());
        style = "frame";
        this.frameId = frameId;
        this.geometry.stretch = true;
    }

    public function onShow(data:D):Void {}

    public function onHide():Void {}
}
