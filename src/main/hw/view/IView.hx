package hw.view;

import flash.display.DisplayObject;
import flash.geom.Rectangle;
import hw.view.geometry.Geometry;
import hw.view.geometry.SizeSet;
import hw.view.group.IGroupView;
import hw.view.skin.ISkin;
import hw.view.theme.StyleId;

interface IView<C:DisplayObject> {
    public var geometry(default, set):Geometry;
    public var skin(default, set):ISkin<Dynamic>;

    public var id(default, default):String;

    public var x(default, set):Float;
    public var y(default, set):Float;

    public var width(default, null):Float;
    public var height(default, null):Float;

    public var size(default, null):SizeSet;

    public var style(default, set):StyleId;

    public var content(default, null):C;

    public var parent(default, null):Null<IGroupView>;

    public var visible(default, set):Bool;
    public var index(default, set):Int;
    public var mouseEnabled(default, set):Bool;

    public var rect(get, null):Rectangle;

    public function update():Void;

    public function redraw():Void;

    public function toUpdate():Void;

    public function toUpdateParent():Void;

    public function toRedraw():Void;

    public function remove():Void;

    public function setSize(width:Float, height:Float, type:String = "default"):Void;
}
