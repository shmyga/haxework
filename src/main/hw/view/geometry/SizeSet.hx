package hw.view.geometry;

import haxe.ds.StringMap;

class SizeSet extends StringMap<Size> {

    public function new() {
        super();
    }

    public function update(value:Size, type:String = "default"):Bool {
        var existValue:Size = get(type);
        if (existValue == null || value.width != existValue.width || value.height != existValue.height) {
            set(type, value);
            return true;
        }
        return false;
    }

    public function toSize(?exclude:String):Size {
        var result:Size = 0;
        for (type in keys()) {
            if (exclude == null || type.indexOf(exclude) == -1) {
                var value = get(type);
                result.width = Math.max(result.width, value.width);
                result.height = Math.max(result.height, value.height);
            }
        }
        return result;
    }
}
