package hw.view.geometry;

@:enum abstract VAlign(String) from String to String {
    var NONE = "none";
    var TOP = "top";
    var MIDDLE = "middle";
    var BOTTOM = "bottom";
}
