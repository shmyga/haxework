package hw.view.geometry;

@:enum abstract Position(String) from String to String {
    var LAYOUT = "layout";
    var ABSOLUTE = "absolute";
}
