package hw.view.geometry;

@:style class Geometry {
    @:style(0) public var width(default, default):SizeValue;
    @:style(0) public var height(default, default):SizeValue;
    @:style(0) public var padding(default, default):Box;
    @:style(0) public var margin(default, default):Box;
    @:style(HAlign.NONE) public var hAlign(default, default):HAlign;
    @:style(VAlign.NONE) public var vAlign(default, default):VAlign;
    @:style(Position.LAYOUT) public var position(default, default):Position;
    @:style(-1) public var ratio(default, default):Null<Float>;
    public var stretch(null, set):Bool;

    public function new() {
    }

    private function set_stretch(value:Bool):Bool {
        if (value) {
            width.percent = 100;
            height.percent = 100;
        }
        return value;
    }

    public function setSize(width:SizeValue, height:SizeValue):Geometry {
        this.width = width;
        this.height = height;
        return this;
    }

    public function setMargin(margin:Box):Geometry {
        this.margin = margin;
        return this;
    }

    public function setPadding(padding:Box):Geometry {
        this.padding = padding;
        return this;
    }

    public function setAlign(hAlign:HAlign, vAlign:VAlign):Geometry {
        this.hAlign = hAlign;
        this.vAlign = vAlign;
        return this;
    }

    public function setPosition(position:Position):Geometry {
        this.position = position;
        return this;
    }

    public function setRatio(ratio:Float):Geometry {
        this.ratio = ratio;
        return this;
    }
}
