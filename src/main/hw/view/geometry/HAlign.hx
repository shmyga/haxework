package hw.view.geometry;

@:enum abstract HAlign(String) from String to String {
    var NONE = "none";
    var LEFT = "left";
    var CENTER = "center";
    var RIGHT = "right";
}
