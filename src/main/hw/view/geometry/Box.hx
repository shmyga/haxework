package hw.view.geometry;

abstract Box(Array<Float>) {
    public var left(get, set):Float;
    public var right(get, set):Float;
    public var top(get, set):Float;
    public var bottom(get, set):Float;
    public var vertical(get, never):Float;
    public var horizontal(get, never):Float;
    public var empty(get, never):Bool;

    inline public function new(value:Array<Float> = null) {
        this = switch value {
            case [a]: [a, a, a, a];
            case [a, b]: [a, a, b, b];
            case [a, b, c, d]: [a, b, c, d];
            case _: [0, 0, 0, 0];
        }
    }

    inline private function get_left():Float {
        return this[0];
    }

    inline private function set_left(value:Float):Float {
        return this[0] = value;
    }

    inline private function get_right():Float {
        return this[1];
    }

    inline private function set_right(value:Float):Float {
        return this[1] = value;
    }

    inline private function get_top():Float {
        return this[2];
    }

    inline private function set_top(value:Float):Float {
        return this[2] = value;
    }

    inline private function get_bottom():Float {
        return this[3];
    }

    inline private function set_bottom(value:Float):Float {
        return this[3] = value;
    }

    inline private function get_vertical():Float {
        return top + bottom;
    }

    inline private function get_horizontal():Float {
        return left + right;
    }

    inline private function get_empty():Bool {
        return switch this {
            case [0, 0, 0, 0]: true;
            case _: false;
        }
    }

    public function clone():Box {
        return new Box(this);
    }

    @:from static public function fromArray(value:Array<Float>):Box {
        return new Box(value);
    }

    @:from static public function fromFloat(value:Float):Box {
        return new Box([value]);
    }
}
