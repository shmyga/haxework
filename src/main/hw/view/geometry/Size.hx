package hw.view.geometry;

abstract Size(Array<Float>) {
    public var width(get, set):Float;
    public var height(get, set):Float;
    public var empty(get, never):Bool;

    inline public function new(value:Array<Float>) {
        this = switch(value) {
            case []: [-1, -1];
            case [a]: [a, a];
            case [a, b]: [a, b];
            case x: x;
        }
    }

    inline private function get_width():Float {
        return this[0];
    }

    inline private function set_width(value:Float):Float {
        return this[0] = value;
    }

    inline private function get_height():Float {
        return this[1];
    }

    inline private function set_height(value:Float):Float {
        return this[1] = value;
    }

    inline private function get_empty():Bool {
        return switch this {
            case [-1, -1]: true;
            case _: false;
        }
    }

    @:to public inline function toArray():Array<Float> {
        return this;
    }

    @:from static public inline function fromArray(value:Array<Float>):Size {
        return new Size(value);
    }

    @:from static public inline function fromFloat(value:Float):Size {
        return new Size([value]);
    }
}
