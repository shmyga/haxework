package hw.view.geometry;

enum SizeType {
    FIXED;
    PERCENT;
    DISPLAY_WIDTH;
    DISPLAY_HEIGHT;
}

@:singleton class DisplaySize {
    public var width:Int;
    public var height:Int;

    public function new(width:Int = 0, height:Int = 0) {
        this.width = width;
        this.height = height;
    }

    public function update(width:Int, height:Int) {
        this.width = width;
        this.height = height;
    }
}

abstract SizeValue({type:SizeType, value:Float}) {
    public static final PERCENT_TYPE_CHAR:String = "%";
    public static final DISPLAY_WIDTH_TYPE_CHAR:String = "w";
    public static final DISPLAY_HEIGHT_TYPE_CHAR:String = "h";
    
    public var type(get, set):SizeType;
    public var value(get, set):Float;

    public var fixed(get, set):Float;
    public var percent(get, set):Float;

    public function new(type:SizeType, value:Float) {
        this = {type: type, value: value};
    }

    private inline function get_type():SizeType {
        return this.type;
    }

    private inline function set_type(value:SizeType):SizeType {
        return this.type = value;
    }

    private inline function get_value():Float {
        return this.value;
    }

    private inline function set_value(value:Float):Float {
        return this.value = value;
    }

    private inline function get_fixed():Float {
        return switch type {
            case FIXED: value;
            case DISPLAY_WIDTH: value * (DisplaySize.instance.width / 100);
            case DISPLAY_HEIGHT: value * (DisplaySize.instance.height / 100);
            case _: 0;
        }
    }

    private inline function set_fixed(value:Float):Float {
        this.type = FIXED;
        return this.value = value;
    }

    private inline function get_percent():Float {
        return switch type {
            case PERCENT: value;
            case _: 0;
        }
    }

    private inline function set_percent(value:Float):Float {
        this.type = PERCENT;
        return this.value = value;
    }

    @:from static public inline function fromInt(value:Int):SizeValue {
        return new SizeValue(FIXED, value);
    }

    @:from static public inline function fromFloat(value:Float):SizeValue {
        return new SizeValue(FIXED, value);
    }

    @:from static public inline function fromString(value:String):SizeValue {
        var typeChar = value.substr(value.length - 1);
        if (typeChar == PERCENT_TYPE_CHAR) {
            return new SizeValue(PERCENT, Std.parseFloat(value));
        } else if (typeChar == DISPLAY_WIDTH_TYPE_CHAR) {
            return new SizeValue(DISPLAY_WIDTH, Std.parseFloat(value));
        } else if (typeChar == DISPLAY_HEIGHT_TYPE_CHAR) {
            return new SizeValue(DISPLAY_HEIGHT, Std.parseFloat(value));
        } else {
            return new SizeValue(FIXED, Std.parseFloat(value));
        }
    }
}
