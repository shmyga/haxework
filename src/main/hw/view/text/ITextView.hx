package hw.view.text;

import flash.text.TextField;
import hw.view.IView;
import hw.view.layout.ILayout;

interface ITextView extends IView<Dynamic> {
    public var font(default, set):FontPreset;
    public var layout(default, default):ILayout;

    public var textField(default, null):TextField;
    public var text(get, set):String;
}
