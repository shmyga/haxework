package hw.view.text;

import hw.color.Color;
import hw.view.geometry.SizeValue;
import flash.text.TextFormatAlign;

@:style class FontPreset {
    @:style("Courirer") public var family(default, default):String;
    @:style(false) public var embed(default, default):Null<Bool>;
    @:style(0xffffff) public var color(default, default):Null<Color>;
    @:style(16) public var size(default, default):Null<SizeValue>;
    @:style(false) public var bold(default, default):Null<Bool>;
    @:style(TextFormatAlign.LEFT) public var align(default, default):TextFormatAlign;

    public function new(?family:String, ?embed:Bool, ?color:Color, ?size:SizeValue,
                        ?bold:Bool, ?align:TextFormatAlign) {
        this.family = family;
        this.embed = embed;
        this.color = color;
        this.size = size;
        this.bold = bold;
        this.align = align;
    }
}
