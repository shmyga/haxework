package hw.view.text;

import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;
import flash.text.TextFormatAlign;
import hw.text.BitmapTextField;
import hw.text.TextUtil;
import hw.view.geometry.HAlign;
import hw.view.geometry.VAlign;
import hw.view.layout.ILayout;
import hw.view.layout.Layout;

@:style(true) class TextView extends SpriteView implements ITextView {
    @:style public var font(default, default):FontPreset;
    public var layout(default, default):ILayout;

    public var textField(default, null):TextField;
    public var text(get, set):String;

    private var _text:String;

    public var fill(default, set):Bool = true;

    public var shadow(default, set):Bool;
    public var shadowColor(default, set):Int;

    public function new() {
        super();
        font = new FontPreset();
        style = "text";
        layout = new Layout();
        textField = buildTextField();
        textField.width = 100;
        textField.height = 100;
        textField.multiline = true;
        textField.wordWrap = true;
        #if dev_layout
        textField.borderColor = 0xff0000;
        textField.border = true;
        #end
        content.addChild(textField);
    }

    private function buildTextField():TextField {
        #if bitmap_text
        return new BitmapTextField();
        #else
        return new TextField();
        #end
    }

    private function set_fill(value:Bool):Bool {
        if (fill != value) {
            fill = value;
            toUpdate();
        }
        return fill;
    }

    private function get_text():String {
        return textField.text;
    }

    private function set_text(value:String):String {
        if (_text != value) {
            _text = value;
            toUpdate();
        }
        return _text;
    }

    private function currentText():String {
        return _text;
    }

    private function updateTextSize():Void {
        var size = TextUtil.getSize(textField);
        if (!Math.isNaN(size.x) && !Math.isNaN(size.y)/* && size.x > 0 && size.y > 0*/) {
            setSize(textField.wordWrap ? 0 : size.x, size.y, "text");
        }
    }

    override public function update():Void {
        //Kludge
        if (textField.wordWrap && width - geometry.padding.horizontal == 0) {
            super.update();
            return;
        }
        textField.embedFonts = font.embed;
        textField.defaultTextFormat = new TextFormat(
        font.family, Std.int(font.size.fixed), font.color,
        font.bold, false, false, null,
        font.align
        );
        textField.autoSize = fill ? TextFieldAutoSize.NONE : TextFieldAutoSize.LEFT;
        var t:String = currentText();
        if (t != null) textField.text = t;
        textField.setTextFormat(textField.defaultTextFormat);
        updateTextSize();
        placeTextField(textField);
        //ToDo:
        //var t:Point = content.localToGlobal(new Point(textField.x, textField.y));
        //t.x = Math.round(t.x);
        //t.y = Math.round(t.y);
        //t = content.globalToLocal(t);
        //textField.x = t.x;
        //textField.y = t.y;
        super.update();
    }

    private function placeTextField(textField:TextField):Void {
        textField.width = width;
        textField.height = size.exists("text") ? size.get("text").height : height;
        //textField.height = height;

        textField.x = switch (layout.hAlign) {
            case LEFT | NONE: geometry.padding.left;
            case CENTER: (width - textField.width) / 2 + geometry.padding.left - geometry.padding.right;
            case RIGHT: width - textField.width - geometry.padding.right;
            default: 0;
        }
        textField.y = switch (layout.vAlign) {
            case TOP | NONE: geometry.padding.top;
            case MIDDLE: (height - textField.height) / 2 + geometry.padding.top - geometry.padding.bottom;
            case BOTTOM: height - textField.height - geometry.padding.bottom;
            default: 0;
        }
    }

    override private function set_mouseEnabled(value:Bool):Bool {
        textField.mouseEnabled = value;
        return super.set_mouseEnabled(value);
    }

    private function set_shadow(value) {
        if (Std.is(textField, BitmapTextField)) {
            cast(textField, BitmapTextField).shadow = value;
            return cast(textField, BitmapTextField).shadow;
        } else {
            return value;
        }
    }

    private function set_shadowColor(value) {
        if (Std.is(textField, BitmapTextField)) {
            cast(textField, BitmapTextField).shadowColor = value;
            cast(textField, BitmapTextField).shadow = true;
            return cast(textField, BitmapTextField).shadowColor;
        } else {
            return value;
        }
    }
}
