package hw.view.list;

import hw.signal.Signal;
import hw.utils.NumberUtil;
import flash.geom.Point;
import flash.events.MouseEvent;

class ScrollBarView extends SpriteView {

    public var position(default, set):Float;
    public var ratio(default, set):Float;

    public var onScroll(default, null):Signal<Float> = new Signal();

    private var mousePosition:Float;

    public function new() {
        super();
        content.buttonMode = true;
        ratio = 1;
        position = 0;
        content.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDownEvent);
    }

    private function onMouseDownEvent(event:MouseEvent):Void {
        content.stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMoveEvent);
        content.stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUpEvent);
        var p:Point = content.globalToLocal(new Point(event.stageX, event.stageY));
        onMouseDown(p);
    }

    private function onMouseMoveEvent(event:MouseEvent):Void {
        var p:Point = content.globalToLocal(new Point(event.stageX, event.stageY));
        onMouseMove(p);
    }

    private function onMouseUpEvent(event:MouseEvent):Void {
        content.stage.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMoveEvent);
        content.stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUpEvent);
    }

    private function onMouseDown(p:Point):Void {}

    private function onMouseMove(p:Point):Void {}

    private function set_position(value:Float):Float {
        value = NumberUtil.limitFloat(value, 0, 1 - ratio);
        if (position != value) {
            position = value;
            toRedraw();
        }
        return position;
    }

    private function set_ratio(value:Float):Float {
        if (ratio != value) {
            ratio = value;
            toRedraw();
        }
        return ratio;
    }

    public function dispose():Void {
        onScroll.dispose();
    }
}
