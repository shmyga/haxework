package hw.view.list;

import hw.view.geometry.HAlign;
import hw.view.geometry.VAlign;
import hw.view.layout.HorizontalLayout;
import hw.view.layout.VerticalLayout;
import hw.view.list.ListView.IListItemView;

class VListView<D> extends ListView<D> {

    public function new() {
        super(new VerticalLayout(), new HorizontalLayout());
        box.layout.hAlign = CENTER;
        box.layout.vAlign = TOP;
    }

    override private function recalcSize(item:IListItemView<D>):Void {
        itemSize = item.geometry.height.fixed + item.geometry.margin.vertical + box.layout.margin;
        itemCount = Math.ceil(Math.max(0, box.height / itemSize)) + 2;
        sizeDiff = itemCount - ((box.height - box.layout.margin - 1) / itemSize);
    }

    override private function set_offsetDiff(value:Float):Float {
        box.geometry.padding.top = -value * itemSize;
        mask.geometry.margin.top = -box.geometry.padding.top;
        box.toUpdate();
        mask.toUpdate();
        return super.set_offsetDiff(value);
    }

    override private function onMouseWheel(value:Int):Void {
        offset = offset - value;
    }
}
