package hw.view.list;

import flash.geom.Point;
import hw.view.skin.HScrollBarSkin;

class HScrollBarView extends ScrollBarView {

    public function new() {
        super();
        skin = new HScrollBarSkin();
        style = "scroll.horizontal";
    }

    override private function onMouseDown(p:Point):Void {
        mousePosition = p.x - width * position;
    }

    override private function onMouseMove(p:Point):Void {
        position = (p.x - mousePosition) / width;
        onScroll.emit(position);
    }
}
