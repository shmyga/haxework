package hw.view.list;

import hw.view.geometry.HAlign;
import hw.view.geometry.VAlign;
import hw.view.layout.HorizontalLayout;
import hw.view.layout.VerticalLayout;
import hw.view.list.ListView.IListItemView;

class HListView<D> extends ListView<D> {

    public function new() {
        super(new VerticalLayout(), new HorizontalLayout());
        box.layout.hAlign = LEFT;
        box.layout.vAlign = MIDDLE;
    }

    override private function recalcSize(item:IListItemView<D>):Void {
        itemSize = item.geometry.width.fixed + item.geometry.margin.horizontal + box.layout.margin;
        itemCount = Math.ceil(Math.max(0, box.width / itemSize)) + 2;
        sizeDiff = itemCount - ((box.width - box.layout.margin - 1) / itemSize);
    }

    override private function set_offsetDiff(value:Float):Float {
        box.geometry.padding.left = -value * itemSize;
        mask.geometry.margin.left = -box.geometry.padding.left;
        box.toUpdate();
        mask.toUpdate();
        return super.set_offsetDiff(value);
    }

    override private function onMouseWheel(value:Int):Void {
        offset = offset - value;
    }
}
