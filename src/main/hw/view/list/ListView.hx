package hw.view.list;

import hw.view.group.Overflow;
import flash.events.MouseEvent;
import hw.signal.Signal;
import hw.utils.NumberUtil;
import hw.view.geometry.Position;
import hw.view.geometry.HAlign;
import hw.view.geometry.VAlign;
import hw.view.form.ButtonView;
import hw.view.group.GroupView;
import hw.view.layout.ILayout;
import hw.view.skin.Skin;

class ListView<D> extends GroupView {

    public var data(default, set):Array<D>;
    public var factory(null, default):Void->IListItemView<D>;

    public var offset(default, set):Int;
    private var offsetDiff(default, set):Float;

    private var itemCount(default, set):Int;
    private var sizeDiff:Float;

    public var onItemSelect(default, null):Signal<IListItemView<D>>;
    public var scroll(default, set):ScrollBarView;

    public var prev(default, set):ButtonView;
    public var next(default, set):ButtonView;

    public var filter(default, set):D -> Bool;
    private var filteredData:Array<D>;

    public var selected(default, set):Array<D>;

    private var main:GroupView;
    private var box:GroupView;
    private var mask:SpriteView;
    private var itemSize:Float;

    public var items(default, null):Array<IListItemView<D>>;
    private var itemsListeners:Map<IListItemView<D>, MouseEvent -> Void>;

    public function new(layout:ILayout, otherLayout:ILayout) {
        super(otherLayout);
        main = new GroupView(layout);
        main.layout.hAlign = CENTER;
        main.layout.vAlign = MIDDLE;
        main.geometry.stretch = true;
        addView(main);
        box = new GroupView(layout);
        box.geometry.stretch = true;
        box.overflow.y = CROP;
        main.addView(box);
        mask = new SpriteView();
        mask.geometry.stretch = true;
        mask.geometry.position = ABSOLUTE;
        mask.skin = Skin.color(0xffffff);
        box.content.mask = mask.content;
        box.addView(mask);
        onItemSelect = new Signal();
        itemSize = 0;
        offset = 0;
        offsetDiff = 0;
        sizeDiff = 0;
        items = [];
        itemsListeners = new Map<IListItemView<D>, MouseEvent -> Void>();
        selected = [];
        content.addEventListener(MouseEvent.MOUSE_WHEEL, _onMouseWheelEvent);
    }

    private function set_scroll(value:ScrollBarView):ScrollBarView {
        if (scroll != null) {
            scroll.onScroll.disconnect(onScroll);
            removeView(scroll);
        }
        scroll = value;
        if (scroll != null) {
            scroll.onScroll.connect(onScroll);
            addView(scroll);
        }
        toUpdate();
        return scroll;
    }

    private function set_prev(value:ButtonView):ButtonView {
        if (prev != null) {
            main.removeView(prev);
            prev.dispose();
        }
        prev = value;
        prev.onPress.connect(onPrevPress);
        main.addViewFirst(prev);
        return prev;
    }

    private function onPrevPress(_):Void {
        offset = offset - 1;
    }

    private function set_next(value:ButtonView):ButtonView {
        if (next != null) {
            main.removeView(next);
            next.dispose();
        }
        next = value;
        next.onPress.connect(onNextPress);
        main.addView(next);
        return next;
    }

    private function onNextPress(_):Void {
        offset = offset + 1;
    }

    public function onScroll(position:Float):Void {
        var x:Float = filteredData.length * position;
        var o:Int = Math.floor(x);
        offsetDiff = (x - o);
        offset = o;
    }

    private function _onMouseWheelEvent(event:MouseEvent):Void {
        #if flash event.preventDefault(); #end
        onMouseWheel(event.delta);
    }

    private function onMouseWheel(value:Int):Void {}

    private function set_offset(value:Int):Int {
        value = NumberUtil.limitInt(value, 0, filteredData == null ? 0 : filteredData.length - itemCount + 2);
        if (offset != value) {
            if (filteredData != null) {
                //ToDo: constant for 2
                if (value == 0) offsetDiff = 0;
                if (value == filteredData.length - itemCount + 2) offsetDiff = sizeDiff - 2;
            }
            offset = value;
            render();
        }
        return offset;
    }

    private function set_data(value:Array<D>):Array<D> {
        data = value;
        render();
        return data;
    }

    private function set_filter(value:D -> Bool):D -> Bool {
        if (filter != value) {
            filter = value;
            render();
        }
        return filter;
    }

    private function set_selected(value:Array<D>):Array<D> {
        if (selected != value) {
            selected = value;
            toUpdate();
        }
        return selected;
    }

    public function render():Void {
        if (data != null && factory != null) {
            filteredData = filter == null ? data : data.filter(filter);
            if (scroll != null) {
                scroll.ratio = Math.min(1.0, (itemCount - sizeDiff) / filteredData.length);
                scroll.position = ((offset + offsetDiff) / filteredData.length);
            }
            for (i in 0...itemCount) {
                var item:IListItemView<D> = items[i];
                var index = offset + i;
                if (filteredData[index] == null) {
                    item.visible = false;
                } else {
                    item.visible = true;
                    item.item_index = index;
                    item.data = filteredData[item.item_index];
                    if (
                    selected != null && selected.indexOf(item.data) > -1 ?
                    item.style.add("selected") :
                    item.style.remove("selected")
                    ) {
                        item.style = item.style;
                    }
                }
            }
        }
    }

    override public function update():Void {
        super.update();
        recalcSize(factory());
        render();
    }

    private function recalcSize(item:IListItemView<D>):Void {}

    private function set_itemCount(value:Int):Int {
        if (itemCount != value) {
            itemCount = value;
            var diff:Int = itemCount - items.length;
            if (diff > 0) {
                for (i in 0...diff) {
                    var item:IListItemView<D> = factory();
                    item.visible = false;
                    items.push(item);
                    setClickListener(item);
                    box.addView(item);
                }
            } else if (diff < 0) {
                for (i in 0...-diff) {
                    var item:IListItemView<D> = items.pop();
                    item.content.removeEventListener(MouseEvent.CLICK, itemsListeners.get(item));
                    itemsListeners.remove(item);
                    box.removeView(item);
                }
            }
        }
        return itemCount;
    }

    private function set_offsetDiff(value:Float):Float {
        offsetDiff = value;
        return offsetDiff;
    }

    private function setClickListener(item:IListItemView<D>):Void {
        var listener:MouseEvent -> Void = function(event:MouseEvent):Void {
            onItemSelect.emit(item);
        }
        item.content.addEventListener(MouseEvent.CLICK, listener);
        itemsListeners.set(item, listener);
    }
}

interface IListItemView<D> extends IView<Dynamic> {
    public var item_index(default, default):Int;
    public var data(default, set):D;
}
