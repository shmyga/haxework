package hw.view.list;

import flash.geom.Point;
import hw.view.skin.VScrollBarSkin;

class VScrollBarView extends ScrollBarView {

    public function new() {
        super();
        skin = new VScrollBarSkin();
        style = "scroll.vertical";
    }

    override private function onMouseDown(p:Point):Void {
        mousePosition = p.y - height * position;
    }

    override private function onMouseMove(p:Point):Void {
        position = (p.y - mousePosition) / height;
        onScroll.emit(position);
    }
}
