package hw.view.list;

import hw.view.form.LabelView;
import hw.view.geometry.HAlign;
import hw.view.list.ListView.IListItemView;

private typedef Formatter<T> = Int -> T -> String;

class LabelListItem<T> extends LabelView implements IListItemView<T> {

    public var item_index(default, default):Int;
    public var data(default, set):T;
    public var formatter(default, default):Formatter<T>;

    private static function defaultFormatter<T>(index:Int, value:T):String {
        return Std.string(value);
    }

    public function new(formatter:Formatter<T> = null) {
        super();
        textField.wordWrap = true;
        this.formatter = formatter == null ? defaultFormatter : formatter;
        geometry.width.percent = 100;
        geometry.height.fixed = 20;
        geometry.padding = 8;
        layout.hAlign = LEFT;
    }

    private function set_data(value:T):T {
        data = value;
        text = formatter(item_index, value);
        style = 'text${item_index % 2}';
        return value;
    }

    public static function factory<T>():LabelListItem<T> {
        return new LabelListItem();
    }
}
