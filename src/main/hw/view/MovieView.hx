package hw.view;

import hw.net.SwfLoader;
import flash.display.MovieClip;

//ToDo: sprite wrapper?
class MovieView extends View<MovieClip> {

  public var movie(get, set):MovieClip;
  public var movieUrl(default, set):String;

  public function new(?movie:MovieClip) {
    super(movie);
  }

  private function get_movie():MovieClip {
    return cast content;
  }

  private function set_movie(value:MovieClip):MovieClip {
    var index:Int = 0;
    if (parent != null && content != null) {
      index = parent.container.getChildIndex(content);
      parent.container.removeChild(content);
    }
    content = value;
    content.visible = visible;
    if (parent != null) {
      parent.container.addChildAt(content, index);
    }
    invalidate();
    return cast content;
  }

  private function set_movieUrl(value:String):String {
    movieUrl = value;
    new SwfLoader().GET(movieUrl)
      .then(function(data:MovieClip):Void {
        movie = data;
      });
    return movieUrl;
  }

  override public function update():Void {
    if (contentSize && content != null) {
      width = content.loaderInfo.width;
      height = content.loaderInfo.height;
    }
    super.update();
    if (!contentSize && content != null) {
      var s:Float = Math.min(width / content.loaderInfo.width, height / content.loaderInfo.height);
      content.scaleX = content.scaleY = s;
      content.x = (width - content.loaderInfo.width * s) / 2;
      content.y = (height - content.loaderInfo.height * s) / 2;
    }
  }
}
