package hw.view.layout;

import hw.view.geometry.HAlign;
import hw.view.geometry.SizeValue;
import hw.view.geometry.VAlign;
import hw.view.group.IGroupView;

class HorizontalLayout extends DefaultLayout {

    override public function place(group:IGroupView, views:Array<IView<Dynamic>>):Void {
        views = filterViews(group, views);

        var fixedSize:Float = margin * (views.length - 1);
        var leftSize:Float = group.width - group.geometry.padding.horizontal;

        var maxSize:Float = 0;

        for (view in views) {
            switch view.geometry.width.type {
                case PERCENT:
                    leftSize -= (view.geometry.margin.horizontal);
                case FIXED | DISPLAY_WIDTH | DISPLAY_HEIGHT:
                    fixedSize += (view.width + view.geometry.margin.horizontal);
            }
            maxSize = Math.max(maxSize, view.size.toSize("percent").height + view.geometry.padding.vertical + view.geometry.margin.vertical);
        }

        setGroupSize(group, fixedSize, maxSize);

        leftSize -= fixedSize;
        for (view in views) {
            switch view.geometry.width.type {
                case PERCENT:
                    var result = view.geometry.width.value / 100 * leftSize;
                    fixedSize += result + view.geometry.margin.horizontal;
                    view.setSize(result - view.geometry.padding.horizontal, 0, "percent.width");
                case _:
            };
        }

        var x:Float = 0;
        switch hAlign {
            case LEFT | NONE: x = group.geometry.padding.left;
            case CENTER: x = Math.max((group.width - fixedSize) / 2, 0) + group.geometry.padding.left - group.geometry.padding.right;
            case RIGHT: x = group.width - fixedSize - group.geometry.padding.right;
        }

        for (view in views) {
            view.x = x + view.geometry.margin.left;
            x += (view.width + view.geometry.margin.horizontal + margin);
            setViewHeight(group, view);
            placeViewVertical(group, view);
        }
    }
}
