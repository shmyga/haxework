package hw.view.layout;

import hw.view.geometry.HAlign;
import hw.view.geometry.Position;
import hw.view.geometry.VAlign;
import hw.view.group.IGroupView;
import hw.view.group.Overflow;

class DefaultLayout extends Layout {

    override public function place(group:IGroupView, views:Array<IView<Dynamic>>):Void {
        var width:Float = 0;
        var height:Float = 0;
        for (view in views) {
            setViewWidth(group, view);
            setViewHeight(group, view);
            placeViewHorizontal(group, view);
            placeViewVertical(group, view);
            var size = view.size.toSize("percent");
            width = Math.max(width, size.width + view.geometry.margin.horizontal);
            height = Math.max(height, size.height + view.geometry.margin.horizontal);
        }
        setGroupSize(group, width, height);
    }

    private function setGroupSize(group:IGroupView, width:Float, height:Float):Void {
        group.setSize(
            switch group.overflow.x {
                case STRETCH: width;
                case _: 0;
            },
            switch group.overflow.y {
                case STRETCH: height;
                case _: 0;
            },
            "group"
        );
        var ox = switch group.overflow.x {
            case SCROLL:
                width / (group.width - group.geometry.padding.horizontal);
            case _: 1;
        }
        group.overflowX = ox;
        var oy = switch group.overflow.y {
            case SCROLL:
                height / (group.height - group.geometry.padding.vertical);
            case _: 1;
        }
        group.overflowY = oy;
    }

    private function filterViews(group:IGroupView, views:Array<IView<Dynamic>>):Array<IView<Dynamic>> {
        return Lambda.array(Lambda.filter(views, function(view:IView<Dynamic>):Bool {
            return switch (view.geometry.position) {
                case ABSOLUTE:
                    setViewWidth(group, view);
                    setViewHeight(group, view);
                    placeViewHorizontal(group, view);
                    placeViewVertical(group, view);
                    false;
                case LAYOUT:
                    var result:Bool = view.visible;
                    result;
            }
        }));
    }

    private function setViewWidth(group:IGroupView, view:IView<Dynamic>):Void {
        view.setSize(view.geometry.width.percent / 100 * (group.width - view.geometry.margin.horizontal - group.geometry.padding.horizontal) - view.geometry.padding.horizontal, 0, "percent.width");
    }

    private function setViewHeight(group:IGroupView, view:IView<Dynamic>):Void {
        view.setSize(0, view.geometry.height.percent / 100 * (group.height - view.geometry.margin.vertical - group.geometry.padding.vertical) - view.geometry.padding.vertical, "percent.height");
    }

    private function placeViewHorizontal(group:IGroupView, view:IView<Dynamic>):Void {
        var align:HAlign = view.geometry.hAlign;
        if (align == NONE) align = hAlign;
        switch (align) {
            case LEFT | NONE:
                view.x = group.geometry.padding.left + view.geometry.margin.left;
            case CENTER:
                view.x = (group.width - view.width) / 2 +
                (group.geometry.padding.left - group.geometry.padding.right) +
                (view.geometry.margin.left - view.geometry.margin.right);
            case RIGHT:
                view.x = group.width - view.width - group.geometry.padding.right - view.geometry.margin.right;
        }
    }

    private function placeViewVertical(group:IGroupView, view:IView<Dynamic>):Void {
        var align:VAlign = view.geometry.vAlign;
        if (align == NONE) align = vAlign;
        switch (align) {
            case TOP | NONE:
                view.y = group.geometry.padding.top + view.geometry.margin.top;
            case MIDDLE:
                view.y = (group.height - view.height) / 2 +
                (group.geometry.padding.top - group.geometry.padding.bottom) +
                (view.geometry.margin.top - view.geometry.margin.bottom);
            case BOTTOM:
                view.y = group.height - view.height - group.geometry.padding.bottom - view.geometry.margin.bottom;
        }
    }
}
