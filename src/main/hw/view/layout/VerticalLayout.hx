package hw.view.layout;

import hw.view.geometry.HAlign;
import hw.view.geometry.SizeValue;
import hw.view.geometry.VAlign;
import hw.view.group.IGroupView;

class VerticalLayout extends DefaultLayout {

    override public function place(group:IGroupView, views:Array<IView<Dynamic>>):Void {
        views = filterViews(group, views);

        var fixedSize:Float = margin * (views.length - 1);
        var leftSize:Float = group.height - group.geometry.padding.vertical;

        var maxSize:Float = 0;

        for (view in views) {
            switch view.geometry.height.type {
                case PERCENT:
                    leftSize -= (view.geometry.margin.vertical);
                case FIXED | DISPLAY_WIDTH | DISPLAY_HEIGHT:
                    fixedSize += (Math.max(view.geometry.height.value, view.height) + view.geometry.margin.vertical);
            }
            maxSize = Math.max(maxSize, view.size.toSize("percent").width + view.geometry.padding.horizontal + view.geometry.margin.horizontal);
        }

        setGroupSize(group, maxSize, fixedSize);

        leftSize -= fixedSize;
        for (view in views) {
           switch view.geometry.height.type {
                case PERCENT:
                    var result = view.geometry.height.value / 100 * leftSize;
                    fixedSize += result + view.geometry.margin.vertical;
                    view.setSize(0, result - view.geometry.padding.vertical, "percent.height");
                case _:
            };
        }

        var y:Float = 0;
        switch vAlign {
            case TOP | NONE: y = group.geometry.padding.top;
            case MIDDLE: y = Math.max((group.height - fixedSize) / 2, 0) + group.geometry.padding.top - group.geometry.padding.bottom;
            case BOTTOM: y = group.height - fixedSize - group.geometry.padding.bottom;
        }

        for (view in views) {
            view.y = y + view.geometry.margin.top;
            y += (view.height + view.geometry.margin.vertical + margin);
            setViewWidth(group, view);
            placeViewHorizontal(group, view);
        }
    }
}
