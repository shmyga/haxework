package hw.view.layout;

import hw.view.geometry.HAlign;
import hw.view.geometry.VAlign;
import hw.view.group.IGroupView;
import hw.view.theme.IStylable;

interface ILayout extends IStylable {
    public var hAlign(get, set):HAlign;
    public var vAlign(get, set):VAlign;
    public var margin(get, set):Null<Float>;

    public function place(group:IGroupView, views:Array<IView<Dynamic>>):Void;

    public function setAlign(hAlign:HAlign, vAlign:VAlign):ILayout;

    public function setMargin(margin:Float):ILayout;
}
