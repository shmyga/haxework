package hw.view.layout;

import hw.view.geometry.HAlign;
import hw.view.geometry.VAlign;
import hw.view.group.IGroupView;

@:style class Layout implements ILayout {

    @:style(HAlign.NONE) public var hAlign(default, default):HAlign;
    @:style(VAlign.NONE) public var vAlign(default, default):VAlign;
    @:style(0) public var margin(default, default):Null<Float>;

    public function new() {}

    public function place(group:IGroupView, views:Array<IView<Dynamic>>):Void {}

    public function setAlign(hAlign:HAlign, vAlign:VAlign):ILayout {
        this.hAlign = hAlign;
        this.vAlign = vAlign;
        return this;
    }

    public function setMargin(margin:Float):ILayout {
        this.margin = margin;
        return this;
    }
}
