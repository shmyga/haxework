package hw.view.layout;

import hw.view.geometry.VAlign;
import hw.view.group.IGroupView;

typedef Row = {
    var width:Float;
    var height:Float;
    var views:Array<IView<Dynamic>>;
}

class TailLayout extends DefaultLayout {

    @:style(0) public var rowSize:Null<Int>;
    @:style(false) public var stretch:Null<Bool>;

    private function placeRow(group:IGroupView, y:Float, row:Row):Void {
        var x:Float = (group.width - row.width) / 2;
        for (v in row.views) {
            v.x = x;
            var vy =  switch group.layout.vAlign {
                case TOP | NONE: y;
                case MIDDLE: y + (row.height - v.height) / 2;
                case BOTTOM: y + (row.height - v.height);
            }
            v.y = vy;
            x += v.width + margin;
        }
    }

    override public function place(group:IGroupView, views:Array<IView<Dynamic>>):Void {
        var rows:Array<Row> = [];
        var row:Row = {
            width: 0,
            height: 0,
            views: [],
        }
        var w:Float = 0;
        var size = group.size.toSize("group");
        for (view in filterViews(group, views)) {
            setViewWidth(group, view);
            setViewHeight(group, view);
            if (
            (rowSize > 0 && row.views.length >= rowSize) ||
            (/*rowSize == 0 && */row.width + view.width + margin + group.geometry.margin.horizontal > size.width && !stretch)
            ) {
                row.width -= margin;
                w = Math.max(w, row.width);
                rows.push(row);
                row = {
                    width: 0,
                    height: 0,
                    views: [],
                };
            }
            row.views.push(view);
            row.width += view.width + margin;
            row.height = Math.max(row.height, view.height);
        }
        row.width -= margin;
        w = Math.max(w, row.width);
        rows.push(row);
        var h:Float = Lambda.fold(rows, function(row, h) return row.height + h, 0) + margin * (rows.length - 1);
        var y:Float = Math.max(group.geometry.padding.top, (group.height - h) / 2);

        y = group.geometry.padding.top;
        if (h < group.height) {
            y = switch vAlign {
                case TOP | NONE: group.geometry.padding.top;
                case MIDDLE: (group.height - h) / 2;
                case BOTTOM: group.height - h - group.geometry.padding.bottom;
            }
        }
        for (row in rows) {
            placeRow(group, y, row);
            y += row.height + margin;
        }

        setGroupSize(group, w, h);
    }
}
