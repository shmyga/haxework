package hw.view.utils;

import flash.display.BitmapData;
import flash.display.Graphics;
import flash.geom.Matrix;
import flash.geom.Point;
import flash.geom.Rectangle;

@:enum abstract FillType(String) from String to String {
    var NONE = "NONE";
    var DEFAULT = "DEFAULT";
    var COVER = "COVER";
    var CONTAIN = "CONTAIN";
    var REPEAT = "REPEAT";
    var STRETCH = "STRETCH";
    var NINEPATH = "NINEPATH";
}

class DrawUtil {

    public static function draw(graphics:Graphics, image:BitmapData, rect:Rectangle, ?fillType:FillType = null, ?color:Int = -1, ?clear:Bool = true):Void {
        if (image == null) return;
        if (fillType == null) fillType = FillType.DEFAULT;
        if (clear) graphics.clear();
        if (color > -1) {
            graphics.beginFill(color);
            graphics.drawRect(rect.x, rect.y, rect.width, rect.height);
            graphics.endFill();
        }
        var m:Matrix = new Matrix();
        var sx:Float = 1.0;
        var sy:Float = 1.0;
        switch (fillType) {
            case FillType.REPEAT:
                graphics.beginBitmapFill(image, m, true, false);
                graphics.drawRect(rect.x, rect.y, rect.width, rect.height);
                graphics.endFill();
                return;
            case FillType.NONE:
                graphics.beginBitmapFill(image, m, false, false);
                graphics.drawRect(rect.x, rect.y, rect.width, rect.height);
                graphics.endFill();
                return;
            case FillType.NINEPATH:
                draw9path(graphics, image, rect);
                return;
            case FillType.DEFAULT:
            case FillType.CONTAIN:
                sx = sy = Math.min(rect.width / image.width, rect.height / image.height);
            case FillType.COVER:
                sx = sy = Math.max(rect.width / image.width, rect.height / image.height);
            case FillType.STRETCH:
                sx = rect.width / image.width;
                sy = rect.height / image.height;
        }
        m.scale(sx, sy);
        var dx:Float = rect.x + (rect.width - image.width * sx) / 2;
        var dy:Float = rect.y + (rect.height - image.height * sy) / 2;
        m.translate(dx, dy);
        graphics.beginBitmapFill(image, m, false, true);
        rect.x = Math.max(rect.x, m.tx);
        rect.y = Math.max(rect.y, m.ty);
        rect.width = Math.min(rect.width, image.width * sx);
        rect.height = Math.min(rect.height, image.height * sy);
        graphics.drawRect(rect.x, rect.y, rect.width, rect.height);
        graphics.endFill();
    }

    private static function draw9path(graphics:Graphics, image:BitmapData, rect:Rectangle):Void {
        var w:Int = Math.round(image.width / 2);
        var h:Int = Math.round(image.height / 2);
        var m:Matrix = null;
        //lt
        graphics.beginBitmapFill(image, m, false);
        graphics.drawRect(0, 0, w, h);
        graphics.endFill();
        //rt
        m = new Matrix();
        m.translate(rect.width - 2 * w, 0);
        graphics.beginBitmapFill(image, m, false);
        graphics.drawRect(rect.width - w, 0, w, h);
        graphics.endFill();
        //lb
        m = new Matrix();
        m.translate(0, rect.height - 2 * h);
        graphics.beginBitmapFill(image, m, false);
        graphics.drawRect(0, rect.height - h, w, h);
        graphics.endFill();
        //rb
        m = new Matrix();
        m.translate(rect.width - 2 * w, rect.height - 2 * h);
        graphics.beginBitmapFill(image, m, false);
        graphics.drawRect(rect.width - w, rect.height - h, w, h);
        graphics.endFill();
        //c
        graphics.beginFill(image.getPixel(w, h));
        graphics.drawRect(w - 1, h - 1, rect.width - 2 * w + 2, rect.height - 2 * h + 2);
        graphics.endFill();
        //t
        var t:BitmapData = new BitmapData(1, h);
        t.copyPixels(image, new Rectangle(w, 0, 1, h), new Point(0, 0));
        graphics.beginBitmapFill(t, null, true);
        graphics.drawRect(w, 0, rect.width - w * 2, h);
        graphics.endFill();
        //b
        var b:BitmapData = new BitmapData(1, h);
        b.copyPixels(image, new Rectangle(w, h, 1, h), new Point(0, 0));
        m = new Matrix();
        m.translate(0, rect.height - h);
        graphics.beginBitmapFill(b, m, true);
        graphics.drawRect(w, rect.height - h, rect.width - w * 2, h - 2); //ToDo:?
        graphics.endFill();
        //l
        var l:BitmapData = new BitmapData(w, 1);
        l.copyPixels(image, new Rectangle(0, h, w, 1), new Point(0, 0));
        graphics.beginBitmapFill(l, null, true);
        graphics.drawRect(0, h, w, rect.height - h * 2);
        graphics.endFill();
        //r
        var r:BitmapData = new BitmapData(w, 1);
        r.copyPixels(image, new Rectangle(w, h, w, 1), new Point(0, 0));
        m = new Matrix();
        m.translate(rect.width - w, 0);
        graphics.beginBitmapFill(r, m, true);
        graphics.drawRect(rect.width - w, h, w, rect.height - h * 2);
        graphics.endFill();
    }
}
