package hw.view.utils;

import flash.display.BitmapData;
import flash.filters.ColorMatrixFilter;
import flash.geom.ColorTransform;
import flash.geom.Point;
import flash.geom.Rectangle;
import hw.color.Color;

class BitmapUtil {

    private static var cache:Map<BitmapData, Map<String, BitmapData>> = new Map<BitmapData, Map<String, BitmapData>>();

    private static function fromCache(image:BitmapData, key:String):Null<BitmapData> {
        return cache.exists(image) && cache.get(image).exists(key) ? cache.get(image).get(key) : null;
    }

    private static function toCache(image:BitmapData, key:String, value:BitmapData):Void {
        if (!cache.exists(image)) cache.set(image, new Map<String, BitmapData>());
        cache.get(image).set(key, value);
    }


    public static function multiply(image:BitmapData, m:Float):BitmapData {
        var result = fromCache(image, "multiply:" + m);
        if (result != null) return result;
        var ct:ColorTransform = new ColorTransform(m, m, m, 1.0, 0, 0, 0);
        var out:BitmapData = image.clone();
        out.colorTransform(out.rect, ct);
        toCache(image, "multiply:" + m, out);
        return out;
    }

    public static function grayscale(image:BitmapData, m:Float):BitmapData {
        var result = fromCache(image, "grayscale:" + m);
        if (result != null) return result;
        var matrix:Array<Float> = [];
        matrix = matrix.concat([m, m, m, 0, 0]);
        matrix = matrix.concat([m, m, m, 0, 0]);
        matrix = matrix.concat([m, m, m, 0, 0]);
        matrix = matrix.concat([0, 0, 0, 1, 0]);
        var cmf:ColorMatrixFilter = new ColorMatrixFilter(matrix);
        var out:BitmapData = image.clone();
        out.applyFilter(out, out.rect, new Point(0, 0), cmf);
        toCache(image, "grayscale:" + m, out);
        return out;
    }

    public static function colorize(data:BitmapData, color:Color):BitmapData {
        if (color.zero) return data;
        var result = data.clone();
        var transform = new ColorTransform(color.red / 255, color.green / 255, color.blue / 255, 1, 0, 0, 0, 0);
        result.colorTransform(new Rectangle(0, 0, result.width, result.height), transform);
        return result;
    }
}
