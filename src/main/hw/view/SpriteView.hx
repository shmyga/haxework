package hw.view;

import flash.display.Sprite;
import hw.view.skin.SpriteSkin;

class SpriteView extends View<Sprite> {

    public function new() {
        super(new Sprite());
        skin = new SpriteSkin();
    }

    override public function redraw():Void {
        this.content.graphics.clear(); // ToDo: in skin
        super.redraw();
        #if dev_layout
        var graphics = content.graphics;
        graphics.lineStyle(1, 0x00ff00);
        graphics.drawRect(0, 0, width, height);
        graphics.lineStyle();
        #end
    }
}
