package hw.view.popup;

import hw.view.group.IGroupView;
import hw.animate.IAnimate;

typedef P = PopupView<Dynamic>;

@:provide class PopupManager {

    public var showAnimateFactory(default, default):IView<Dynamic> -> IAnimate;
    public var closeAnimateFactory(default, default):IView<Dynamic> -> IAnimate;

    private var popups:Array<P>;

    @:provide private static var root:IGroupView;

    public function new() {
        popups = new Array<P>();
    }

    public function show(popup:P):Void {
        root.addView(popup);
        if (showAnimateFactory != null) {
            showAnimateFactory(popup).start();
        }
        popups.push(popup);
    }

    public function close(popup:P):Void {
        popups.remove(popup);
        if (closeAnimateFactory != null) {
            closeAnimateFactory(popup).start().then(_ -> remove(popup));
        } else {
            remove(popup);
        }
    }

    public function remove(popup:P):Void {
        if (root.containsView(popup)) {
            root.removeView(popup);
            root.focus();
        }
    }

    public function closeTop():Bool {
        if (popups.length > 0) {
            close(popups[popups.length - 1]);
            return true;
        }
        return false;
    }
}
