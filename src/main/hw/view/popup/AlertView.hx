package hw.view.popup;

import hw.view.form.LabelView;
import hw.view.text.TextView;
import promhx.Promise;

@:template class AlertView extends PopupView<Dynamic> {

    @:view var header:LabelView;
    @:view var message:TextView;

    public function new() {
        super();
    }

    public static function alert(message:String):Promise<Dynamic> {
        var result = new AlertView();
        result.message.text = message;
        return result.show();
    }
}
