package hw.view.popup;

import hw.view.form.LabelView;
import hw.view.text.TextView;
import promhx.Promise;

@:template class ConfirmView extends PopupView<Bool> {

    @:view var header:LabelView;
    @:view var message:TextView;

    public function new() {
        super();
    }

    public static function confirm(message:String):Promise<Bool> {
        var result = new ConfirmView();
        result.message.text = message;
        return result.show();
    }
}
