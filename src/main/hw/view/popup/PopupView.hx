package hw.view.popup;

import flash.events.MouseEvent;
import hw.view.geometry.Position;
import hw.view.group.GroupView;
import hw.view.skin.Skin;
import promhx.Deferred;
import promhx.Promise;

class PopupView<R> extends GroupView {
    @:provide var manager:PopupManager;

    public var background(default, null):IView<Dynamic>;
    public var view(default, set):IView<Dynamic>;
    private var deferred:Deferred<R>;

    public function new() {
        super();
        geometry.stretch = true;
        geometry.position = Position.ABSOLUTE;
        background = buildBackground();
        background.content.addEventListener(MouseEvent.CLICK, onBackgroundClick);
        addView(background);
    }

    private function buildBackground():IView<Dynamic> {
        var result = new SpriteView();
        result.geometry.stretch = true;
        result.geometry.position = Position.ABSOLUTE;
        result.skin = Skin.color(0x000000, 0.6);
        return result;
    }

    private function onBackgroundClick(event:MouseEvent):Void {
        reject("background");
    }

    private function set_view(value:IView<Dynamic>):IView<Dynamic> {
        if (view != null) {
            removeView(view);
        }
        view = value;
        addView(view);
        return this.view;
    }

    private function onShow():Void {}

    private function onClose():Void {}

    public function show():Promise<R> {
        manager.show(this);
        deferred = new Deferred<R>();
        onShow();
        return deferred.promise();
    }

    public function close(result:R):Void {
        var d = deferred;
        deferred = null;
        manager.close(this);
        onClose();
        if (d != null) {
            d.resolve(result);
        }
    }

    public function reject(reason:Dynamic):Void {
        var d = deferred;
        deferred = null;
        manager.close(this);
        if (d != null) {
            //d.throwError(reason);
            d.resolve(null);
        }
    }
}
