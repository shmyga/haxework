package hw.view.form;

import hw.view.geometry.HAlign;
import hw.view.geometry.VAlign;
import hw.view.text.TextView;

class LabelView extends TextView {

    public function new() {
        super();
        style = "label";
        fill = false;
        textField.selectable = false;
        textField.wordWrap = false;
        textField.multiline = true;
        layout.hAlign = HAlign.CENTER;
        layout.vAlign = VAlign.MIDDLE;
    }
}
