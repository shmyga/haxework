package hw.view.form;

class ToggleButtonView extends ButtonView {

    public var on(default, set):Bool;

    public var onText(default, set):String;

    public function new() {
        super();
    }

    private function set_on(value:Bool):Bool {
        on = value;
        toUpdate();
        toRedraw();
        return on;
    }

    private function set_onText(value:String):String {
        if (onText != value) {
            onText = value;
            toUpdate();
        }
        return onText;
    }

    override private function currentText():String {
        return on && onText != null ? onText : super.currentText();
    }
}
