package hw.view.form;

import hw.view.skin.ButtonColorSkin;
import hw.signal.Signal;
import flash.events.MouseEvent;

enum ButtonState {
    UP;
    OVER;
    DOWN;
    DISABLED;
}

class ButtonView extends LabelView {

    public static var TYPE = "button";

    public var disabled(default, set):Bool;
    public var state(get, null):ButtonState;
    public var onPress(default, null):Signal<ButtonView>;
    public var propagation(default, default):Bool;

    private var overed:Bool;
    private var downed:Bool;

    public function new() {
        super();
        skin = new ButtonColorSkin();
        style = "button";
        propagation = true;
        overed = false;
        downed = false;
        state = ButtonState.UP;
        onPress = new Signal();
        content.buttonMode = true;
        content.mouseChildren = false;
        #if js
        content.addEventListener(MouseEvent.MOUSE_UP, onMouseClick);
        #else
        content.addEventListener(MouseEvent.CLICK, onMouseClick);
        #end
        #if !mobile
        content.addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
        content.addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
        #end
        content.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
        content.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
    }

    private function onMouseClick(event:MouseEvent):Void {
        #if js if (downed) { #end
        if (!propagation) event.stopImmediatePropagation();
        if (!disabled) onPress.emit(this);
        #if js } #end
    }

    private function onMouseOver(event:MouseEvent):Void {
        overed = true;
        toRedraw();
    }

    private function onMouseOut(event:MouseEvent):Void {
        overed = false;
        toRedraw();
    }

    private function onMouseDown(event:MouseEvent):Void {
        downed = true;
        if (content.stage != null) {
            content.stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
            toRedraw();
        }
    }

    private function onMouseUp(event:MouseEvent):Void {
        downed = false;
        if (content.stage != null) {
            content.stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
            toRedraw();
        }
    }

    private function set_disabled(value:Bool):Bool {
        if (disabled != value) {
            disabled = value;
            content.buttonMode = !disabled;
            toRedraw();
        }
        return disabled;
    }

    private function get_state():ButtonState {
        if (disabled) return DISABLED;
        #if mobile
        return downed ? DOWN : UP;
        #else
        return (downed && overed) ? DOWN : overed ? OVER : UP;
        #end
    }

    public function dispose():Void {
        onPress.dispose();
        content.removeEventListener(MouseEvent.CLICK, onMouseClick);
        content.removeEventListener(MouseEvent.MOUSE_UP, onMouseClick);
        content.removeEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
        content.removeEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
        content.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
        content.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
    }
}
