package hw.view.form;

import flash.events.MouseEvent;
import flash.geom.Point;
import hw.signal.Signal;
import hw.view.data.DataView;
import hw.view.geometry.HAlign;
import hw.view.geometry.Position;
import hw.view.group.GroupView;
import hw.view.group.IGroupView;
import hw.view.layout.VerticalLayout;
import hw.view.skin.Skin;
import hw.view.theme.StyleId;
using hw.utils.StringUtil;

class SelectIdView<D, K> extends SelectView<D> {
    public var selectedId(get, set):K;
    public var idResolver(default, default):D -> K;

    private var _selectedId:K;

    public function new() {
        super();
        idResolver = function(item:D):K return Reflect.field(item, "id");
    }

    override private function set_selected(value:D):D {
        var result = super.set_selected(value);
        _selectedId = idResolver(result);
        return result;
    }

    private inline function get_selectedId():K {
        return _selectedId;
    }

    private function set_selectedId(value:K):K {
        if (_selectedId != value) {
            _selectedId = value;
            if (data != null) {
                for (item in data) {
                    if (idResolver(item) == _selectedId) {
                        selected = item;
                        break;
                    }
                }
            }
        }
        return _selectedId;
    }
}

class SelectView<D> extends GroupView {

    public var currentView(default, null):ButtonView;
    public var dataView(default, null):DataView<D, LabelView>;

    public var labelStyle(default, set):StyleId;
    public var labelBuilder(default, set):D -> String;
    public var data(default, set):Array<D>;
    public var selected(default, set):D;
    public var onSelect(default, null):Signal<D> = new Signal();

    @:provide private static var root:IGroupView;

    public function new() {
        super(new VerticalLayout());
        skin = Skin.transparent();
        currentView = new ButtonView();
        currentView.onPress.connect(function(_) toggle());
        dataView = new DataView();
        dataView.style = "dropdown";
        dataView.geometry.position = ABSOLUTE;
        dataView.factory = factory;
        dataView.onDataSelect.connect(function(value:D):Void {
            selected = value;
            close();
        });
        views = [currentView];
        labelBuilder = function(value:D):String return Std.string(value).title();
    }

    private function factory(index:Int, value:D):LabelView {
        var result = new LabelView();
        result.layout.hAlign = LEFT;
        result.geometry.width.percent = 100;
        if (labelStyle != null) {
            result.style = labelStyle;
        }
        result.text = labelBuilder(value);
        return result;
    }

    private function set_labelStyle(value:StyleId):StyleId {
        if (labelStyle != value) {
            labelStyle = value;
            currentView.style = labelStyle;
            for (view in dataView.dataViews) {
                view.style = labelStyle;
            }
        }
        return labelStyle;
    }

    private function set_labelBuilder(value:D -> String):D -> String {
        labelBuilder = value;
        for (i in 0...dataView.dataViews.length) {
            dataView.dataViews[i].text = labelBuilder(data[i]);
        }
        return labelBuilder;
    }

    private function set_data(value:Array<D>):Array<D> {
        data = value;
        dataView.data = value;
        return data;
    }

    private function set_selected(value:D):D {
        selected = value;
        currentView.text = labelBuilder(selected);
        onSelect.emit(selected);
        return selected;
    }

    public function toggle():Void {
        if (root.containsView(dataView)) {
            close();
        } else {
            open();
        }
    }

    public function open():Void {
        root.addView(dataView);
        content.stage.addEventListener(MouseEvent.MOUSE_DOWN, onStageMouseClick);
    }

    public function close():Void {
        content.stage.removeEventListener(MouseEvent.MOUSE_DOWN, onStageMouseClick);
        root.removeView(dataView);
    }

    override public function update():Void {
        super.update();
        var rect = currentView.rect;
        dataView.geometry.margin.left = rect.left;
        dataView.geometry.margin.top = rect.top + rect.height + layout.margin;
    }

    private function onStageMouseClick(event:MouseEvent):Void {
        var objects = content.stage.getObjectsUnderPoint(new Point(event.stageX, event.stageY));
        if (objects.indexOf(content) == -1 && objects.indexOf(dataView.content) == -1) {
            close();
        }
    }
}
