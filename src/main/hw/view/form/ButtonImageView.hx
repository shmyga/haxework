package hw.view.form;

import flash.display.BitmapData;
import hw.view.skin.ButtonBitmapSkin;
import hw.view.skin.ISkin;
import hw.view.utils.BitmapUtil;
import hw.view.utils.DrawUtil.FillType;
import hw.net.ImageLoader;

class ButtonImageView extends ButtonView {

    public var image(default, set):BitmapData;
    private var bitmapSkin:ButtonBitmapSkin = new ButtonBitmapSkin();

    public function new(image:BitmapData = null) {
        super();
        skin = bitmapSkin;
        if (image != null) {
            this.image = image;
        }
    }

    private function set_image(value:BitmapData):BitmapData {
        if (image != value) {
            setSize(value.width, value.height, "image");
            image = value;
            bitmapSkin.image = image;
            toRedraw();
        }
        return image;
    }
}
