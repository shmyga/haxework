package hw.view.form;

import hw.view.text.TextView;
import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFieldType;
import flash.text.TextFormat;
import flash.text.TextFormatAlign;
import hw.signal.Signal;

class InputView extends TextView {

    public var hint(default, set):String;
    public var onChange(default, null):Signal<String> = new Signal();

    private var hintTextField:TextField;

    public function new() {
        super();
        style = "input";
        textField.type = TextFieldType.INPUT;
        textField.addEventListener(Event.CHANGE, onTextChange);
        textField.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
        textField.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);

        hintTextField = buildHintTextField();
        content.addChild(hintTextField);
        font.align = TextFormatAlign.LEFT;
    }

    private function set_hint(value:String):String {
        if (hint != value) {
            hint = value;
            toUpdate();
        }
        return hint;
    }

    private function buildHintTextField():TextField {
        var textField:TextField = new TextField();
        textField.autoSize = TextFieldAutoSize.NONE;
        textField.type = TextFieldType.DYNAMIC;
        textField.multiline = false;
        textField.defaultTextFormat = new TextFormat("Arial", 16, 0xa0a0a0);
        textField.mouseEnabled = false;
        return textField;
    }

    private function onTextChange(event:Event):Void {
        hintTextField.visible = (textField.text == "");
    }

    private function onKeyUp(event:KeyboardEvent):Void {
        event.stopImmediatePropagation();
        text = textField.text;
        onChange.emit(_text);
    }

    private function onKeyDown(event:KeyboardEvent):Void {
        event.stopImmediatePropagation();
    }

    override public function update():Void {
        super.update();
        var htf:TextFormat = textField.defaultTextFormat;
        htf.color = 0xa0a0a0;
        htf.size -= 2;
        hintTextField.defaultTextFormat = htf;
        hintTextField.text = hint == null ? "" : hint;
        placeTextField(hintTextField);
    }

    public function dispose():Void {
        textField.removeEventListener(Event.CHANGE, onTextChange);
        textField.removeEventListener(KeyboardEvent.KEY_UP, onKeyUp);
        textField.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
    }
}
