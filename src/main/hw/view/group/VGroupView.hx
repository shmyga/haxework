package hw.view.group;

import hw.view.layout.VerticalLayout;

class VGroupView extends GroupView {

  public function new() {
    super(new VerticalLayout());
  }
}
