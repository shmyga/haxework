package hw.view.group;

import flash.display.DisplayObjectContainer;
import hw.view.group.Overflow.Overflow;
import hw.view.layout.ILayout;

interface IGroupView extends IView<Dynamic> {
    public var layout(default, set):ILayout;
    public var overflow(default, set):Overflow;

    public var overflowX(default, set):Float;
    public var overflowY(default, set):Float;

    public var container(default, null):DisplayObjectContainer;
    public var views(default, set):Array<IView<Dynamic>>;

    public function containsView(view:IView<Dynamic>):Bool;

    public function addView(view:IView<Dynamic>):IView<Dynamic>;

    public function addViewFirst(view:IView<Dynamic>):IView<Dynamic>;

    public function insertView(view:IView<Dynamic>, index:Int):IView<Dynamic>;

    public function removeView(view:IView<Dynamic>):IView<Dynamic>;

    public function removeAllViews():Void;

    public function focus(?view:IView<Dynamic>):Void;
}
