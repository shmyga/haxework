package hw.view.group;

import flash.geom.Point;
import flash.events.TouchEvent;
import flash.geom.Rectangle;
import flash.display.DisplayObjectContainer;
import flash.display.Sprite;
import flash.events.MouseEvent;
import hw.signal.Signal;
import hw.view.geometry.Position;
import hw.view.layout.DefaultLayout;
import hw.view.layout.ILayout;
import hw.view.list.HScrollBarView;
import hw.view.list.ScrollBarView;
import hw.view.list.VScrollBarView;

class OverflowControl {
    public var overflow(default, set):Float = 0;
    public var offset(default, set):Float = 0;
    public var size(default, default):Float = 0;
    public var scroll(null, null):ScrollBarView;
    public var offsetSignal(default, null):Signal<Float>;

    private var scrollFactory:Void -> ScrollBarView;

    public function new(scrollFactory:Void -> ScrollBarView) {
        this.scrollFactory = scrollFactory;
        offsetSignal = new Signal();
    }

    private function set_offset(value:Float):Float {
        if (offset != value) {
            offset = checkOffset(value);
            if (scroll != null) {
                scroll.position = -offset / size / overflow;
            }
            offsetSignal.emit(offset);
        }
        return offset;
    }

    private function checkOffset(value:Float):Float {
        if (value > 0) {
            return 0;
        } else if (value < -size * (overflow - 1)) {
            return -size * (overflow - 1);
        }
        return value;
    }

    public function set_overflow(value:Float):Float {
        if (overflow != value) {
            overflow = value;
            if (overflow > 1 && scroll == null) {
                scroll = scrollFactory();
                scroll.onScroll.connect(function(value) {
                    offset = value > 0 ? -value * overflow * size : 0;
                });
            }
            if (scroll != null) {
                scroll.ratio = 1 / overflow;
            }
            offset = checkOffset(offset);
        }
        return overflow;
    }
}

@:style(true) class GroupView extends SpriteView implements IGroupView {
    public var container(default, null):DisplayObjectContainer;

    public var views(default, set):Array<IView<Dynamic>>;
    @:style public var layout(default, default):ILayout;
    @:style public var overflow(default, default):Overflow;

    public var overflowX(default, set):Float;
    public var overflowY(default, set):Float;

    private var _mask:Sprite;
    private var scrollX(get, null):HScrollBarView;
    private var scrollY(get, null):VScrollBarView;
    private var maskEnable(default, set):Bool;

    public var overflowControlX(default, null):OverflowControl;
    public var overflowControlY(default, null):OverflowControl;

    public function new(?layout:ILayout, ?overflow:Overflow) {
        super();
        container = new Sprite();
        content.addChild(container);
        _mask = new Sprite();
        this.layout = layout != null ? layout : new DefaultLayout();
        this.overflow = overflow != null ? overflow : new Overflow();
        views = [];
        overflowControlX = new OverflowControl(function() return scrollX);
        overflowControlX.offsetSignal.connect(function(value) container.x = value);
        overflowControlY = new OverflowControl(function() return scrollY);
        overflowControlY.offsetSignal.connect(function(value) container.y = value);
        content.addEventListener(MouseEvent.MOUSE_WHEEL, onMouseWheelEvent);
        content.addEventListener(TouchEvent.TOUCH_BEGIN, onTouchBegin);
    }

    private function set_maskEnable(value:Bool):Bool {
        if (maskEnable != value) {
            maskEnable = value;
            if (maskEnable) {
                content.addChild(_mask);
                container.mask = _mask;
            } else {
                content.removeChild(_mask);
                container.mask = null;
            }

        }
        return value;
    }

    private function onMouseWheelEvent(event:MouseEvent):Void {
        if (overflowY > 1) {
            #if flash event.preventDefault(); #end
            overflowControlY.offset += event.delta * 15;
        }
    }

    private var touchPoint:Point;

    private function onTouchBegin(event:TouchEvent):Void {
        if (overflowY > 1) {
            touchPoint = new Point(event.stageX, event.stageY);
            content.stage.addEventListener(TouchEvent.TOUCH_MOVE, onTouchMove);
            content.stage.addEventListener(TouchEvent.TOUCH_END, onTouchEnd);
        }
    }

    private function onTouchMove(event:TouchEvent):Void {
        if (overflowY > 1) {
            overflowControlY.offset -= touchPoint.y - event.stageY;
        }
        if (overflowX > 1) {
            overflowControlX.offset -= touchPoint.x - event.stageX;
        }
        touchPoint = new Point(event.stageX, event.stageY);
    }

    private function onTouchEnd(event:TouchEvent):Void {
        event.preventDefault();
        stage.removeEventListener(TouchEvent.TOUCH_MOVE, onTouchMove);
        stage.removeEventListener(TouchEvent.TOUCH_END, onTouchEnd);
    }

    private function get_scrollX():HScrollBarView {
        if (scrollX == null) {
            scrollX = new HScrollBarView();
            scrollX.geometry.position = Position.ABSOLUTE;
            addView(scrollX);
            container.removeChild(scrollX.content);
            content.addChild(scrollX.content);
        }
        return scrollX;
    }

    private function get_scrollY():VScrollBarView {
        if (scrollY == null) {
            scrollY = new VScrollBarView();
            scrollY.geometry.position = Position.ABSOLUTE;
            addView(scrollY);
            container.removeChild(scrollY.content);
            content.addChild(scrollY.content);
        }
        return scrollY;
    }

    public function set_overflowX(value:Float):Float {
        maskEnable = value > 1;
        return overflowX = overflowControlX.overflow = value;
    }

    public function set_overflowY(value:Float):Float {
        maskEnable = value > 1;
        return overflowY = overflowControlY.overflow = value;
    }

    override public function update():Void {
        super.update();
        layout.place(this, views);
        for (view in views) {
            view.update();
            if (view.index > -1) {
                container.setChildIndex(view.content, Std.int(Math.min(views.length - 1, view.index)));
            }
        }
        overflowControlX.size = width;
        overflowControlY.size = height;
        _mask.graphics.clear();
        _mask.graphics.beginFill(0, 1);
        if (!Math.isNaN(width) && !Math.isNaN(height) && width > 0 && height > 0) {
            _mask.graphics.drawRect(0, 0, width, height);
        }
        _mask.graphics.endFill();
    }

    public function set_views(value:Array<IView<Dynamic>>):Array<IView<Dynamic>> {
        removeAllViews();
        if (views == null) views = [];
        for (view in value) addView(view);
        return views;
    }

    public function containsView(view:IView<Dynamic>):Bool {
        return views.indexOf(view) > -1;
    }

    public function addView(view:IView<Dynamic>):IView<Dynamic> {
        views.push(view);
        if (view.content != null) container.addChild(view.content);
        view.parent = this;
        toUpdate();
        return view;
    }

    public function insertView(view:IView<Dynamic>, index:Int):IView<Dynamic> {
        if (index < 0) index = views.length + index;
        views.insert(index, view);
        if (view.content != null) container.addChild(view.content);
        view.parent = this;
        toUpdate();
        return view;
    }

    public function addViewFirst(view:IView<Dynamic>):IView<Dynamic> {
        views.unshift(view);
        container.addChild(view.content);
        view.parent = this;
        toUpdate();
        return view;
    }

    public function removeView(view:IView<Dynamic>):IView<Dynamic> {
        view.parent = null;
        views.remove(view);
        if (view.content != null) container.removeChild(view.content);
        toUpdate();
        return view;
    }

    public function removeAllViews():Void {
        if (views != null) {
            for (view in views.slice(0)) {
                if (view != scrollX && view != scrollY) {
                    removeView(view);
                }
            }
        }
    }

    public function focus(?view:IView<Dynamic>):Void {
        if (container.stage != null) {
            container.stage.focus = view != null ? view.content : content;
        }
    }

    override private function get_rect():Rectangle {
        var result = super.get_rect();
        result.x += overflowControlX.offset;
        result.y += overflowControlY.offset;
        return result;
    }
}
