package hw.view.group;

@:enum abstract OverflowType(String) from String to String {
    var STRETCH = "stretch";
    var SCROLL = "scroll";
    var CROP = "crop";
}

@:style class Overflow {
    @:style(STRETCH) public var x(default, default):OverflowType;
    @:style(STRETCH) public var y(default, default):OverflowType;

    public function new(?overflowX:OverflowType, ?overflowY:OverflowType) {
        this.x = x;
        this.y = y;
    }
}
