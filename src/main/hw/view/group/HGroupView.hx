package hw.view.group;

import hw.view.layout.HorizontalLayout;

class HGroupView extends GroupView {

  public function new() {
    super(new HorizontalLayout());
  }
}
