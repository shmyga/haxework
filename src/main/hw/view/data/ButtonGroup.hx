package hw.view.data;

import hw.view.theme.StyleId;
import hw.view.form.ToggleButtonView;
import hw.view.data.DataView.Factory;
import haxe.EnumTools;
import hw.view.layout.HorizontalLayout;
import hw.view.layout.ILayout;

using hw.utils.StringUtil;

class ButtonGroup<D> extends DataView<D, ToggleButtonView> {

    public var selected(default, set):D;
    public var buttonStyle(default, set):StyleId;

    public function new(?layout:ILayout) {
        super(layout != null ? layout : new HorizontalLayout());
        factory = buildFactory();
        onDataSelect.connect(function(value:D) selected = value);
    }

    // ToDo: enum equals
    private function dataIndex(value:D):Int {
        for (i in 0...data.length) {
            if (Reflect.isEnumValue(value)) {
                if (EnumValueTools.equals(cast value, cast data[i])) {
                    return i;
                }
            } else {
                if (value == data[i]) {
                    return i;
                }
            }
        }
        return -1;
    }

    private function set_selected(value:D):D {
        selected = value;
        var index = dataIndex(value);
        for (i in 0...dataViews.length) {
            var view = dataViews[i];
            view.on = i == index;
        }
        return selected;
    }

    private function set_buttonStyle(value:StyleId):StyleId {
        if (buttonStyle != value) {
            buttonStyle = value;
            for (view in dataViews) {
                view.style = buttonStyle;
            }
        }
        return buttonStyle;
    }

    override private function rebuild():Void {
        super.rebuild();
        for (view in dataViews) {
            view.style = buttonStyle;
        }
    }

    public static function buildFactory<D>(?label:D -> String):Factory<D, ToggleButtonView> {
        if (label == null) {
            label = function(value:D) return Std.string(value).title();
        }
        return function(index:Int, value:D):ToggleButtonView {
            var result = new ToggleButtonView();
            result.text = label(value);
            return result;
        }
    }
}
