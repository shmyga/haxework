package hw.view.data;

import flash.display.DisplayObject;
import flash.events.MouseEvent;
import hw.signal.Signal;
import hw.view.group.GroupView;
import hw.view.layout.ILayout;
import hw.view.layout.VerticalLayout;

typedef Factory<D, V:IView<Dynamic>> = Int -> D -> V

class ActionDataView<D, V:IView<Dynamic>, A> extends DataView<D, V> {
    public var onDataAction(default, null):Signal2<D, A> = new Signal2();
}

class DataView<D, V:IView<Dynamic>> extends GroupView {

    public var data(default, set):Array<D>;
    public var factory(default, set):Factory<D, V>;
    public var onItemSelect(default, null):Signal3<Int, D, V> = new Signal3();
    public var onDataSelect(default, null):Signal<D> = new Signal();
    public var dataViews(default, null):Array<V>;

    private var objectIndexes:Map<DisplayObject, Int> = new Map();

    public function new(?layout:ILayout) {
        super(layout != null ? layout : new VerticalLayout());
        dataViews = [];
    }

    private function set_data(value:Array<D>):Array<D> {
        data = value;
        if (factory != null) rebuild();
        return data;
    }

    private function set_factory(value:Factory<D, V>):Factory<D, V> {
        factory = value;
        if (data != null) rebuild();
        return factory;
    }

    private function rebuild():Void {
        for (view in dataViews) {
            view.content.removeEventListener(MouseEvent.CLICK, onItemClick);
        }
        objectIndexes = new Map();
        dataViews = [for (index in 0...data.length) factory(index, data[index])];
        views = cast dataViews;
        for (i in 0...dataViews.length) {
            objectIndexes[dataViews[i].content] = i;
            dataViews[i].content.addEventListener(MouseEvent.CLICK, onItemClick);
        }
    }

    private function onItemClick(event:MouseEvent):Void {
        var index = objectIndexes[event.currentTarget];
        onDataSelect.emit(data[index]);
        onItemSelect.emit(index, data[index], dataViews[index]);
    }
}
