package hw.view;

import flash.display.BitmapData;
import hw.net.ImageLoader;
import hw.view.skin.BitmapSkin;
import hw.view.utils.BitmapUtil;
import hw.view.utils.DrawUtil.FillType;

class ImageView extends SpriteView {

    public var image(default, set):BitmapData;
    public var imageUrl(default, set):String;
    public var color(default, set):Int = -1;
    public var fillType(default, set):FillType;
    public var stretch:Bool = true;

    private var bitmapSkin:BitmapSkin = new BitmapSkin();
    private var coloredImage:BitmapData;

    public function new(image:BitmapData = null) {
        super();
        fillType = FillType.DEFAULT;
        skin = bitmapSkin;
        if (image != null) {
            this.image = image;
        }
    }

    private function set_image(value:BitmapData):BitmapData {
        if (image != value) {
            if (stretch) {
                setSize(value.width, value.height, "image");
            }
            image = value;
            if (color > -1) {
                coloredImage = BitmapUtil.colorize(image, color);
            }
            bitmapSkin.image = coloredImage == null ? image : coloredImage;
            toRedraw();
        }
        return image;
    }

    private function set_imageUrl(value:String):String {
        if (imageUrl != value) {
            imageUrl = value;
            new ImageLoader().GET(imageUrl).then(function(data) {
                image = data;
            }).catchError(function(e) L.w("ImageView", "load", e));
        }
        return imageUrl;
    }

    private function set_fillType(value:FillType):FillType {
        if (fillType != value) {
            bitmapSkin.fillType = fillType = value;
            toRedraw();
        }
        return fillType;
    }

    private function set_color(value:Int):Int {
        if (color != value) {
            color = value;
            if (image != null) {
                coloredImage = BitmapUtil.colorize(image, color);
                bitmapSkin.image = coloredImage;
            }
            toRedraw();
        }
        return color;
    }
}
