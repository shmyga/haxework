package hw.text;

import flash.geom.Point;
import flash.text.TextField;

class TextUtil {

  private static var K_HEIGHT = 1.185;

  public static function getSize(textField:TextField):Point {
    //ToDo: canvas.getContext("2d").measureText very slow
    #if js
    var s = 1;
    var t = textField.text;
    if (t != null) for (i in 0...t.length) {
      if (t.charCodeAt(i) == 10) s++;
    }
    var _textHeight = (textField.getTextFormat().size + 2) * s * K_HEIGHT;
    var _textWidth = textField.width;
    #else
    var _textWidth = textField.textWidth;
    var _textHeight = textField.textHeight * K_HEIGHT;
    #end
    return new Point(_textWidth, _textHeight);
  }
}
