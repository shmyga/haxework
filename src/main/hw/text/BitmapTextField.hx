package hw.text;

import haxe.Timer;
import flash.text.TextFormat;
import flash.events.Event;
import flash.display.Bitmap;
import flash.geom.Matrix;
import flash.display.BitmapData;
import flash.text.TextField;

class BitmapTextField extends TextField {

  public var shadow(default, set):Bool = false;
  public var stroke(default, set):Bool = false;
  public var shadowColor(default, set):Int = 0x000000;
  public var shadowAlpha(default, set):Float = 0.6;

  private static var K_HEIGHT = 1.185;

  private var bitmap:Bitmap;

  private var _text:String;
  private var _invalidated:Bool;
  private var _invalidatedNext:Bool;

  public function new()  {
    super();
    bitmap = new Bitmap();
    bitmap.smoothing = true;
    visible = false;
    addEventListener(Event.ADDED, onAdded);
    addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
    addEventListener(Event.REMOVED, onRemoved);
    addEventListener(Event.ENTER_FRAME, onEnterFrame);
  }

  private function set_shadow(value) {
    if (shadow != value) {
      shadow = value;
      _invalidated = true;
    }
    return shadow;
  }

  private function set_stroke(value) {
    if (stroke != value) {
      stroke = value;
      _invalidated = true;
    }
    return stroke;
  }

  private function set_shadowColor(value) {
    if (shadowColor != value) {
      shadowColor = value;
      _invalidated = true;
    }
    return shadowColor;
  }


  private function set_shadowAlpha(value) {
    if (shadowAlpha != value) {
      shadowAlpha = value;
      _invalidated = true;
    }
    return shadowAlpha;
  }

  private function onAdded(_) {
    parent.addChild(bitmap);
    _invalidated = true;
  }

  private function onAddedToStage(_) {
    _invalidatedNext = true;
  }

  private function onRemoved(_) {
    if (bitmap.parent != null) {
      bitmap.parent.removeChild(bitmap);
    }
  }

  private function onEnterFrame(_) {
    if (_text != text) {
      _text = text;
      _invalidated = true;
    }
    if (_invalidated) {
      redraw();
    }
    if (_invalidatedNext) {
      _invalidated = true;
      _invalidatedNext = false;
    }
  }

  private function redraw() {
    var size = TextUtil.getSize(this);
    //ToDo: openfl 3.0.6 -> 3.1.0
    Timer.delay(function() {
      bitmap.bitmapData = draw(this, size.x, size.y, shadowColor, shadowAlpha, shadow, stroke);
    }, 1);
    bitmap.x = x;
    bitmap.y = y;
    _invalidated = false;
  }

  public static function draw(textField:TextField, textWidth:Float, textHeight:Float, color:Int, alpha:Float, shadow:Bool, stroke:Bool):BitmapData {
    textField.visible = true;
    var bd = new BitmapData(Std.int(textWidth) + 6, Std.int(textHeight) + 6, true, 0x00000000);

    if (shadow || stroke) {
      var baseTf = textField.getTextFormat();
      var tf = textField.getTextFormat();
      tf.color = color;
      textField.setTextFormat(tf);
      textField.alpha = alpha;

      if (stroke) {
        for (p in [[1, 2], [2, 1], [1, 0], [0, 1]]) {
          var m = new Matrix();
          m.translate(p[0], p[1]);
          bd.draw(textField, m, null, null, null, false);
        }
      }

      if (shadow) {
        for (p in [[2, 3], [3, 2], [1, 0], [0, 1]]) {
          var m = new Matrix();
          m.translate(p[0], p[1]);
          bd.draw(textField, m, null, null, null, false);
        }
      }

      textField.alpha = 1.0;
      textField.setTextFormat(baseTf);
    }

    var m = new Matrix();
    m.translate(1, 1);
    bd.draw(textField, m, null, null, null, false);

    textField.visible = false;
    return bd;
  }
}
