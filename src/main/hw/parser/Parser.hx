package hw.parser;

import haxe.macro.Context;
import haxe.macro.Expr;
import haxe.macro.Type;
import haxe.macro.TypeTools;
import hw.macro.ClassProvideMacro;
import hw.macro.ClassTypeMacro;
import hw.macro.DispatcherMacro;
import hw.macro.FieldMacro;
import hw.macro.ProvideMacro;
import hw.macro.ResourceMacro;
import hw.macro.SingletonMacro;
import hw.macro.StyleMacro;
import hw.macro.TemplateMacro;

class Parser {

    private static var classTypeMacros:Array<ClassTypeMacro> = [
        new ClassProvideMacro(),
        new TemplateMacro(),
        new DispatcherMacro(),
        new StyleMacro(),
        new SingletonMacro(),
    ];

    private static var fieldMacros:Array<FieldMacro> = [
        new ProvideMacro(),
        new ResourceMacro(),
    ];

    private static var processed:Map<String, Bool> = new Map();

    private static function auto():Void {
        haxe.macro.Compiler.addGlobalMetadata("", "@:build(hw.parser.Parser.autoRun())", true, true, false);
    }

    private static macro function autoRun():Array<Field> {
        var localType = Context.getLocalType();
        return switch localType {
            case Type.TInst(_.get() => ct, _):
                var localName = TypeTools.toString(localType);
                if (processed.exists(localName)) {
                    return null;
                }
                var modify:Bool = false;
                var fields:Array<Field> = Context.getBuildFields();
                var result:Array<Field> = [];
                // process fields meta
                for (field in fields) {
                    result.push(field);
                    for (item in fieldMacros) {
                        if (item.has(field)) {
                            modify = true;
                            result = result.concat(item.apply(field));
                        }
                    }
                }
                if (modify) {
                    fields = result;
                }
                // process class meta
                for (item in classTypeMacros) {
                    if (item.has(ct)) {
                        modify = true;
                        fields = item.apply(ct, fields);
                    }
                }
                processed.set(localName, true);
                modify ? fields : null;
            default: null;
        }
    }
}
