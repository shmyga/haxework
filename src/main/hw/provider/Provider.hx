package hw.provider;

abstract KeyType<T>(String) {

    public function new(value:String) {
        this = value;
    }

    @:from public static inline function fromClass<T>(value:Class<T>):KeyType<T> {
        return new KeyType<T>(Type.getClassName(value));
    }

    @:from public static inline function fromEnum<T>(value:Enum<T>):KeyType<T> {
        return new KeyType<T>(Type.getEnumName(value));
    }
}

@:singleton class Provider {

    private static function key<T>(i:KeyType<T>, ?type:Dynamic):String {
        var result:String = Std.string(i);
        if (type != null) result += ':${Std.string(type)}';
        return result;
    }

    public var factories:Map<String, Class<Dynamic>>;
    private var args:Map<String, Array<Dynamic>>;
    private var instances:Map<String, Dynamic>;

    public function new() {
        factories = new Map();
        args = new Map();
        instances = new Map();
        resolveDefaultFactories();
    }

    macro static function resolveDefaultFactories() {
        var result = [];
        for (item in hw.macro.ClassProvideMacro.bundle) {
            // ToDo: ClassType to Expr?
            var k = haxe.macro.Context.parse(haxe.macro.MacroStringTools.toDotPath(item.key.pack, item.key.name), haxe.macro.Context.currentPos());
            var v = haxe.macro.Context.parse(haxe.macro.MacroStringTools.toDotPath(item.value.pack, item.value.name), haxe.macro.Context.currentPos());
            result.push(macro setFactory(${k}, ${v}));
        }
        return macro $b{result};
    }

    public function setFactory<T>(i:KeyType<T>, clazz:Class<T>, ?type:Dynamic, ?args:Array<Dynamic>):Void {
        var key = key(i, type);
        factories.set(key, clazz);
        if (args != null) this.args.set(key, args);
    }

    public function set<T>(i:KeyType<T>, instance:T, ?type:Dynamic):Void {
        var key = key(i, type);
        instances.set(key, instance);
    }

    public function get<T>(i:KeyType<T>, ?type:Dynamic):T {
        var key = key(i, type);
        if (instances.exists(key)) {
            return instances.get(key);
        } else if (factories.exists(key)) {
            var instance:T = Type.createInstance(factories.get(key), args.exists(key) ? args.get(key) : []);
            instances.set(key, instance);
            return instance;
        } else {
            throw 'Factory for "${key}" not found';
        }
    }

    public function build<T>(i:Class<T>, ?type:Dynamic):T {
        var key = key(i, type);
        if (factories.exists(key)) {
            var instance:T = Type.createInstance(factories.get(key), args.exists(key) ? args.get(key) : []);
            return instance;
        } else {
            throw 'Factory for "${key}" not found';
        }
    }

    public function setProperty<T>(i:Class<T>, field:String, value:Dynamic, ?type:Dynamic):Void {
        var o:Dynamic = get(i, type);
        if (o != null && Reflect.hasField(o, field)) {
            Reflect.setProperty(o, field, value);
        }
    }
}
