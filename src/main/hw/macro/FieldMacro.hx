package hw.macro;

import haxe.macro.Expr;

using hw.macro.MacroUtil;

class FieldMacro {
    public var metaName(default, null):String;

    public function new(metaName:String) {
        this.metaName = metaName;
    }

    public function has(field:Field):Bool {
        return field.getFieldMeta(metaName) != null;
    }

    public function apply(field:Field):Array<Field> {
        return [];
    }
}
