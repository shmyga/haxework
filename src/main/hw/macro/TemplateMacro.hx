package hw.macro;

import haxe.macro.Context;
import haxe.macro.Expr;
import haxe.macro.Type;
import hw.macro.PositionJsonParser;
import Lambda;

using hw.macro.MacroUtil;

class TemplateMacro extends ClassTypeMacro {

    private var bindings:Map<String, String>;
    private var templateFile:String;
    private var template:Dynamic;
    private var i:Int;

    public function new() {
        super(":template");
    }

    private static function getSpecField(object:Dynamic, field:String):Dynamic {
        if (Reflect.hasField(object, "@" + field)) {
            return Reflect.field(object, "@" + field);
        } else if (Reflect.hasField(object, "$" + field)) {
            return Reflect.field(object, "$" + field);
        } else {
            return null;
        }
    }

    private function getPosition(?position:JsonKeyPosition):Position {
        var min = position == null ? 1 : position.min; // :-(
        var max = position == null ? 1 : position.max;
        var file = position == null || position.file == null ? templateFile : position.file;
        return Context.makePosition({min:min, max:max, file:file});
    }

    private function specialValue(name:String, key:String, a:Array<String>, position:JsonKeyPosition, exprs:Array<Expr>):Dynamic {
        return switch (a[0]) {
            case "asset" | "a":
                switch a[1] {
                    case "image":
                        'openfl.Assets.getBitmapData("${a[2]}")';
                    case _:
                        a[2];
                }
            case "resource" | "r":
                var bindExpr = 'hw.provider.Provider.instance.get(hw.resources.IResources).${a[1]}.bind("${a[2]}", ${name}, "${key}")';
                exprs.push(Context.parse(bindExpr, getPosition(position)));
                null;
            case "translate" | "t":
                'new hw.translate.TranslateString("${a[1]}")';
            case "template":
                var template = FileUtil.loadFile(a[1]);
                return createValue(name, key, template, position, exprs);
            case _:
                Context.error('Unsupported prefix "${a[0]}"', getPosition(position));
        }
    }

    private function createValue(name:String, key:String, value:Dynamic, position:JsonKeyPosition, exprs:Array<Expr>):Dynamic {
        return if (Std.is(value, Array)) {
            value.map(function(v) {
                return createValue(null, null, v, position, exprs);
            });
        } else if (Std.is(value, String)) {
            if (value.charAt(0) == "~" ) {
                value.substring(1, value.length);
            } else if (value.charAt(0) == "@" || value.charAt(0) == "$") {
                specialValue(name, key, value.substring(1, value.length).split(":"), position, exprs);
            } else if (~/(0x|#)[A-Fa-f\d]{6}/.match(value)) {
                Std.parseInt(StringTools.replace(Std.string(value), "#", "0x"));
            } else {
                "\"" + value + "\"";
            }
        } else if (Std.is(value, Float) || (Std.is(value, Bool))) {
            value;
        } else if (value != null) {
            var className:String = getSpecField(value, "class");
            if (className != null) {
                className;
            } else {
                var type:Dynamic = getSpecField(value, "type");
                if (type != null) {
                    var varName = 'a${i++}';
                    if (Std.is(type, Array)) {
                        var args = cast(type,Array<Dynamic>).slice(1).join(",");
                        exprs.push(Context.parse('var ${varName} = ${type[0]}(${args})', getPosition(position)));
                    } else if (type == "Dynamic") {
                        exprs.push(Context.parse('var ${varName} = cast {}', getPosition(position)));
                    } else {
                        exprs.push(Context.parse('var ${varName} = new ${type}()', getPosition(position)));
                    }
                    createElement(varName, value, exprs);
                    varName;
                } else {
                    createElement('${name}.${key}', value, exprs);
                    '${name}.${key}';
                }
            }
        } else {
            value;
        }
    }

    private function createElement(name:String, data:Dynamic, exprs:Array<Expr>):String {
        if (Reflect.hasField(data, "id")) {
            var id = Reflect.field(data, "id");
            // ToDo: only for view?
            if (Std.is(id, String) && bindings.exists(id)) {
                var bind = bindings.get(id);
                exprs.push(Context.parse('this.${bind} = ${name}', getPosition()));
                bindings.remove(id);
            }
        }

        for (key in Reflect.fields(data)) {
            if (key.charAt(0) == "$" || key.charAt(0) == "@") continue;
            var position = Reflect.field(data, "$" + key);
            var value = createValue(name, key, Reflect.field(data, key), position, exprs);
            if (value != null) {
                switch [key.charAt(0), key.charAt(key.length - 1)] {
                    case ["+", _]:
                        var e:Expr = Context.parse(value, getPosition(position));
                        e = switch e.expr {
                            case ECall(_, _): macro function(_) ${e};
                            case _: e;
                        }
                        exprs.push(macro $p{[name].concat(key.substr(1).split("."))}.connect(${e}));
                    case ["_", "_"]:
                        //exprs.push(Context.parse('${name}["${key.substr(1, key.length - 2)}"] = ${value}', getPosition(position)));
                        exprs.push(Context.parse('${name}.set("${key.substr(1, key.length - 2)}", ${value})', getPosition(position)));
                    case _:
                        exprs.push(Context.parse('${name}.${key} = ${value}', getPosition(position)));
                }
            }
        }

        return name;
    }

    private function buildBuild(exprs:Array<Expr>):Field {
        return {
            name: "build",
            access: [Access.APrivate],
            pos: getPosition(),
            kind: FieldType.FFun({
                args: [],
                expr: macro $b{exprs},
                params: [],
                ret: null
            })
        }
    }

    private function upgradeConstructor(constructor:Field = null):Field {
        if (constructor == null) {
            constructor = {
                name: "new",
                access: [Access.APublic],
                pos: getPosition(),
                kind: FieldType.FFun({
                    args: [],
                    expr: macro super(),
                    params: [],
                    ret: null
                })
            }
        }
        MacroUtil.upgradeField(constructor, macro build(), 1);
        return constructor;
    }

    private static function findViewsBindings(fields:Array<Field>):Map<String, String> {
        var result:Map<String, String> = new Map();
        for (field in fields) {
            var viewMeta = field.getFieldMeta(":view");
            if (viewMeta != null) {
                var viewId:String = viewMeta.params.length == 0 ? field.name : MacroUtil.getExprString(viewMeta.params[0].expr);
                result.set(viewId, field.name);
            }
        }
        return result;
    }

    override public function apply(classType:ClassType, fields:Array<Field>):Array<Field> {
        i = 0;
        var meta = classType.getClassMeta(metaName);
        var params = MacroUtil.getMetaParams(meta);
        var filePath = params[0];
        if (filePath == null) {
            filePath = classType.pack.join("/") + "/" + classType.name + ".yaml";
        }
        // ToDo: template builder
        templateFile = Context.resolvePath(filePath);
        template = FileUtil.loadFile(templateFile);
        bindings = findViewsBindings(fields);
        var result:Array<Field> = fields.slice(0);
        var exprs:Array<Expr> = [];
        createElement("this", template, exprs);
        result.push(buildBuild(exprs));
        var constructor = Lambda.find(result, function(f) return f.name == "new");
        if (constructor != null) {
            result.remove(constructor);
        }
        result.push(upgradeConstructor(constructor));
        if (Lambda.count(bindings) > 0) {
            var keys = Lambda.map({iterator: bindings.keys}, function(k) return '"${k}"').join(",");
            Context.error('Invalid @:view bindings: $keys', getPosition());
        }
        return result;
    }
}
