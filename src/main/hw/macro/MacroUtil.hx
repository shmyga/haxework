package hw.macro;

import haxe.macro.Context;
import haxe.macro.ExprTools;
import haxe.macro.Expr;
import haxe.macro.Type;

class MacroUtil {

    public static function getMetaParams(meta:MetadataEntry):Array<String> {
        return meta.params.map(function(param:Expr) return switch param.expr {
            case EConst(CString(value)): value;
            case _: null;
        });
    }

    public inline static function DynamicType():ComplexType {
        return ComplexType.TPath({name:'Dynamic', pack:[], params:[]});
    }

    public static function getClassMeta(classType:ClassType, metaName:String):Null<MetadataEntry> {
        for (meta in classType.meta.get()) {
            if (meta.name == metaName) {
                return meta;
            }
        }
        return null;
    }

    public static function getFieldMeta(field:Field, metaName:String):Null<MetadataEntry> {
        if (field.meta != null) {
            for (meta in field.meta) {
                if (meta.name == metaName) {
                    return meta;
                }
            }
        }
        return null;
    }

    public static function getExprString(expr:ExprDef) {
        return switch expr {
            case EConst(CString(value)): value;
            case _: null;
        }
    }

    public static function getComplexType(field:Field):Null<ComplexType> {
        return switch field.kind {
            case FProp(get, set, t, e): t;
            case FVar(t, e): t;
            case _: null;
        }
    }

    public static function getField(fields:Array<Field>, name:String):Null<Field> {
        return Lambda.find(fields, function(field:Field):Bool return field.name == name);
    }

    public static function getExprClassType(expr:Expr):Null<ClassType> {
        var typeName = ExprTools.toString(expr);
        return switch Context.getType(typeName) {
            case TInst(t, _): t.get();
            case _: null;
        }
    }

    public static function upgradeField(field:Field, expr:Expr, position:Int = 0):Field {
        switch field.kind {
            case FFun(f):
                var fieldExpr = f.expr;
                switch fieldExpr.expr {
                    case EBlock(exprs):
                        exprs.insert(position, expr);
                        fieldExpr = macro $b{exprs};
                    case _:
                        fieldExpr = macro $b{[fieldExpr, expr]}
                }
                f.expr = fieldExpr;
            case _:
        }
        return field;
    }
}
