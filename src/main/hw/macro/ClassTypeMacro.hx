package hw.macro;

import haxe.macro.Expr;
import haxe.macro.Type;

class ClassTypeMacro {

    public var metaName(default, null):String;

    public function new(metaName:String) {
        this.metaName = metaName;
    }

    public function has(classType:ClassType):Bool {
        return classType.meta.has(metaName);
    }

    public function apply(classType:ClassType, fields:Array<Field>):Array<Field> {
        return fields;
    }
}
