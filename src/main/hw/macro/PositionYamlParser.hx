package hw.macro;

import haxe.DynamicAccess;

class PositionYamlParser {

    private var filename:String;
    private var content:String;
    private var cache:Map<String, Int>;
    private var result:Dynamic;

    private function new(filename:String, content:String, result:Dynamic) {
        this.filename = filename;
        this.content = content;
        this.result = result;
        this.cache = new Map();
    }

    private function parseAll():Void {
        //parseBlock(result);
    }

    private function parseBlock(result:DynamicAccess<Dynamic>):Void {
        for (field in result.keys()) {
            var offset = content.indexOf(field, cache.get(field));
            cache.set(field, offset);
            result.set("$" + field, {
                min:offset,
                max:offset + field.length,
                file:filename,
            });
            var value = result.get(field);
            if (Std.is(value, Array)) {
                for (item in cast(value, Array<Dynamic>)) {
                    parseBlock(item);
                }
            } else {
                parseBlock(value);
            }
        }
    }

    public static inline function parse(filename:String, content:String, result:Dynamic):Void {
        new PositionYamlParser(filename, content, result).parseAll();
    }
}
