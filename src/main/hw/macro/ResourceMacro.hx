package hw.macro;

using hw.macro.MacroUtil;

class ResourceMacro extends FieldMacro {

    public function new() {
        super(":resource");
    }
}
