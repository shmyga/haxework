package hw.macro;

import haxe.macro.Expr;
import haxe.macro.Type;

class ClassProvideMacro extends ClassTypeMacro {

    public static var bundle(default, null):Array<{key: ClassType, value: ClassType}> = [];

    public function new() {
        super(":provide");
    }

    override public function apply(classType:ClassType, fields:Array<Field>):Array<Field> {
        var meta = MacroUtil.getClassMeta(classType, metaName);
        var valueType = meta.params.length == 0 ? classType : MacroUtil.getExprClassType(meta.params[0]);
        bundle.push({key: classType, value: valueType});
        return super.apply(classType, fields);
    }
}
