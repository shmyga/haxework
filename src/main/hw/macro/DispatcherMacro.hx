package hw.macro;

import haxe.macro.TypeTools;
import haxe.macro.Context;
import haxe.macro.ExprTools;
import haxe.macro.Expr;
import haxe.macro.Type;

class DispatcherMacro extends ClassTypeMacro {

    public function new() {
        super(":dispatcher");
    }

    private static inline function signalName(fieldName:String):String {
        if (fieldName.substr(0, 2) == "on") {
            return fieldName.substr(2, 1).toLowerCase() + fieldName.substr(3) + "Signal";
        } else {
            return fieldName + "Signal";
        }
    }

    override public function apply(classType:ClassType, fields:Array<Field>):Array<Field> {
        var result:Array<Field> = fields.slice(0);
        var typeName = ExprTools.toString(classType.meta.extract(metaName)[0].params[0]);
        var type:ClassType = MacroUtil.getExprClassType(classType.meta.extract(metaName)[0].params[0]);
        var fields = type.fields.get();
        for (field in fields) {
            var argsTypes:Array<Type> = switch field.type {
                case TFun(args, _): args.map(function(arg) return arg.t);
                case _ : [];
            }
            var signal = 'Signal${argsTypes.length}';
            var type = TPath({
                pack: ['hw', 'signal'],
                name: 'Signal',
                sub: signal,
                params: argsTypes.map(function(t) return TPType(TypeTools.toComplexType(t))),
            });
            result.push({
                name: signalName(field.name),
                access: [APublic],
                pos: Context.currentPos(),
                kind: FProp('default', 'null', type, Context.parse('new hw.signal.Signal.${signal}()', Context.currentPos())),
            });
        }
        result.push({
            name: 'connect',
            access: [APublic],
            pos: Context.currentPos(),
            kind: FFun({
                args: [{
                    name: 'listener',
                    type: TPath({
                        pack: type.pack,
                        name: type.name,
                    }),
                }],
                ret: null,
                expr: macro $b{fields.map(function(field) {
                    return Context.parse('${signalName(field.name)}.connect(listener.${field.name})', Context.currentPos());
                })},
            }),
        });
        result.push({
            name: 'disconnect',
            access: [APublic],
            pos: Context.currentPos(),
            kind: FFun({
                args: [{
                    name: 'listener',
                    type: TPath({
                        pack: type.pack,
                        name: type.name,
                    }),
                }],
                ret: null,
                expr: macro $b{fields.map(function(field) {
                    return Context.parse('${signalName(field.name)}.disconnect(listener.${field.name})', Context.currentPos());
                })},
            }),
        });
        return result;
    }
}
