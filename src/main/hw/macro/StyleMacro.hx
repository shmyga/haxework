package hw.macro;

import haxe.macro.Context;
import haxe.macro.Expr;
import haxe.macro.Type;

using haxe.macro.ComplexTypeTools;
using hw.macro.MacroUtil;
using haxe.macro.MacroStringTools;

typedef TProperty<T> = {
    var field: Field;
    var defaultValue: T;
}

class StyleMacro extends ClassTypeMacro {

    public function new() {
        super(":style");
    }

    private static function processPropertyField(field:Field):Array<Field> {
        var result:Array<Field> = [];
        var type:ComplexType = field.getComplexType();
        var meta = field.getFieldMeta(":style");
        var defaultValue:Expr = meta.params.length > 0 ? meta.params[0] : null;
        field.kind = FProp("get", "set", type);
        var defaultName = 'default_${field.name}';
        result.push({
            name: defaultName,
            access: [APrivate],
            pos: field.pos,
            kind: FVar(type, defaultValue),
        });
        var currentName = 'current_${field.name}';
        result.push({
            name: currentName,
            access: [APrivate],
            pos: field.pos,
            kind: FVar(type),
        });
        var themeName = 'theme_${field.name}';
        result.push({
            name: themeName,
            access: [APrivate],
            pos: field.pos,
            kind: FVar(type),
        });
        var getter = [];
        getter.push(macro var result = $i{currentName});
        getter.push(macro if (result == null) result = $i{themeName});
        getter.push(macro if (result == null) result = $i{defaultName});
        getter.push(macro return result);
        result.push({
            name: 'get_${field.name}',
            access: [APrivate],
            pos: field.pos,
            kind: FFun({
                args: [],
                expr: macro $b{getter},
                params: [],
                ret: type,
            })
        });
        result.push({
            name: 'set_${field.name}',
            access: [APrivate],
            pos: field.pos,
            kind: FFun({
                args: [{name: "value", type: type}],
                expr: macro $b{[
                macro $i{currentName} = value,
                macro return $i{field.name}
                ]},
                params: [],
                ret: type,
            })
        });
        return result;
    }

    private static function processStyledField(field:Field):Array<Field> {
        var type:ComplexType = field.getComplexType();
        var result:Array<Field> = [];
        field.kind = FProp("default", "set", type);
        var expr:Array<Expr> = [];
        expr.push(macro if (value == null) return null);
        expr.push(macro value.styleKey = (styleKey!=null?(styleKey+"."):"")+$v{field.name});
        expr.push(macro value.style = style);
        expr.push(macro $i{field.name} = value);
        expr.push(macro return $i{field.name});
        result.push({
            name: 'set_${field.name}',
            access: [APrivate],
            pos: field.pos,
            kind: FFun({
                args: [{name: "value", type: type}],
                expr: macro $b{expr},
                params: [],
                ret: type,
            })
        });
        return result;
    }

    private static function buildStyleKeyField(styleds:Array<Field>, overrideField:Bool):Array<Field> {
        var result:Array<Field> = [];
        var type:ComplexType = "String".toComplex();
        if (!overrideField) {
            result.push({
                name: "styleKey",
                access: [APublic],
                pos: Context.currentPos(),
                kind: FProp("default", "set", type),
            });
        }
        var expr:Array<Expr> = [];
        if (overrideField) {
            expr.push(macro super.styleKey = value);
        }
        expr.push(macro styleKey = value);
        for (field in styleds) {
            expr.push(macro $i{field.name}.styleKey = styleKey+"."+$v{field.name});
        }
        expr.push(macro return styleKey);
        var access:Array<Access> = [APrivate];
        if (overrideField) {
            access.push(AOverride);
        }
        result.push({
            name: 'set_styleKey',
            access: access,
            pos: Context.currentPos(),
            kind: FFun({
                args: [{name: "value", type: type}],
                expr: macro $b{expr},
                params: [],
                ret: type,
            })
        });
        return result;
    }

    private static function buildStyleField(properties:Array<Field>, styleds:Array<Field>, hasOnStyle:Bool, overrideField:Bool):Array<Field> {
        var result:Array<Field> = [];
        var type:ComplexType = "hw.view.theme.StyleId".toComplex();
        if (!overrideField) {
            result.push({
                name: "style",
                access: [APublic],
                pos: Context.currentPos(),
                kind: FProp("default", "set", type),
            });
        }
        var expr:Array<Expr> = [];
        if (overrideField) {
            expr.push(macro super.style = value);
        }
        expr.push(macro style = value);
        for (field in styleds) {
            expr.push(macro $i{field.name}.style = style);
        }
        for (field in properties) {
            var propertyName = 'theme_${field.name}';
            expr.push(macro $i{propertyName} = hw.provider.Provider.instance.get(hw.view.theme.ITheme).resolve((styleKey!=null?(styleKey+"."):"")+$v{field.name}, style));
        }
        for (field in styleds) {
            var propertyName = '${field.name}';
            expr.push(macro $i{propertyName} = hw.provider.Provider.instance.get(hw.view.theme.ITheme).resolve((styleKey!=null?(styleKey+"."):"")+$v{field.name}, style));
        }
        if (hasOnStyle) {
            expr.push(macro onStyle());
        }
        expr.push(macro return style);
        var access:Array<Access> = [APrivate];
        if (overrideField) {
            access.push(AOverride);
        }
        result.push({
            name: 'set_style',
            access: access,
            pos: Context.currentPos(),
            kind: FFun({
                args: [{name: "value", type: type}],
                expr: macro $b{expr},
                params: [],
                ret: type,
            })
        });
        return result;
    }

    override public function apply(classType:ClassType, fields:Array<Field>):Array<Field> {
        var overrideStyle = classType.getClassMeta(metaName).params.length > 0;
        var result:Array<Field> = fields.slice(0);
        var styleds:Array<Field> = [];
        var properties:Array<Field> = [];
        var newFields:Array<Field> = [];
        var hasOnStyle:Bool = fields.getField("onStyle") != null;
        for (field in fields) if (field.meta != null) {
            var meta = field.getFieldMeta(metaName);
            if (meta != null) {
                if (meta.params.length > 0) {
                    newFields = newFields.concat(processPropertyField(field));
                    properties.push(field);
                } else {
                    newFields = newFields.concat(processStyledField(field));
                    styleds.push(field);
                }
            }
        }
        result = result
        .concat(newFields)
        .concat(buildStyleKeyField(styleds, overrideStyle))
        .concat(buildStyleField(properties, styleds, hasOnStyle, overrideStyle));
        return result;
    }
}
