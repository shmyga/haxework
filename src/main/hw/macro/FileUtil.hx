package hw.macro;

import yaml.YamlException;
import yaml.Parser;
import yaml.Yaml;
import haxe.macro.Context;

class FileUtil {

    public static function loadJsonFile(path:String):Dynamic {
        Context.registerModuleDependency(Context.getLocalModule(), path);
        var content = sys.io.File.getContent(path);
        var json = null;
        try {
            json = PositionJsonParser.parse(content, path);
        } catch (error:Dynamic) {
            Context.error(error, Context.makePosition({min:0, max:0, file:path}));
        }
        Context.parse(content, Context.makePosition({min:0, max:0, file:path}));
        return json;
    }

    public static function loadYamlFile(path:String):Dynamic {
        path = sys.FileSystem.absolutePath(path);
        Context.registerModuleDependency(Context.getLocalModule(), path);
        var content = sys.io.File.getContent(path);
        var result = null;
        try {
            result = Yaml.parse(content, Parser.options().useObjects());
            PositionYamlParser.parse(path, content, result);
        } catch (error:YamlException) {
            Context.error(Std.string(error), Context.makePosition({min:0, max:0, file:path}));
        }
        Context.parse('/*$content*/""', Context.makePosition({min:0, max:0, file:path}));
        return result;
    }

    public static function loadFile(path:String):Dynamic {
        var ext = path.split('.').pop();
        return switch(ext) {
            case 'json': loadJsonFile(path);
            case 'yml' | 'yaml': loadYamlFile(path);
            case x: throw 'Unsupported file format: "${x}"';
        }
    }
}
