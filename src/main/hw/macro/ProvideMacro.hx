package hw.macro;

import haxe.macro.Context;
import haxe.macro.Expr;

using hw.macro.MacroUtil;

class ProvideMacro extends FieldMacro {

    public function new() {
        super(":provide");
    }

    override public function apply(field:Field):Array<Field> {
        var meta:MetadataEntry = field.getFieldMeta(metaName);
        var result:Array<Field> = [];
        var type:ComplexType = switch field.kind {
            case FVar(t): t;
            default: null;
        }
        var name:String = switch type {
            case TPath(p): p.name;
            default: null;
        }
        var provideType:String = meta.params.length == 0 ? null : MacroUtil.getExprString(meta.params[0].expr);
        var isStatic = Lambda.exists(field.access, function(a: Access) return a == AStatic);
        field.kind = FProp('get', 'set', type);
        var access = [APrivate, AInline];
        if (isStatic) access.push(AStatic);
        var args = [name];
        if (provideType != null) args.push('"${provideType}"');
        result.push({
            name: 'get_${field.name}',
            access: access,
            pos: field.pos,
            kind: FFun({
                args: [],
                expr: Context.parse('return hw.provider.Provider.instance.get(${args.join(',')})', field.pos),
                params: [],
                ret: type,
            })
        });
        args.insert(1, "value");
        result.push({
            name: 'set_${field.name}',
            access: access,
            pos: field.pos,
            kind: FFun({
                args: [{name: 'value', type: type}],
                expr: Context.parse('{hw.provider.Provider.instance.set(${args.join(',')}); return value;}', field.pos),
                params: [],
                ret: type,
            })
        });
        return result;
    }
}
