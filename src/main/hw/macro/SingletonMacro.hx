package hw.macro;

import haxe.macro.Context;
import haxe.macro.Expr;
import haxe.macro.Type;

class SingletonMacro extends ClassTypeMacro {

    public function new() {
        super(":singleton");
    }


    override public function apply(classType:ClassType, fields:Array<Field>):Array<Field> {
        var result:Array<Field> = fields.slice(0);
        var classTypePath:TypePath = {
            pack: classType.pack,
            name: classType.name,
        };
        var type:ComplexType = TPath(classTypePath);
        result.push({
            name: 'instance',
            access: [APublic, AStatic],
            pos: Context.currentPos(),
            kind: FProp('get', 'null', type),
        });
        var typeStr = classType.name;
        result.push({
            name: 'get_instance',
            access: [APrivate, AStatic],
            pos: Context.currentPos(),
            kind: FFun({
                args: [],
                ret: type,
                expr: macro $b{[
                macro if (instance == null) instance = new $classTypePath(),
                macro return instance
                ]},
            }),
        });
        return result;
    }
}
