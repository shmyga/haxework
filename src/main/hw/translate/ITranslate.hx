package hw.translate;

interface ITranslate {
    public function get(key:String):String;
    public function format(key:String, args:Array<Dynamic>):String;
    public function formatPlurar(key:String, num:Int, args:Array<Dynamic>):String;
    public function getArray(key:String):Array<String>;
    public function getPlural(key:String, num:Int):String;
}
