package hw.translate;

class Translate implements ITranslate {

    public var lang(default, null):String;
    private var data:Dynamic;

    public function new(lang:String, data:Dynamic) {
        this.lang = lang;
        this.data = data;
    }

    public function format(key:String, args:Array<Dynamic>):String {
        var string:String = get(key);
        for (i in 0...args.length) {
            var arg:Dynamic = args[i];
            string = StringTools.replace(string, '{$i}', arg);
        }
        return string;
    }

    public function formatPlurar(key:String, num:Int, args:Array<Dynamic>):String {
        var string:String = getPlural(key, num);
        for (i in 0...args.length) {
            var arg:Dynamic = args[i];
            string = StringTools.replace(string, '{$i}', arg);
        }
        return string;
    }

    public function get(key:String):String {
        var result = getObject(key);
        return result != null ? result : key;
    }

    public function getArray(key:String):Array<String> {
        var result = getObject(key);
        return result != null ? result : [];
    }

    public function getPlural(key:String, num:Int):String {
        var array:Array<String> = getArray(key);
        switch (lang) {
            case "ru": return plurarRU(array, num);
            default: return plurarDefault(array, num);
        }
    }

    private function getObject(key:String):Dynamic {
        var path:Array<String> = key.split(".");
        var tmp:Dynamic = data;
        for (i in 0...path.length) {
            if (Reflect.hasField(tmp, path[i])) {
                tmp = Reflect.field(tmp, path[i]);
            } else {
                return null;
            }
        }
        return tmp;
    }

    private function plurarRU(strings:Array<String>, num:Int):String {
        if (num > 10 && num < 20) return strings[2];
        var n:Int = num % 10;
        switch (n) {
            case 1: return strings[0];
            case 2: return strings[1];
            case 3: return strings[1];
            case 4: return strings[1];
            default: return strings[2];
        }
    }

    private function plurarDefault(strings:Array<String>, num:Int):String {
        var n:Int = num % 10;
        switch (n) {
            case 1: return strings[0];
            default: return strings[1];
        }
    }
}
