package hw.translate;

import hw.provider.Provider;

abstract TranslateString(String) from String to String {
    @:provide var translate:ITranslate;

    inline public function new(value:String, args:Array<Dynamic> = null) {
        if (args != null) {
            this = translate.format(value, args);
        } else {
            this = translate.get(value);
        }
    }
}

abstract TranslateArrayString(String) from String to String {
    @:provide var translate:ITranslate;

    inline public function new(value:String, index:Int) {
        this = translate.getArray(value)[index];
    }
}

abstract TranslatePluralString(String) from String to String {
    @:provide var translate:ITranslate;

    inline public function new(value:String, num:Int, args:Array<Dynamic> = null) {
        if (args != null) {
            this = translate.formatPlurar(value, num, args);
        } else {
            this = translate.getPlural(value, num);
        }
    }
}
