package hw.app;

import flash.Lib;
import flash.system.Capabilities;

@:singleton class Const {
    public var FPS(default, null):Int;
    public var BUILD(default, null):String;
    public var VERSION(default, null):String;
    public var NAME(default, null):String;
    public var DEBUG(default, null):Bool;

    public function new():Void {
        BUILD = CompilationOption.get("build");
        #if lime
        FPS = Std.parseInt(Lib.current.stage.application.meta.get("fps"));
        VERSION = Lib.current.stage.application.meta.get("version");
        NAME = Lib.current.stage.application.meta.get("name");
        #end
        DEBUG = Capabilities.isDebugger;
    }
}
