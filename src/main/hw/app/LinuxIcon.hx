package hw.app;

import flash.display.BitmapData;
import flash.filters.ColorMatrixFilter;
import flash.geom.Point;
import flash.Lib;

class LinuxIcon {

    public static var value(default, set):BitmapData;

    private static function set_value(value:BitmapData):BitmapData {
        LinuxIcon.value = value;
        Lib.current.stage.window.setIcon(prepareIcon(value).image);
        return LinuxIcon.value;
    }

    private static function prepareIcon(bitmap:BitmapData):BitmapData {
        var matrix:Array<Float> = [];
        matrix = matrix.concat([0, 0, 1, 0, 0]);
        matrix = matrix.concat([0, 1, 0, 0, 0]);
        matrix = matrix.concat([1, 0, 0, 0, 0]);
        matrix = matrix.concat([0, 0, 0, 1, 0]);
        var cmf:ColorMatrixFilter = new ColorMatrixFilter(matrix);
        var bitmap:BitmapData = bitmap.clone();
        bitmap.applyFilter(bitmap, bitmap.rect, new Point(0, 0), cmf);
        return bitmap;
    }
}
