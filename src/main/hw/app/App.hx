package hw.app;

import flash.display.BitmapData;
import flash.display.StageDisplayState;
import flash.events.FullScreenEvent;
import flash.Lib;
import hw.animate.FadeAnimate;
import hw.animate.IAnimate;
import hw.animate.UnFadeAnimate;
import hw.resources.IResources;
import hw.signal.Signal;
import hw.view.IView;
import hw.view.popup.PopupManager;
import hw.view.Root;
import hw.view.theme.ITheme;

class App {

    public var view(default, set):IView<Dynamic>;

    private function set_view(value:IView<Dynamic>):IView<Dynamic> {
        view = value;
        Root.bind(view);
        return view;
    }

    public var icon(default, set):BitmapData;

    private function set_icon(value:BitmapData):BitmapData {
        icon = value;
        #if linux
        if (icon != null) {
            hw.app.LinuxIcon.value = icon;
        }
        #end
        return icon;
    }

    public var fullScreenSupport(get, never):Bool;

    public function get_fullScreenSupport():Bool {
        return Lib.current.stage.allowsFullScreen;
    }

    public var fullScreen(get, set):Bool;

    private function get_fullScreen():Bool {
        return Lib.current.stage.displayState != StageDisplayState.NORMAL;
    }

    private function set_fullScreen(value:Bool):Bool {
        Lib.current.stage.displayState = value ? StageDisplayState.FULL_SCREEN : StageDisplayState.NORMAL;
        return get_fullScreen();
    }

    public var fullScreenSignal(default, null):Signal<Bool> = new Signal();

    @:provide static var app:App;
    @:provide static var resources:IResources;
    @:provide static var popupManager:PopupManager;

    @:provide public var theme:ITheme;

    public function new() {
        Lib.current.stage.stageFocusRect = false;
        Lib.current.stage.addEventListener(FullScreenEvent.FULL_SCREEN, event -> fullScreenSignal.emit(event.fullScreen));

        popupManager.showAnimateFactory = createShowAnimate;
        popupManager.closeAnimateFactory = createCloseAnimate;

        resources.text.put("app.version", Const.instance.VERSION);
        resources.text.put("app.name", Const.instance.NAME);

        app = this;
    }

    private function createShowAnimate(view:IView<Dynamic>):IAnimate {
        return new UnFadeAnimate(view);
    }

    private function createCloseAnimate(view:IView<Dynamic>):IAnimate {
        return new FadeAnimate(view);
    }
}
