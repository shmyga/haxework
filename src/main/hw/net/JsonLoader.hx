package hw.net;

import flash.utils.ByteArray;
import flash.events.Event;
import flash.net.URLLoader;
import haxe.Json;

class JsonLoader<T> extends BaseURLLoader<T> {

  override private function extrudeResult(e:Event):T {
    var str:String = null;
    var data:T = null;
    try {
      str = Std.string(cast(e.currentTarget, URLLoader).data);
      data = Json.parse(str);
    } catch (error:Dynamic) {
      throwError(error);
    }
    return data;
  }

  override private function extrudeResultFromBytes(bytes:ByteArray):T {
    var str:String = null;
    var data:T = null;
    try {
      str = bytes.readUTFBytes(bytes.length);
      data = Json.parse(str);
    } catch (error:Dynamic) {
      throwError(error);
    }
    return data;
  }
}
