package hw.net;

import flash.display.MovieClip;
import flash.display.LoaderInfo;
import flash.display.Loader;
import flash.events.Event;

class SwfLoader extends BaseMediaLoader<MovieClip> {

  override private function extrudeResult(e:Event):MovieClip {
    var content:MovieClip = cast(cast(e.currentTarget, LoaderInfo).loader.content, MovieClip);
    content.gotoAndStop(0);
    return content;
  }
}

