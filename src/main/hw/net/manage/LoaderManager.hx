package hw.net.manage;

class LoaderManager implements ILoaderManager {

  private static inline var TAG:String = "LoaderManager";

  public var actives(default, null):Array<ILoader<Dynamic>>;
  public var queue(default, null):Array<ILoader<Dynamic>>;
  public var limit(default, default):Int;

  public function new(limit:Int = 10) {
    queue = new Array<ILoader<Dynamic>>();
    actives = new Array<ILoader<Dynamic>>();
    this.limit = limit;
  }

  public function add(loader:ILoader<Dynamic>):Void {
    if (actives.length >= limit) {
      queue.push(loader);
    } else {
      run(loader);
    }
  }

  private function run(loader:ILoader<Dynamic>):Void {
    actives.push(loader);
    loader.run();
  }

  public function release(loader:ILoader<Dynamic>):Void {
    actives.remove(loader);
    if (queue.length > 0 && actives.length < limit) {
      run(queue.shift());
    }
  }
}
