package hw.net.manage;

@:provide(LoaderManager) interface ILoaderManager {
  public var actives(default, null):Array<ILoader<Dynamic>>;
  public var queue(default, null):Array<ILoader<Dynamic>>;
  public var limit(default, default):Int;

  public function add(loader:ILoader<Dynamic>):Void;
  public function release(loader:ILoader<Dynamic>):Void;
}
