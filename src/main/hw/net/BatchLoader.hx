package hw.net;

import promhx.Promise;
import promhx.Deferred;

class BatchLoader<T> {

  public var factory:Class<ILoader<T>>;

  public function new(factory:Class<ILoader<T>>) {
    this.factory = factory;
  }

  public function GET(urls:Array<String>):Promise<Array<T>> {
    return Promise.whenAll(urls.map(function(url:String):Promise<T> {
      var loader:ILoader<T> = Type.createInstance(factory, []);
      return loader.GET(url);
    }));
  }
}
