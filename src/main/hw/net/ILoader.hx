package hw.net;

import promhx.Promise;
import flash.utils.ByteArray;

interface ILoader<T> {
  public var timeout(default, default):Int;
  public var busy(default, null):Bool;
  public var completed(default, null):Float;

  public function request(url:String, method:String, data:Dynamic = null):Promise<T>;
  public function fromBytes(data:ByteArray):Promise<T>;
  public function GET(url:String, data:Dynamic = null):Promise<T>;
  public function POST(url:String, data:Dynamic = null):Promise<T>;
  public function DELETE(url:String, data:Dynamic = null):Promise<T>;
  public function cancel():Void;

  public function run():Void;
}
