package hw.net;

import flash.events.ProgressEvent;
import flash.system.Security;
import flash.system.SecurityDomain;
import flash.system.ApplicationDomain;
import flash.system.LoaderContext;
import flash.utils.ByteArray;
import hw.net.BaseLoader;
import flash.events.SecurityErrorEvent;
import flash.events.IOErrorEvent;
import flash.events.Event;
import flash.net.URLRequest;
import flash.display.Loader;

class BaseMediaLoader<T> extends BaseLoader<T> {

  private var loader:Loader;

  override private function internalRequest(url:String):Void {
    //L.d("BaseMediaLoader", "request: " + url);
    cockTimeout();
    loader = buildLoader();
    loader.load(new URLRequest(url), buildLoaderContext(false));
  }

  override private function internalFromBytes(data:ByteArray):Void {
    loader = buildLoader();
    #if flash
    loader.loadBytes(data, buildLoaderContext(true));
    #else
    loader.loadBytes(data);
    #end
  }

  private function buildLoaderContext(bytes:Bool):LoaderContext {
    #if flash
    return switch (Security.sandboxType) {
      case Security.REMOTE:
        //null;
        bytes ? null : new LoaderContext(true, ApplicationDomain.currentDomain, SecurityDomain.currentDomain);
      case Security.APPLICATION:
        var loaderContext:LoaderContext = new LoaderContext();
        loaderContext.allowLoadBytesCodeExecution = true;
        loaderContext;
      default:
        null;
    }
    #else
    return null;
    #end
  }

  private function buildLoader():Loader {
    var loader:Loader = new Loader();
    loader.contentLoaderInfo.addEventListener(Event.INIT, onInit);
    loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onComplete);
    loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onError);
    loader.contentLoaderInfo.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecurityError);
    loader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onProgress);
    return loader;
  }

  override private function dispose():Void {
    super.dispose();
    if (loader != null) {
      loader.contentLoaderInfo.removeEventListener(Event.INIT, onInit);
      loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onComplete);
      loader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onError);
      loader.contentLoaderInfo.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecurityError);
      loader.contentLoaderInfo.removeEventListener(ProgressEvent.PROGRESS, onProgress);
      #if flash try { loader.close(); } catch (error:Dynamic) {} #end
      loader = null;
    }
  }
}

