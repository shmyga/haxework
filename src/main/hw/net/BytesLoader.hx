package hw.net;

import flash.net.URLLoaderDataFormat;
import flash.utils.ByteArray;
import flash.net.URLLoader;
import flash.events.Event;

class BytesLoader extends BaseURLLoader<ByteArray> {

  public function new() {
    super(URLLoaderDataFormat.BINARY);
  }

  override private function extrudeResult(e:Event):ByteArray {
    return cast(cast(e.currentTarget, URLLoader).data, ByteArray);
  }

  override private function extrudeResultFromBytes(bytes:ByteArray):ByteArray {
    return bytes;
  }
}
