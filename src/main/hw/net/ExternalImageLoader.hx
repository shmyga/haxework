package hw.net;

import flash.system.Security;
import flash.utils.ByteArray;
import flash.display.LoaderInfo;
import flash.display.Loader;
import flash.display.Bitmap;
import flash.events.Event;
import flash.display.BitmapData;

class ExternalImageLoader extends BaseMediaLoader<BitmapData> {

  private var internalLoader:InternalLoader;

  override private function extrudeResult(e:Event):BitmapData {
    var loader:Loader = cast(e.currentTarget, LoaderInfo).loader;
    #if flash
    if (Security.sandboxType == Security.APPLICATION) {
      var content:Bitmap = cast(loader.content,Bitmap);
      return content.bitmapData;
    } else {
    #end
      internalLoader = new InternalLoader();
      internalLoader.fromBytes(loader.contentLoaderInfo.bytes)
        .then(function(data:BitmapData):Void {
          resolve(data);
        })
        .catchError(function(error:Dynamic):Void {
          throwError(e);
        });
      return null;
    #if flash} #end
  }

  override public function dispose():Void {
    super.dispose();
    if (internalLoader != null) {
      internalLoader.dispose();
      internalLoader = null;
    }
  }
}

class InternalLoader extends BaseMediaLoader<BitmapData> {

  override private function extrudeResult(e:Event):BitmapData {
    var loader:Loader = cast(e.currentTarget, LoaderInfo).loader;
    var bitmapData:BitmapData = new BitmapData(Math.round(loader.width), Math.round(loader.height), true, 0x00000000);
    bitmapData.draw(loader);
    loader.unloadAndStop();
    return bitmapData;
  }
}

