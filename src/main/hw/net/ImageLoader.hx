package hw.net;

import flash.display.LoaderInfo;
import flash.display.Loader;
import flash.display.Bitmap;
import flash.events.Event;
import flash.display.BitmapData;

class ImageLoader extends BaseMediaLoader<BitmapData> {

  override private function extrudeResult(e:Event):BitmapData {
    var content:Bitmap = cast(cast(e.currentTarget, LoaderInfo).loader.content,Bitmap);
    return content.bitmapData;
  }
}

