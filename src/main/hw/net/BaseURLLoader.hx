package hw.net;

import flash.events.ErrorEvent;
import haxe.Timer;
import flash.utils.ByteArray;
import flash.net.URLRequestHeader;
import flash.net.URLRequestMethod;
import flash.net.URLVariables;
import flash.events.ProgressEvent;
import flash.net.URLLoaderDataFormat;
import flash.events.SecurityErrorEvent;
import flash.events.IOErrorEvent;
import flash.net.URLRequest;
import flash.events.Event;
import flash.net.URLLoader;
import promhx.Thenable;

class BaseURLLoader<T> extends BaseLoader<T> {

  private var dataFormat:URLLoaderDataFormat;
  private var loader:URLLoader;

  public function new(dateFormat:URLLoaderDataFormat = null) {
    super();
    this.dataFormat = (dateFormat == null ? URLLoaderDataFormat.TEXT : dateFormat);
  }

  override private function internalRequest(url:String):Void {
    cockTimeout();
    loader = buildLoader();
    var request:URLRequest = new URLRequest(url);
    if (method != URLRequestMethod.POST && method != URLRequestMethod.GET) {
      request.method = URLRequestMethod.POST;
      request.requestHeaders.push(new URLRequestHeader("X-HTTP-Method-Override", method));
    } else {
      request.method = method;
    }
    if (data != null && method == URLRequestMethod.POST) {
      var variables:URLVariables = new URLVariables();
      for (key in Reflect.fields(data)) {
        Reflect.setField(variables, key, Reflect.field(data, key));
      }
      request.data = variables;
    }
    loader.load(request);
  }

  override private function internalFromBytes(data:ByteArray):Void {
    if (data == null) {
      throwError("Content not found");
    } else {
      var data:T = extrudeResultFromBytes(data);
      resolve(data);
    }
    dispose();
  }

  private function extrudeResultFromBytes(bytes:ByteArray):T {
    throw "Abstract";
    return null;
  }

  private function buildLoader():URLLoader {
    var loader:URLLoader = new URLLoader();
    loader.dataFormat = dataFormat;
    loader.addEventListener(Event.COMPLETE, onComplete);
    loader.addEventListener(IOErrorEvent.IO_ERROR, onError);
    loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecurityError);
    loader.addEventListener(ProgressEvent.PROGRESS, onProgress);
    return loader;
  }

  override private function dispose():Void {
    super.dispose();
    if (loader != null) {
      loader.removeEventListener(Event.COMPLETE, onComplete);
      loader.removeEventListener(IOErrorEvent.IO_ERROR, onError);
      loader.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecurityError);
      loader.removeEventListener(ProgressEvent.PROGRESS, onProgress);
      try { loader.close(); } catch (error:Dynamic) {}
      loader = null;
    }
  }

  override public function cancel():Void {
    if (loader != null) {
      try {loader.close();} catch (error:Dynamic) {}
    }
    super.cancel();
  }

  override private function onError(e:Event):Void {
    var error:String = extrudeError(loader.data);
    if (error != null && error != "") {
      error = error + ". URL: " + url;
    } else if (Std.is(e, ErrorEvent)) {
      error = cast(e, ErrorEvent).text;
    } else {
      error = "Unknown Error. URL: " + url;
    }
    throwError(error);
    dispose();
  }

  private function extrudeError(data:Dynamic):String {
    return if (data == null) null else {
      var s:String = Std.string(data);
      var r:EReg = ~/<h1>(.*?)<\/h1>/;
      if (r.match(s)) {
        r.matched(1);
      } else {
        s;
      }
    }
  }
}

