package hw.net;

import flash.net.URLLoader;
import flash.events.Event;

class TextLoader extends BaseURLLoader<String> {

  override private function extrudeResult(e:Event):String {
    return Std.string(cast(e.currentTarget, URLLoader).data);
  }
}
