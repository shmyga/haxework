package hw.net;

import flash.events.Event;
import flash.net.URLLoader;

class XmlLoader extends BaseURLLoader<Xml> {

  override private function extrudeResult(e:Event):Xml {
    return Xml.parse(Std.string(cast(e.currentTarget, URLLoader).data));
  }
}
