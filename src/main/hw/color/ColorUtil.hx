package hw.color;

class ColorUtil {

    public static function floor(colorPart:Float):Int {
        return Std.int(Math.max(0, Math.min(255, colorPart)));
    }

    public static function multiply(color:Color, m:Float):Color {
        return [
            color.alpha,
            floor(color.red * m),
            floor(color.green * m),
            floor(color.blue * m),
        ];
    }

    public static function diff(color:Color, d:Int):Color {
        return [
            color.alpha,
            floor(color.red + d),
            floor(color.green + d),
            floor(color.blue + d),
        ];
    }

    public static function grey(color:Color):Color {
        var m = (color.red + color.green + color.blue) / 3;
        return [
            color.alpha,
            floor(m),
            floor(m),
            floor(m),
        ];
    }
}
