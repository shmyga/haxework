package hw.color;

abstract Color(Int) {
    public var alpha(get, never):Int;
    public var red(get, never):Int;
    public var green(get, never):Int;
    public var blue(get, never):Int;
    public var zero(get, never):Bool;

    public inline function new(value:Int) {
        this = value;
    }

    private inline function get_alpha():Int {
        return (this >> 24) & 255;
    }

    private inline function get_red():Int {
        return (this >> 16) & 255;
    }

    private inline function get_green():Int {
        return (this >> 8) & 255;
    }

    private inline function get_blue():Int {
        return this & 255;
    }

    private inline function get_zero():Bool {
        return green == 0 && red == 0 && blue == 0;
    }

    @:from static public inline function fromArray(value:Array<Int>):Color {
        return new Color((value[0] << 24) + (value[1] << 16) + (value[2] << 8) + value[3]);
    }

    @:from static public inline function fromInt(value:Int):Color {
        return new Color(value);
    }

    @:to public inline function toInt():Int {
        return this;
    }

    @:from static public inline function fromString(value:String):Color {
        return new Color(switch value {
            case "white": 0xFFFFFF;
            case "silver": 0xC0C0C0;
            case "gray": 0x808080;
            case "black": 0x000000;
            case "red": 0xFF0000;
            case "maroon": 0x800000;
            case "yellow": 0xFFFF00;
            case "olive": 0x808000;
            case "lime": 0x00FF00;
            case "green": 0x008000;
            case "aqua": 0x00FFFF;
            case "teal": 0x008080;
            case "blue": 0x0000FF;
            case "navy": 0x000080;
            case "fuchsia": 0xFF00FF;
            case "purple": 0x800080;
            case x: Std.parseInt('0x${x.split('#').pop()}');
        });
    }

    @:to public inline function toString():String {
        return StringTools.hex(this);
    }
}
