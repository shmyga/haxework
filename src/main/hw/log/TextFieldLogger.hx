package hw.log;

import flash.text.TextField;


class TextFieldLogger extends BaseLogger {

    private var textField:TextField;

    public function new(textField:TextField) {
        super();
        this.textField = textField;
    }

    override private function write(text:String):Void {
        this.textField.text += ("\n" + text);
    }
}
