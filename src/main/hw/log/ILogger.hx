package hw.log;

enum LogLevel {
  DEBUG; INFO; WARNING; ERROR;
}

interface ILogger {
  public function log(level:LogLevel, tag:String, message:String, ?error:Dynamic, ?p:haxe.PosInfos):Void;
  public function d(tag:String, message:String, ?error:Dynamic, ?p:haxe.PosInfos):Void;
  public function i(tag:String, message:String, ?error:Dynamic, ?p:haxe.PosInfos):Void;
  public function w(tag:String, message:String, ?error:Dynamic, ?p:haxe.PosInfos):Void;
  public function e(tag:String, message:String, ?error:Dynamic, ?p:haxe.PosInfos):Void;
}
