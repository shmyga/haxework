package hw.log;

import hw.log.BaseLogger;

#if js
class SocketLogger extends BaseLogger {}
#else

#if flash
import flash.net.Socket;
import flash.events.IOErrorEvent;
import flash.events.SecurityErrorEvent;
#else
import sys.net.Host;
import sys.net.Socket;
#end


class SocketLogger extends BaseLogger {

    private var socket:Socket;

    public function new() {
        super();
        socket = new Socket();
        #if flash
        socket.addEventListener(IOErrorEvent.IO_ERROR, function(error):Void {
            onError(error);
        });
        socket.addEventListener(SecurityErrorEvent.SECURITY_ERROR, function(error):Void {
            onError(error);
        });
        socket.connect(CompilationOption.get("debug.address"), Std.parseInt(CompilationOption.get("debug.port")));
        #else
    try {
      socket.connect(new Host(CompilationOption.get("debug.address")), Std.parseInt(CompilationOption.get("debug.port")));
    } catch (error: Dynamic) {
      onError(error);
    }
    #end
    }

    private function onError(error:Dynamic):Void {
        //trace("SocketLogger", "", error);
    }

    override private function write(text:String):Void {
        try {
            #if flash
            socket.writeUTFBytes(text);
            socket.writeUTFBytes('\n');
            socket.flush();
            #else
            socket.write(text);
            socket.write('\n');
            #end
        } catch (error:Dynamic) {}
    }
}

#end
