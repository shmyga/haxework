package hw.log;

import hw.log.ILogger.LogLevel;
import haxe.CallStack;


class LoggerUtil {

    public static function printPos(pos:haxe.PosInfos):String {
        return '${pos.fileName}:${pos.lineNumber}:';
    }

    public static function printStackItem(item:StackItem):String {
        return switch item {
            case CFunction: 'CFunction';
            case Module(m): m;
            case FilePos(s, file, line): '${file}:${line}';
            case Method(classname, method): '${classname}::${method}}';
            case LocalFunction(v): 'LocalFunction(${v})';
        }
    }

    public static function getErrorStack(error:Dynamic):String {
        #if flash
        if (Std.is(error, flash.errors.Error)) {
            return cast(error, flash.errors.Error).getStackTrace();
        }
        #end
        return CallStack.exceptionStack().map(printStackItem).join('\n\t');
    }

    public static function printError(error:Dynamic):String {
        return error == null ? '' : Std.string('${error}\n\t${getErrorStack(error)}');
    }
}


class BaseLogger implements ILogger {

    public function new() {}

    public function log(level:LogLevel, tag:String, message:String, ?error:Dynamic, ?p:haxe.PosInfos):Void {
        write(buildString(level, tag, message, error, p));
    }

    private function buildString(level:LogLevel, tag:String, message:String, ?error:Dynamic, ?p:haxe.PosInfos):String {
        return '${Date.now()} | ${LoggerUtil.printPos(p)} [${level}] ${tag} - ${message}${LoggerUtil.printError(error)}';
    }

    private function write(text:String):Void {}

    public function d(tag:String, message:String, ?error:Dynamic, ?p:haxe.PosInfos):Void {
        log(LogLevel.DEBUG, tag, message, error, p);
    }

    public function i(tag:String, message:String, ?error:Dynamic, ?p:haxe.PosInfos):Void {
        log(LogLevel.INFO, tag, message, error, p);
    }

    public function w(tag:String, message:String, ?error:Dynamic, ?p:haxe.PosInfos):Void {
        log(LogLevel.WARNING, tag, message, error, p);
    }

    public function e(tag:String, message:String, ?error:Dynamic, ?p:haxe.PosInfos):Void {
        log(LogLevel.ERROR, tag, message, error, p);
    }
}
