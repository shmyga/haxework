package hw.log;

import haxe.PosInfos;
#if cpp
import cpp.Stdio;
import cpp.ConstCharStar;
#end


class TraceLogger extends BaseLogger {

    public function new() {
        super();
    }

    public static function print(v: Dynamic, ?infos : PosInfos) : Void {
        #if flash
            #if (fdb || native_trace)
                var str = flash.Boot.__string_rec(v, "");
                untyped __global__["trace"](str);
            #else
                untyped flash.Boot.__trace(v, infos);
            #end
        #elseif neko
            untyped {
                $print(v);
                $print("\n");
            }
        #elseif js
            if (js.Syntax.typeof(untyped console) != "undefined" && (untyped console).log != null)
                (untyped console).log(v);
            //untyped js.Boot.__trace(v, infos);
        #elseif android
            hw.log.AndroidLog.write(3, "", ConstCharStar.fromString(Std.string(v)));
        #elseif (php && php7)
            php.Boot.trace(v);
        #elseif php
            untyped __call__('_hx_trace', v);
        #elseif cpp
            Stdio.printf(ConstCharStar.fromString(Std.string(v)+'\n'));
            //untyped __trace(v, null);
        #elseif cs
            cs.system.Console.WriteLine(v);
        #elseif java
            untyped __java__("java.lang.System.out.println(v)");
        #elseif lua
            untyped __define_feature__("use._hx_print",_hx_print(v));
        #elseif (python)
            python.Lib.println(v);
        #elseif hl
            var str = Std.string(v);
            Sys.println(str);
        #end
    }

    override private function write(text:String):Void {
        print(text);
    }
}
