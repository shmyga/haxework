package hw.log;

import cpp.VarArg;
import haxe.extern.Rest;
import cpp.ConstCharStar;

@:include("android/log.h")
extern class AndroidLog {

    @:native("__android_log_print")
    public static function print(prio:Int, tag:ConstCharStar, fmt:ConstCharStar, rest:Rest<VarArg>):Void;

    @:native("__android_log_write")
    public static function write(prio:Int, tag:ConstCharStar, message:ConstCharStar):Void;
}
