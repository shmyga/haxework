package hw.log;

import hw.log.ILogger.LogLevel;
import flash.external.ExternalInterface;
import hw.log.BaseLogger;


class JSLogger extends BaseLogger {

    private var available:Bool;

    public function new() {
        super();
        available = ExternalInterface.available;
    }

    override public function e(tag:String, message:String, ?error:Dynamic, ?p:haxe.PosInfos):Void {
        if (available) {
            var text:String = buildString(LogLevel.ERROR, tag, message, error);
            try {ExternalInterface.call("console.error", text);} catch (error:Dynamic) {available = false;}
        }
    }

    override private function write(text:String):Void {
        if (available) {
            try {ExternalInterface.call("console.log", text);} catch (error:Dynamic) {available = false;}
        }
    }
}
