package hw.utils;

using StringTools;

class StringUtil {

    public static function title(value:String):String {
        return (value.substr(0, 1).toUpperCase() + value.substr(1)).replace("_", " ");
    }
}
