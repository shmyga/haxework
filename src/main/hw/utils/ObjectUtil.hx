package hw.utils;

import haxe.Unserializer;
import haxe.Serializer;

class ObjectUtil {

    public static function clone<T>(object:T):T {
        return new Unserializer(Serializer.run(object)).unserialize();
    }
}
