package hw.utils;

class NumberUtil {

  public static inline function limitInt(value:Int, min:Int, max:Int):Int {
    return Math.round(Math.max(Math.min(max, value), min));
  }

  public static inline function limitFloat(value:Float, min:Float, max:Float):Float {
    return Math.max(Math.min(max, value), min);
  }
}
