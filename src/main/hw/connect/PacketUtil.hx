package hw.connect;

import haxe.io.Bytes;
import haxe.io.BytesOutput;
import protohx.Message;

class PacketUtil {

    public static function fromBytes<P:Message>(bytes:Bytes, factory:Class<P>):P {
        var packet:P = Type.createInstance(factory, []);
        packet.mergeFrom(bytes);
        return packet;
    }

    public static function toBytes<P:Message>(packet:P):Bytes {
        var out = new BytesOutput();
        packet.writeTo(out);
        return out.getBytes();
    }

    public static function toBytesWithSize<P:Message>(packet:P):Bytes {
        var out = new BytesOutput();
        out.writeUInt16(0);
        packet.writeTo(out);
        var bytes = out.getBytes();
        bytes.setUInt16(0, bytes.length - 2);
        return bytes;
    }
}
