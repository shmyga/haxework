package hw.connect.js;

import haxe.io.Bytes;
import hw.connect.IConnection;
import js.Browser;
import js.html.BinaryType;
import js.html.WebSocket;
import promhx.Deferred;
import promhx.Promise;
import protohx.Message;

class JsConnection<O:Message, I:Message> extends BaseConnection<O, I> {

    private var host:String;
    private var port:Int;
    private var socket:WebSocket;

    public function new(host:String, port:Int, inputFactory:Class<I>) {
        super(inputFactory);
        this.host = host;
        this.port = port;
        connected = false;
    }

    public static function isSecured():Bool {
        return Browser.location.protocol == "https:";
    }

    private function buildSocket(host:String, port:Int):WebSocket {
        var protocol = isSecured() ? "wss:" : "ws:";
        return new WebSocket('$protocol//$host:$port');
    }

    override public function connect():Promise<IConnection<O, I>>  {
        var self = this;
        connectDeferred = new Deferred();
        try {
            socket = buildSocket(host, port);
            socket.binaryType = BinaryType.ARRAYBUFFER;
            socket.onopen = this.onConnect;
            socket.onclose = this.onClose;
            socket.onerror = this.onError;
            socket.onmessage = this.onSocketData;
        } catch (error:Dynamic) {
            connectDeferred.throwError(error);
        }
        return connectDeferred.promise();
    }

    override public function disconnect():Void {
        socket.close(1000);
        connected = false;
    }

    private function onError(event:Dynamic):Void {
        socket.close(1000);
        connected = false;
        handler.emit(ConnectionEvent.ERROR(event));
    }

    private function onConnect(_):Void {
        connected = true;
        handler.emit(ConnectionEvent.CONNECTED);
        connectDeferred.resolve(this);
    }

    private function onClose(_):Void {
        socket.close();
        connected = false;
        handler.emit(ConnectionEvent.DISCONNECTED);
    }

    private function onSocketData(event:Dynamic):Void {
        var bytes = Bytes.ofData(event.data);
        pushData(bytes);
    }

    override public function send(packet:O):Void {
        if (connected) {
            super.send(packet);
            var bytes = PacketUtil.toBytesWithSize(packet);
            socket.send(bytes.getData());
        } else {
            L.w("Connection", "closed");
        }
    }
}
