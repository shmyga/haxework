package hw.connect.session;

import haxe.io.Bytes;
import hw.connect.server.ServerConnection;
import hw.connect.server.ServerWSConnection;
import protohx.Message;
import sys.net.Socket;

class ProtoSession<O:Message, I:Message> implements ISession {
    private static inline var TAG = "Session";

    private static var POLICY_FILE:String = [
        "<?xml version=\"1.0\"?>",
        "<!DOCTYPE cross-domain-policy SYSTEM \"http://www.adobe.com/xml/dtds/cross-domain-policy.dtd\">",
        "<cross-domain-policy>",
        "<site-control permitted-cross-domain-policies=\"master-only\"/>",
        "<allow-access-from domain=\"*\" to-ports=\"*\"/>",
        "</cross-domain-policy>"
    ].join("\n");

    private static var idCounter:Int = 0;

    public var id(default, null):Int;
    public var connection(default, null):IConnection<O, I>;
    private var socket:Socket;
    private var request:Class<I>;

    public function new(socket:Socket, request:Class<I>) {
        this.id = ++idCounter;
        this.socket = socket;
        this.request = request;
    }

    private function buildConnection(bytes:Bytes):IConnection<O, I> {
        var str:String = bytes.getString(0, bytes.length);
        if (str == "<policy-file-request/>" + String.fromCharCode(0)) {
            L.d(TAG, "policy-file-request");
            socket.output.writeString(POLICY_FILE + String.fromCharCode(0));
            socket.output.flush();
            return null;
        }
        if (StringTools.startsWith(str, "GET")) {
            connection = new ServerWSConnection<O, I>(socket, request);
        } else {
            connection = new ServerConnection<O, I>(socket, request);
        }
        connection.receiveHandler.connect(onRequest);
        return connection;
    }

    public function send(packet:O):Void {
        connection.send(packet);
    }

    public function pushData(bytes:Bytes):Void {
        if (connection == null) {
            connection = buildConnection(bytes);
        }
        if (connection != null) {
            connection.pushData(bytes);
        }
    }

    public function disconnect():Void {
        connection.disconnect();
    }

    private function onRequest(request:I):Void {
        L.d(TAG, 'onRequest: ${request}');
    }
}
