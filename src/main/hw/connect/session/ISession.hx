package hw.connect.session;

import haxe.io.Bytes;

interface ISession {
    public var id(default, null):Int;
    public function pushData(bytes:Bytes):Void;
    public function disconnect():Void;
}
