package hw.connect;

import haxe.io.Bytes;
import hw.connect.IConnection;
import hw.signal.Signal;
import promhx.Deferred;
import promhx.Promise;
import protohx.Message;

class BaseConnection<O:Message, I:Message> implements IConnection<O, I> {
    public var handler(default, null):Signal<ConnectionEvent>;
    public var sendHandler(default, null):Signal<O>;
    public var receiveHandler(default, null):Signal<I>;
    public var connected(default, null):Bool;
    public var queue(default, null):PacketQueue<I>;

    private var connectDeferred:Deferred<IConnection<O, I>>;

    public function new(inputFactory:Class<I>) {
        queue = new PacketQueue<I>(inputFactory);
        handler = new Signal<ConnectionEvent>();
        sendHandler = new Signal<O>();
        receiveHandler = new Signal<I>();
    }

    public function connect():Promise<IConnection<O, I>> {
        throw "Not implemented";
    }

    public function disconnect():Void {
        throw "Not implemented";
    }

    public function pushData(bytes:Bytes):Void {
        #if proto_debug L.d('Proto', 'pushData: ${bytes.length}'); #end
        queue.addBytes(bytes);
        while (queue.hasMsg()) {
            var packet:I = queue.popMsg();
            receive(packet);
        }
    }

    public function send(packet:O):Void {
        #if proto_debug L.d('Proto', 'send: ${packet}'); #end
        sendHandler.emit(packet);
    }

    public function receive(packet:I):Void {
        #if proto_debug L.d('Proto', 'receive: ${packet}'); #end
        receiveHandler.emit(packet);
    }
}
