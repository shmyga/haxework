package hw.connect;

import protohx.Message;

class ConnectionFactory {

    public static function buildClientConnection<O:Message, I:Message>(host: String, port:Int, inputFactory:Class<I>):IConnection<O, I> {
        #if flash
        return new hw.connect.flash.FlashConnection<O, I>(host, port, inputFactory);
        #elseif html5
        return new hw.connect.js.JsConnection<O, I>(host, port + (hw.connect.js.JsConnection.isSecured() ? 1 : 0), inputFactory);
        #else
        return new hw.connect.desktop.DesktopConnection<O, I>(host, port, inputFactory);
        #end
    }
}
