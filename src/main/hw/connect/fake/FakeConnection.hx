package hw.connect.fake;

import hw.connect.IConnection;
import promhx.Promise;
import protohx.Message;

class FakeConnection<O:Message, I:Message> extends BaseConnection<O, I> {

    override public function connect():Promise<IConnection<O, I>> {
        handler.emit(ConnectionEvent.CONNECTED);
        var promise:Promise<IConnection<O, I>> = cast Promise.promise(this);
        return promise;
    }

    override public function disconnect():Void {
        handler.emit(ConnectionEvent.DISCONNECTED);
    }
}
