package hw.connect.desktop;

import cpp.vm.Thread;
import haxe.Timer;
import hw.connect.IConnection;
import promhx.Deferred;
import promhx.Promise;
import protohx.Message;
import sys.net.Host;
import sys.net.Socket;

class DesktopConnection<O:Message, I:Message> extends BaseConnection<O, I> {

    private var host:String;
    private var port:Int;
    private var socket:Socket;
    private var reader:Thread;

    public function new(host:String, port:Int, inputFactory:Class<I>) {
        super(inputFactory);
        this.host = host;
        this.port = port;
        connected = false;
        socket = new Socket();
        socket.setFastSend(true);
        socket.output.bigEndian = false;
        socket.input.bigEndian = false;
        sendHandler.connect(_send);
    }

    override public function connect():Promise<IConnection<O, I>> {
        connectDeferred = new Deferred();
        try {
            if (connected) {
                connectDeferred.resolve(this);
            } else {
                socket.connect(new Host(host), port);
                connected = true;
                reader = Thread.create(_read);
                connectDeferred.resolve(this);
                handler.emit(ConnectionEvent.CONNECTED);
            }
        } catch (error:Dynamic) {
            handler.emit(ConnectionEvent.ERROR(error));
            Timer.delay(function() connectDeferred.throwError(error), 1);
        }
        return connectDeferred.promise();
    }

    override public function disconnect():Void {
        socket.close();
        connected = false;
        handler.emit(DISCONNECTED);
    }

    private function _read():Void {
        try {
            while (connected) {
                socket.waitForRead();
                var size = socket.input.readUInt16();
                var data = socket.input.read(size);
                var packet:I = PacketUtil.fromBytes(data, queue.packetClass);
                receiveHandler.emit(packet);
            }
        } catch (error:Dynamic) {
            handler.emit(ERROR(error));
        }
    }

    private function _send(packet:O):Void {
        var bytes = PacketUtil.toBytes(packet);
        socket.output.writeUInt16(bytes.length);
        socket.output.write(bytes);
        socket.output.flush();
    }
}
