package hw.connect;

import haxe.io.Bytes;
import hw.signal.Signal;
import promhx.Promise;
import protohx.Message;

enum ConnectionEvent {
    CONNECTED;
    DISCONNECTED;
    ERROR(error:Dynamic);
}

interface IConnection<O:Message, I:Message> {
    public var connected(default, null):Bool;
    public var handler(default, null):Signal<ConnectionEvent>;
    public var sendHandler(default, null):Signal<O>;
    public var receiveHandler(default, null):Signal<I>;

    public function connect():Promise<IConnection<O, I>>;
    public function disconnect():Void;
    public function send(packet:O):Void;
    public function pushData(bytes:Bytes):Void;
}
