package hw.connect.server;

import hw.Timer;
import protohx.Message;
import sys.net.Socket;

class ServerConnection<O:Message, I:Message> extends BaseConnection<O, I> {

    public var socket(default, null):Socket;

    private var sendQueue:Array<O>;
    private var timer:Timer;

    public function new(socket:Socket, i:Class<I>) {
        super(i);
        this.socket = socket;
        socket.setFastSend(true);
        socket.output.bigEndian = false;
        socket.input.bigEndian = false;
        sendHandler.connect(pushPacket);
        sendQueue = [];
        timer = new Timer(1);
        timer.run = sendRun;
    }

    private function sendPacket(packet:O):Void {
        try {
            var bytes = PacketUtil.toBytes(packet);
            socket.output.writeUInt16(bytes.length);
            socket.output.write(bytes);
            socket.output.flush();
        } catch (error:Dynamic) {
            L.e('Proto', 'Error send packet: ${packet}', error);
        }
    }

    private function sendRun():Void {
        if (sendQueue.length > 0) {
            for (packet in sendQueue) {
                sendPacket(packet);
            }
            sendQueue = [];
        }
    }

    private function pushPacket(packet:O):Void {
        sendQueue.push(packet);
    }

    override public function disconnect():Void {
        if (timer != null) {
            timer.stop();
            timer = null;
        }
        super.disconnect();
    }
}
