package hw.connect;

import haxe.io.Bytes;
import haxe.io.BytesBuffer;
import haxe.io.BytesInput;
import protohx.Message;

class PacketQueue<P:Message> {

    public var packetClass(default, null):Class<P>;

    private var buffer:BytesBuffer;
    private var msgs:List<P>;

    public function new(packetClass:Class<P>) {
        this.packetClass = packetClass;
        msgs = new List<P>();
        buffer = new BytesBuffer();
    }

    public inline function hasMsg():Bool {
        return !msgs.isEmpty();
    }

    public inline function popMsg():P {
        return msgs.pop();
    }

    public inline function addMsg(msg:P):Void {
        msgs.add(msg);
    }

    private function readPackage():Null<P> {
        var bytes = buffer.getBytes();
        var input = new BytesInput(bytes);
        input.bigEndian = false;
        if (input.length > 1) {
            var packetSize = input.readUInt16();
            if (input.length >= packetSize + 2) {
                var packet:P = Type.createInstance(packetClass, []);
                try {
                    packet.mergeFrom(input.read(packetSize));
                    buffer = new BytesBuffer();
                    buffer.add(input.read(input.length - (packetSize + 2)));
                    return packet;
                } catch (error:Dynamic) {
                    L.w("PacketQueue", "readPackage ", error);
                    buffer = new BytesBuffer();
                    return null;
                }
            }
        }
        buffer = new BytesBuffer();
        buffer.add(bytes);
        return null;
    }

    public function addBytes(bytes:Bytes):Void {
        buffer.add(bytes);
        var packet = readPackage();
        while (packet != null) {
            msgs.add(packet);
            packet = readPackage();
        }
    }

    public function clean():Void {
        buffer = new BytesBuffer();
    }
}
