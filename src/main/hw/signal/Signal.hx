package hw.signal;

typedef Signal<A> = Signal1<A>;

class BaseSignal<R> {

    private var receivers:Array<R>;

    public function new() {
        receivers = [];
    }

    public function connect(receiver:R):Void {
        receivers.push(receiver);
    }

    public function disconnect(receiver:R):Void {
        #if neko
        receivers = receivers.filter(function(r) return !Reflect.compareMethods(r, receiver));
        #else
        receivers.remove(receiver);
        #end
    }

    public function dispose():Void {
        receivers = [];
    }
}

typedef Receiver0 = Void -> Void;

class Signal0 extends BaseSignal<Receiver0> {

    public function emit():Void {
        for (receiver in receivers) {
            receiver();
        }
    }
}

typedef Receiver1<A> = A -> Void;

class Signal1<A> extends BaseSignal<Receiver1<A>> {

    public function emit(a:A):Void {
        for (receiver in receivers) {
            receiver(a);
        }
    }
}

typedef Receiver2<A, B> = A -> B -> Void;

class Signal2<A, B> extends BaseSignal<Receiver2<A, B>> {

    public function emit(a:A, b:B):Void {
        for (receiver in receivers) {
            receiver(a, b);
        }
    }
}

typedef Receiver3<A, B, C> = A -> B -> C -> Void;

class Signal3<A, B, C> extends BaseSignal<Receiver3<A, B, C>> {

    public function emit(a:A, b:B, c:C):Void {
        for (receiver in receivers) {
            receiver(a, b, c);
        }
    }
}
