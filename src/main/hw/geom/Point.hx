package hw.geom;

abstract Point(Array<Float>) {
    public var x(get, set):Float;
    public var y(get, set):Float;

    public function new(x:Float, y:Float) {
        this = [x, y];
    }

    private inline function get_x() return this[0];

    private inline function set_x(value) return this[0] = value;

    private inline function get_y() return this[1];

    private inline function set_y(value) return this[1] = value;

    public function add(point:Point):Point {
        return new Point(x + point.x, y + point.y);
    }

    public function subtract(point:Point):Point {
        return new Point(x - point.x, y - point.y);
    }

    public function clone():Point {
        return new Point(x, y);
    }

    public function toString():String {
        return 'Point{x=$x,y=$y}';
    }
}
