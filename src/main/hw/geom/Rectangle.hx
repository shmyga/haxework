package hw.geom;

abstract Rectangle(Array<Float>) {
    public var x(get, set):Float;
    public var y(get, set):Float;
    public var width(get, set):Float;
    public var height(get, set):Float;
    public var center(get, set):Point;
    public var size(get, set):Point;
    public var left(get, never):Float;
    public var right(get, never):Float;
    public var top(get, never):Float;
    public var bottom(get, never):Float;
    public var position(get, set):Point;

    public function new(x:Float = 0, y:Float = 0, width:Float = 0, height:Float = 0) {
        this = [x, y, width, height];
    }

    private inline function get_x() return this[0];

    private inline function set_x(value) return this[0] = value;

    private inline function get_y() return this[1];

    private inline function set_y(value) return this[1] = value;

    private inline function get_width() return this[2];

    private inline function set_width(value) return this[2] = value;

    private inline function get_height() return this[3];

    private inline function set_height(value) return this[3] = value;

    private function get_center():Point {
        return new Point(x + width / 2, y + height / 2);
    }

    private function set_center(value:Point):Point {
        x = value.x - width / 2;
        y = value.y - height / 2;
        return value;
    }

    private function get_size():Point {
        return new Point(width, height);
    }

    private function set_size(value:Point):Point {
        width = value.x;
        height = value.y;
        return value;
    }

    public function contain(point:Point):Bool {
        return point.x >= left && point.y >= top && point.x <= right && point.y <= bottom;
    }

    public function intersection(rect:Rectangle):Bool {
        return !(
            rect.left > right ||
            rect.right < left ||
            rect.top > bottom ||
            rect.bottom < top
        );
    }

    public function clone():Rectangle {
        return new Rectangle(x, y, width, height);
    }

    public function toString():String {
        return 'Rectangle{x=$x,y=$y,width=$width,height=$height}';
    }

    private function get_left():Float {
        return x;
    }

    private function get_right():Float {
        return x + width;
    }

    private function get_top():Float {
        return y;
    }

    private function get_bottom():Float {
        return y + height;
    }

    private function get_position():Point {
        return new Point(x, y);
    }

    private function set_position(position:Point):Point {
        x = position.x;
        y = position.y;
        return position;
    }
}
