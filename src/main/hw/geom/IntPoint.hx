package hw.geom;

abstract IntPoint(Array<Int>) {
    public var x(get, set):Int;
    public var y(get, set):Int;

    public function new(x:Int, y:Int) {
        this = [x, y];
    }

    private inline function get_x() return this[0];

    private inline function set_x(value) return this[0] = value;

    private inline function get_y() return this[1];

    private inline function set_y(value) return this[1] = value;

    public function clone():IntPoint {
        return new IntPoint(x, y);
    }

    public function toString():String {
        return 'IntPoint{x=$x,y=$y}';
    }

    @:to inline public function toInt():Int {
        return (x << 16) + y;
    }

    @:to inline public function toPoint():Point {
        return new Point(x, y);
    }
}

