package hw.animate;

import flash.events.Event;
import flash.Lib;

@:provide class AnimateRunner {

    private var animates:Array<IAnimate>;

    public function new() {
        animates = [];
        Lib.current.stage.addEventListener(Event.ENTER_FRAME, _ -> update());
    }

    private function update():Void {
        if (animates.length > 0) {
            var time = Date.now().getTime();
            animates = animates.filter(animate -> animate.update(time));
        }
    }

    public function push(animate:IAnimate):Void {
        animates.push(animate);
    }
}
