package hw.animate;

import flash.display.DisplayObject;
import flash.display.Sprite;
import hw.animate.Animate;
import hw.animate.IAnimate;
import hw.view.IView;
import promhx.Promise;

class CircleMaskAnimate extends Animate {

    private var mask:Sprite;
    private var cyrcle:Sprite;
    private var size:Float;

    public function new(view:IView<DisplayObject>, duration:Int = -1) {
        super(view, duration);
        this.view = view;
        this.mask = new Sprite();
        this.cyrcle = new Sprite();
    }

    override public function start():Promise<IAnimate> {
        var width = view.parent.width;
        var height = view.parent.height;
        size = Math.sqrt(width * width + height * height);
        //size = Math.max(width, height);
        cyrcle.x = mask.x = -(size - width) / 2 - size;
        cyrcle.y = mask.y = -(size - height) / 2 - size;

        redraw(size, size);

        view.parent.container.addChild(mask);
        view.content.mask = mask;
        view.parent.container.addChild(cyrcle);

        return super.start();
    }

    private function redraw(size:Float, r:Float):Void {
        mask.graphics.clear();
        mask.graphics.beginFill(0xffffff);
        mask.graphics.drawCircle(size + size / 2, size + size / 2, r / 2);
        mask.graphics.endFill();

        cyrcle.graphics.clear();
        cyrcle.graphics.lineStyle(4, 0xffffff);
        cyrcle.graphics.drawCircle(size + size / 2, size + size / 2, r / 2);
        cyrcle.graphics.lineStyle();
    }

    override public function update(time:Float):Bool {
        var result = super.update(time);
        redraw(size, size * progress);
        if (progress >= 1 && view.content.parent != null) {
            if (view.content.parent.contains(mask)) view.content.parent.removeChild(mask);
            view.content.mask = null;
            if (view.content.parent.contains(cyrcle)) view.parent.container.removeChild(cyrcle);
        }
        return result;
    }
}
