package hw.animate;

import hw.animate.Animate;
import promhx.Promise;

class UnFadeAnimate extends Animate {

    override public function start():Promise<IAnimate> {
        object.alpha = 0.0;
        return super.start();
    }

    override public function update(time:Float):Bool {
        var result = super.update(time);
        object.alpha = progress * 1.0;
        if (progress >= 1) {
            object.alpha = 1.0;
        }
        return result;
    }
}
