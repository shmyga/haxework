package hw.animate;

import promhx.Promise;

interface IAnimate {

    public function start():Promise<IAnimate>;

    public function cancel():Void;

    public function update(time:Float):Bool;
}
