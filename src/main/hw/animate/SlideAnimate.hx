package hw.animate;

import promhx.Promise;

class SlideAnimate extends Animate {

    override public function start():Promise<IAnimate> {
        object.x = view.x - this.view.width + this.view.width / progress;
        return super.start();
    }

    override public function update(time:Float):Bool {
        var result = super.update(time);
        object.x = view.x - this.view.width + this.view.width / Math.min(1, progress);
        return result;
    }
}
