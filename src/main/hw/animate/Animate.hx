package hw.animate;

import flash.display.DisplayObject;
import hw.view.IView;
import promhx.Deferred;
import promhx.Promise;

class Animate implements IAnimate {
    public static var defaultDuraion = 300;

    @:provide private static var runner:AnimateRunner;

    private var view:IView<Dynamic>;
    private var duration:Int;
    private var startTime:Float;
    private var progress:Float;

    private var object(get, null):DisplayObject;
    private var deferred:Deferred<IAnimate>;

    public function new(view:IView<Dynamic>, duration:Int = -1) {
        this.view = view;
        this.duration = duration > -1 ? duration : defaultDuraion;
        this.startTime = 0;
    }

    private inline function get_object():DisplayObject {
        return cast view.content;
    }

    public function start():Promise<IAnimate> {
        startTime = Date.now().getTime();
        deferred = new Deferred();
        runner.push(this);
        this.update(startTime);
        return deferred.promise();
    }

    public function update(time:Float):Bool {
        progress = (time - startTime) / duration;
        if (progress >= 1) {
            deferred.resolve(this);
            return false;
        }
        return true;
    }

    public function cancel():Void {
        progress = startTime + duration;
    }
}
