package hw;

#if cpp

import haxe.Log;
import haxe.PosInfos;
import cpp.vm.Thread;

class Timer {

    private var sleep:Float;
    private var stopped:Bool;

    public function new(time_ms:Int) {
        this.sleep = time_ms / 1000.0;
        this.stopped = false;
        Thread.create(function() {
            while (!stopped) {
                Sys.sleep(sleep);
                try {
                    run();
                } catch (error:Dynamic) {
                    trace(hw.log.BaseLogger.LoggerUtil.printError(error));
                }
            }
        });
    }

    public function stop() {
        stopped = true;
    }

    public dynamic function run() {}

    public static function delay(f:Void -> Void, time_ms:Int) {
        var t = new Timer(time_ms);
        t.run = function() {
            t.stop();
            f();
        };
        return t;
    }

    public static function measure<T>(f:Void -> T, ?pos:PosInfos):T {
        var t0 = stamp();
        var r = f();
        Log.trace((stamp() - t0) + "s", pos);
        return r;
    }

    public static inline function stamp():Float {
        return Sys.time();
    }
}

#else

typedef Timer = haxe.Timer;

#end
