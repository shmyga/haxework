package hw.storage;

class NoStorage implements IStorage {

    public function exists(key:String):Bool {
        return false;
    }

    public function write<T>(key:String, value:T):Void {}

    public function read<T>(key:String):Null<T> {
        return null;
    }

    public function clear():Void {}

    public function delete(key:String):Void {}
}
