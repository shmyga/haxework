package hw.storage;

import flash.net.SharedObject;
import haxe.Serializer;
import haxe.Unserializer;

class SharedObjectStorage implements IStorage {

    private var so:SharedObject;

    public function new(name:String = "storage") {
        so = SharedObject.getLocal(name);
    }

    public function exists(key:String):Bool {
        return Reflect.hasField(so.data, key);
    }

    public function write<T>(key:String, value:T):Void {
        so.setProperty(key, Serializer.run(value));
        so.flush();
    }

    public function read<T>(key:String):Null<T> {
        if (exists(key)) {
            var data = Reflect.field(so.data, key);
            return new Unserializer(data).unserialize();
        } else {
            return null;
        }
    }

    public function delete(key:String):Void {
        if (exists(key)) {
            Reflect.deleteField(so.data, key);
            so.flush();
        }
    }

    public function clear():Void {
        so.clear();
    }
}
