package hw.storage;

interface IStorage {
    public function exists(key:String):Bool;

    public function write<T>(key:String, value:T):Void;

    public function read<T>(key:String):Null<T>;

    public function delete(key:String):Void;

    public function clear():Void;
}
