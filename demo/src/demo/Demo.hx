package demo;

import demo.dispatch.DemoDispatcher;
import demo.popup.ColorPopup;
import demo.popup.FontPopup;
import demo.storage.TestStorage;
import hw.app.App;
import hw.log.TraceLogger;
import hw.net.JsonLoader;
import hw.resources.IResources;
import hw.view.data.ButtonGroup;
import hw.view.frame.FrameSwitcher;
import hw.view.frame.FrameView;
import hw.view.group.VGroupView;

@:template class DemoView extends VGroupView {
    @:view var switcher:FrameSwitcher;
    @:view var tabs:ButtonGroup<String>;

    public function new():Void {
        super();
        switcher.change("list");
    }

    private function onFrameSwitch(frame:FrameView<Dynamic>):Void {
        tabs.selected = frame.frameId;
    }

    private function choiceColor():Void {
        new ColorPopup().show().then(function(color) {
            if (color != null) {
                theme.colors = {light: color};
            }
        });
    }

    private function choiceFont():Void {
        new FontPopup().show().then(function(font) {
            if (font != null) {
                theme.font = font;
            }
        });
    }
}

class Demo extends App implements DemoListener {

    @:provide static var storage:TestStorage;

    public static function main() {
        L.push(new TraceLogger());

        App.resources.image.put("logo", HaxeLogo.resolve());
        var app = new Demo();
        app.theme = new AppTheme();
        app.icon = App.resources.image.get("logo");
        app.view = new DemoView();
        trace(storage);
        storage.write("test", "value");

        var dispatcher = new DemoDispatcher();
        dispatcher.connect(app);
        dispatcher.test1Signal.emit();
        dispatcher.test2Signal.emit(1);
        dispatcher.test3Signal.emit(1, "test");
        dispatcher.test4Signal.emit(app);
        dispatcher.disconnect(app);
        dispatcher.test1Signal.emit();
        dispatcher.test2Signal.emit(1);
        dispatcher.test3Signal.emit(1, "test");
        dispatcher.test4Signal.emit(app);

        new JsonLoader().GET("https://embed.tvbit.co/channel/data2/renova.json")
        .then(function(data:Array<Model>) {
            App.resources.any.put("data", data);
            App.resources.any.put("data50", Util.marray(data, 50));
        })
        .catchError(function(error) trace(error));
    }

    public function onTest1():Void {
        trace('test1');
    }

    public function onTest2(a:Int):Void {
        trace('test2', a);
    }

    public function onTest3(a:Int, b:String):Void {
        trace('test3', a, b);
    }

    public function onTest4(app: App):Void {
        trace('test4', app);
    }

}
