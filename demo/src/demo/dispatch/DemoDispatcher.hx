package demo.dispatch;

import hw.app.App;

interface DemoListener {
    public function onTest1():Void;
    public function onTest2(a:Int):Void;
    public function onTest3(a:Int, b:String):Void;
    public function onTest4(app:App):Void;
}

@:dispatcher(DemoListener) class DemoDispatcher {
    public function new() {}
}
