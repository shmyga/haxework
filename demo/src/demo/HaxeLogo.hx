package demo;

import flash.display.BitmapData;

#if !openfl
@:bitmap("haxe-logo.png")
#end
class HaxeLogo extends BitmapData {

    public static function resolve():BitmapData {
        #if openfl
        return openfl.Assets.getBitmapData("image/haxe-logo.png");
        #else
        return new HaxeLogo(0, 0);
        #end
    }
}
