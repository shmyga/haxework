package demo.form;

import hw.view.frame.FrameView;
import hw.view.list.LabelListItem;
import hw.view.list.ListView.IListItemView;
import hw.view.list.VListView;

@:template class ListForm extends FrameView<Dynamic> {
    @:view public var list(default, null):VListView<Model>;

    public function new() {
        super("list");
    }

    private function factory() {
        return new LabelListItem(function(index:Int, value:Model) return '${index}. ${value.id}: ${value.title}');
    }

    private function onItemSelect(item:IListItemView<Model>):Void {
        trace('onItemSelect: ${item.data.id}');
    }
}
