package demo.form;

import hw.view.frame.FrameView;
import hw.view.ImageView;
import hw.view.IView;
import hw.view.text.TextView;
import hw.view.utils.DrawUtil;

@:template class TailForm extends FrameView<Dynamic> {

    public function new() {
        super("tail");
    }

    private function factory(index:Int, value:Model):IView<Dynamic> {
        var view:IView<Dynamic>;
        if (value.image != null) {
            var imageView = new ImageView();
            imageView.style = "view";
            imageView.stretch = false;
            //imageView.style = "border";
            imageView.fillType = FillType.CONTAIN;
            imageView.imageUrl = value.image.url;
            view = imageView;
        } else {
            var textView = new TextView();
            textView.style = "view";
            textView.text = '${value.id}\n${value.maker}';
            view = textView;
        }
        view.setSize(350, 200);
        return view;
    }
}
