package demo.form;

import hw.view.frame.FrameView;
import hw.view.text.TextView;

@:template class DataForm extends FrameView<Dynamic> {
    public function new() {
        super("data");
    }

    private function factory(index:Int, value:Model):TextView {
        var label = new TextView();
        label.geometry.width.percent = 100;
        label.geometry.margin = 1;
        label.geometry.padding = 2;
        label.text = (value.title != null ? '${value.title}\n-\n' : '') + value.message;
        return label;
    }
}
