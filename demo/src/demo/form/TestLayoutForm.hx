package demo.form;

import flash.events.MouseEvent;
import hw.view.frame.FrameView;
import hw.view.SpriteView;

@:template class TestLayoutForm extends FrameView<Dynamic> {

    @:view var render:SpriteView;

    public function new() {
        super("test_layout");
        resize();
        content.addEventListener(MouseEvent.CLICK, function(_) {
            resize();
        });
    }

    private function resize():Void {
        var w = 200 + 400 * Math.random();
        var h = 100 + 200 * Math.random();
        render.setSize(w, h, "render");
    }
}
