package demo.form;

import hw.view.frame.FrameView;

@:template class SelectForm extends FrameView<Dynamic> {
    public function new() {
        super("select");
    }
}
