package demo.popup;

import flash.text.Font;
import flash.text.FontType;
import hw.view.list.LabelListItem;
import hw.view.list.ListView;
import hw.view.popup.PopupView;
import hw.view.theme.ITheme;

class FontLabelView extends LabelListItem<ThemeFont> {

    override private function set_data(value:ThemeFont):ThemeFont {
        style = item_index % 2 == 0 ? "light" : "dark";
        data = value;
        text = value.name;
        font.family = value.name;
        font.embed = value.embed;
        return data;
    }
}

@:template class FontPopup extends PopupView<ThemeFont> {

    @:view var fonts:ListView<ThemeFont>;

    public function new():Void {
        super();
        var values:Array<ThemeFont> = Font.enumerateFonts(true).map(function(font:Font) {
            return {
                name: font.fontName,
                embed: switch font.fontType {
                    case DEVICE: false;
                    case _: true;
                }
            }
        });
        values.sort(function(a:ThemeFont, b:ThemeFont) {
            return switch [a.embed, b.embed] {
                case [false, true]: 1;
                case [true, false]: -1;
                case _: 0;
            }
        });
        fonts.data = values;
    }

    private function fontViewFactory() return new FontLabelView();
}
