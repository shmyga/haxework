package demo.popup;

import hw.view.form.ButtonView;
import hw.view.popup.PopupView;
import hw.view.skin.Skin;

@:template class ColorPopup extends PopupView<Null<Int>> {

    private function colorViewFactory(index:Int, color:Int) {
        var view = new ButtonView();
        view.setSize(48, 48, "fixed");
        view.geometry.padding = 0;
        view.skin = Skin.buttonColor(color);
        return view;
    }
}
