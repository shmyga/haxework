package demo;

import hw.view.geometry.Box;
import hw.color.Color;
import hw.view.theme.Theme;

using hw.color.ColorUtil;

class AppTheme extends Theme {

    public function new(?color:Color, ?textColor:Color):Void {
        super({name: "Courirer"}, {light: color, text: textColor});
    }

    override private function reload():Void {
        super.reload();
        register(new Style("view", [
            "skin.background.color" => colors.light,
            "skin.border.color" => colors.border,
            "geometry.padding" => Box.fromFloat(3),
        ], ["text"]));
        register(new Style("test", [
            "skin.background.color" => Color.fromInt(0x00ffff),
        ]));
    }
}
