package demo;

typedef Model = {
    var id:String;
    var created_at:Int;
    var maker:String;
    var title:String;
    var message:String;
    var image:{url:String, width:Int, height:Int};
}
