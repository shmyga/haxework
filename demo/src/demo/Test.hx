package demo;

import demo.test.TestView;
import hw.view.Root;
import hw.view.SpriteView;

class Test {
    public static function main():Void {
        new Test();
    }

    public function new() {
        trace("Test");
        var view = new SpriteView();
        Root.bind(new TestView());
    }
}
