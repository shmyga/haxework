package demo.storage;

import hw.storage.SharedObjectStorage;

@:provide class TestStorage extends SharedObjectStorage {

    public function new() {
        super("test");
    }
}
