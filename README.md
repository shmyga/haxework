# HaxeWork

HaXe framework.

## View

`hw.view` provides views classes.

### Example

Build form using macro @:template.

form.yaml:

```yaml
---
skin:
- $type: hw.gui.skin.ColorSkin
  color: 0xffff00
geometry.padding: 20
layout.margin: 10
views:
- id: view1
  type: hw.view.SpriteView
  geometry.size.stretch: true
  skin:
  - $type: hw.view.skin.ColorSkin
    color: 0xff0000
- id: view2
  type: hw.view.SpriteView
  geometry.size.width: 100%
  geometry.size.height: 50
  skin:
  - "$type": hw.view.skin.ColorSkin
    color: 0x00ff00
```

```haxe
@:template("form.yaml")
class FormView extends VGroupView {
    @:view public var view1:SpriteView;
    @:view("view2") public var anyVarName:SpriteView;
}

var form = new FormView();
Root.bind(form); // Add form to stage as root view element.
trace(form.view1);
trace(form.anyVarName);
```

## Loader

`hw.net` provides loaders classes.

### Example

```haxe
new JsonLoader().GET("http://example.com/file.json")
  .then(function(data) {
      trace("Json Ok: " + data);
  })
  .catchError(function(error) {
      trace("Json Fail: " + error);
  });
```
